.. index::
   pair: htmx; philosohpy

.. _htmx_philosohpy:

==================================================================================
**HTMX philosophy**
==================================================================================


I prefer the garden/stewardship metaphor
============================================

- https://x.com/htmx_org/status/1449060097162981380?s=20

.. figure:: images/garden.png
   :align: center

En réponse à @DenizAksimsek
Wish we could stop thinking about tech in terms of life & death, as if
frameworks are like mayflies.

I prefer the garden/stewardship metaphor, where a well tended garden,
once it is set in place properly, can be productive for years with little effort

not really the tech culture.

Comment from https://x.com/devsimplicity
---------------------------------------------------

- https://x.com/devsimplicity/status/1449398198427168775?s=20

That's a great metaphor. We should generally try to approach these things
in less destructive ways (i.e. more like permaculture than modern agriculture).

Fun with hyperscript
======================

.. figure:: images/have_you_having_fun.png
   :align: center

   https://discord.com/channels/725789699527933952/796428329531605032/899727170849603697
