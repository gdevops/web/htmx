.. index::
   pair: HTMX ; Extensions
   ! Extensions

.. _htmx_extensions:

==================================================================================
HTMX **extensions**
==================================================================================

- https://htmx.org/extensions/
- :ref:`hx_ext`

Description
============

Htmx provides an extension mechanism for defining and using extensions
within htmx-based applications.


List of extensions
====================

.. figure:: liste_des_extensions.png
   :align: center

.. toctree::
   :maxdepth: 1

   client_side_templates/client_side_templates
   morphdom_swap/morphdom_swap
   preload/preload

