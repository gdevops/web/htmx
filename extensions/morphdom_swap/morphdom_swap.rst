.. index::
   pair: morphdom-swap ; Extension
   ! morphdom-swap

.. _morphdom_swap:

========================================================================================================
HTMX **morphdom-swap** an extension for using the morphdom library as the swapping mechanism in htmx
========================================================================================================

- https://htmx.org/extensions/morphdom-swap/
- https://unpkg.com/htmx.org/dist/ext/morphdom-swap.js
- https://github.com/patrick-steele-idem/morphdom
- https://x.com/psteeleidem

Description
============

This extension allows you to use the `morphdom library <https://github.com/patrick-steele-idem/morphdom>`_
as the swapping mechanism in htmx.

The morphdom library does not support morph element to multiple elements.

If the result of :ref:`hx-select <hx_select>` is more than one element,
it will pick the first one.


Usage
======

.. code-block:: html

    <header>
      <script src="lib/morphdom-umd.js"></script> <!-- include the morphdom library -->
    </header>
    <body hx-ext="morphdom-swap">
       <button hx-swap="morphdom">This button will be swapped with morphdom !</button>
    </body>

Source
=========

- https://unpkg.com/htmx.org/dist/ext/morphdom-swap.js


Examples
==========

- https://youtu.be/dEg-K3kMj60?t=1560
- :ref:`hernantz_htmx_partial_rendering`
