.. index::
   pair: client-side-templates ; Extension
   ! client-side-templates

.. _client_side_templates:

==================================================================================
HTMX **client-side-templates** extension
==================================================================================

- https://htmx.org/extensions/client-side-templates/


Description
============

This extension supports transforming a JSON request response into HTML
via a client-side template before it is swapped into the DOM.

Currently three client-side templating engines are supported:

- mustache
- handlebars
- nunjucks

When you add this extension on an element, any element below it in the
DOM can use one of three attributes named <template-engine>-template
(e.g. mustache-template) with a template ID, and the extension will
resolve and render the template the standard way for that template engine:

- mustache - looks a mustache <script> tag up by ID for the template content
- handlebars - looks in the Handlebars.partials collection for a template with that name
- nunjucks - resolves the template by name via `nunjucks.render()`

The AJAX response body will be parsed as JSON and passed into the
template rendering.


