.. index::
   pair: htmx; FAQ
   ! FAQ

.. _htmx_faq:

=====================
HTMX FAQ
=====================


I have a question about hyperscript and htmx
===============================================

Question
    It seems like you could write some (all?) htmx features as hyperscript.
    Is there a lot of overlap between the two? I haven't used either yet,
    so I am in the "get my head wrapped around what they're trying to do"
    phase of this.


Response
    htmx is the primary technology I would recommend for most web applications:
    you drive server interactions using HTML-based attributes, and stay
    within the HTML model.
    Very simple and you can accomplish a lot (see https://htmx.org/examples)

    hyperscript is a more speculative project for doing pure front end scripting.
    it is event-oriented and thus plays very well with htmx (which has an extensive
    event model: https://htmx.org/reference/#events)
    I reach for it when I want to do something purely client side.
    You can consider it a modern alternative to jQuery if you like.

    I would not build an entire app in hyperscript, but rather would use
    it to embellish an existing app with light front end code.

**htmx and alpinejs**
========================

Question
    thanks. i saw another today. https://alpinejs.dev/ . i've only toyed
    with htmx so far and wondered. do you have to go balls deep with it
    or can you just use it sparingly to begin with and still do ur own
    thing like.

Response
    https://discord.com/channels/725789699527933952/725789747212976259/897823720524103690

    **htmx** is incremental and can be used for as much or as little of your
    site as you want.

    For example, if you have a slow chunk of your site that you want to
    lazy load, it's two attributes: https://htmx.org/examples/lazy-load/

    **Alpine** is a front-end oriented library that pairs well with **htmx**.
    A lot of people use the two together: **htmx for server interaction,
    alpine for pure front end work**.

    There is also :ref:`hyperscript <tuto_hyperscript:hyperscript>`, a sibling project of **htmx**, that we are
    working on and that addresses use cases similar to alpine, but from
    a different direction: https://hyperscript.org/

    Articles:

    - https://dev.to/nicholas_moen/what-i-learned-while-using-django-with-htmx-and-alpine-js-24jg


What are the core philosophical differences between @htmx_org and @unpolyjs ?
================================================================================

Question
    https://x.com/codechips/status/1449409685539393548?s=20
    Both are trying to bring the joy of web development back and it's cool,
    but which one should I chose and why ?

Response
    https://x.com/htmx_org/status/1449419451615653890?s=20
    htmx is a relatively low level extension of HTML

    unpoly has more structure (e.g. layers, animations) and gives you more ootb
    hotwire is even more structured and "top down"/batteries included
    each has pros & cons, all are great options

    (hope that's a fair comparison @unpolyjs)


Does someone have an example of using Django forms and htmx ?
===============================================================

- https://discord.com/channels/725789699527933952/864934037381971988/898107056354099220

- https://github.com/ajcerejeira/talksapp
- https://github.com/adamchainz/django-htmx
- https://github.com/jacklinke/django-htmx-todo-list
- https://github.com/MattSegal/django-htmx-intro
- https://github.com/justdjango/django_htmx_dynamic_forms



Does htmx support JSON POST/PUT commands ?
============================================

Question
    charris — 2021-10-15 à 22:26
    I heard this from someone with a lot more dev-fu than me and this is his
    only real complaint at this point about htmx: if you want to do “real”
    API-style work, with JSON bodies, you have to write script; HTMX doesn’t
    support JSON POST/PUT.
    For my posts and puts I had to back off and use fetch.

Response
    https://discord.com/channels/725789699527933952/725789747212976259/898670864080453743
    1cg — 2021-10-15 à 22:37
    We use "normal" form encoding in the requests we submit.
    We do have a **cutpoint for extensions** do to their own encoding
    however: https://github.com/bigskysoftware/htmx/blob/1778ae77ca5bec9a9e437b0de114660fa5602220/src/htmx.js#L2011

    ::

        function encodeParamsForBody(xhr, elt, filteredParameters) {
            var encodedParameters = null;
            withExtensions(elt, function (extension) {
                if (encodedParameters == null) {
                    encodedParameters = extension.encodeParameters(xhr, filteredParameters, elt);
                }
            });
            if (encodedParameters != null) {
                return encodedParameters;
            } else {
                if (getClosestAttributeValue(elt, "hx-encoding") === "multipart/form-data" ||
                    (matches(elt, "form") && getRawAttribute(elt, 'enctype') === "multipart/form-data")) {
                    return makeFormData(filteredParameters);
                } else {
                    return urlEncode(filteredParameters);
                }
            }
        }

    so if you wanted to submit JSON you could


.. _faq_boost:

When using htmx-boost is there a way to designate elements that should be ignored ?
=======================================================================================

Question
----------

    mauricioarango — Aujourd’hui à 03:40
    Little question: when using htmx-boost is there a way to designate elements
    that should be ignored ?
    I placed the htmx attributes on the document's body. And it almost works
    perfect for my needs, but there are a couple of forms, that have their
    own logic and validations, and , ideally, should be ignored by htmx-boost.
    Thanks so much for your feedback.

Response
    https://discord.com/channels/725789699527933952/725789747212976259/898748570952421417
    1cg — 2021-10-16 à 03:45

    put hx-boost='false' on those elements

