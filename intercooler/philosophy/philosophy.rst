.. index::
   ! philosophy

.. _intercooler_philosophy:

==================================================================================
**intercooler philosophy**
==================================================================================


- https://intercoolerjs.org/docs.html#philosophy


Philosophy
=============

It can be easy for some developers to dismiss intercooler as overly simple
and an archaic way of building web applications.
**This is intellectually lazy**.

Intercooler is a tool for returning to the original network architecture
of the web.
Using HTML as the data transport in communication with a server is what
enables HATEOAS, the core distinguishing feature of that network architecture.

Intercooler goes with the grain of the web, rather than forcing a more
traditional thick-client model onto it, thereby avoiding the complexity
and security issues that come along with that model.

Yes, **intercooler is simple, but it is a deceptive simplicity**, very much
like the early web.

A few related blog posts for the interested reader:

Rescuing REST
HATEOAS is for Humans
API Churn v. Security

On Churn
===========

Many javascript projects are updated at a dizzying pace. Intercooler is not.

This is not because it is dead, but rather because it is (mostly) right:
the basic idea is right, and the implementation at least right enough.

This means there will not be constant activity and churn on the project,
but rather a stewardship relationship: the main goal now is to not screw
it up.

The documentation will be improved, tests will be added, small new delarative
features will be added around the edges, but there will be no massive
rewrite or constant updating.

This is in contrast with the software industry in general and the front
end world in particular, **which has comical levels of churn**.

Intercooler is a sturdy, reliable tool for web development.

Conclusion
=============

And that's it!

Not a ton to it, which is kind of the point: you can build surprisingly
rich UIs with this simple and easy to understand tool, and you can do it
incrementally in the areas that matter the most for your users.

**There is no need to be complex...**

