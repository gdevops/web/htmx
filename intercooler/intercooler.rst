.. index::
   ! intercooler

.. _intercooler:

==================================================================================
**intercooler.js** (htmx ancestor)
==================================================================================

- https://github.com/bigskysoftware/intercooler-js


.. figure:: intercooler_logo.png
   :align: center


.. toctree::
   :maxdepth: 3

   blog/blog
   philosophy/philosophy
