

==================================================================================
HTMX Django revolution
==================================================================================

- https://x.com/JackDLinke/status/1434566085639131138?s=20



Long live the revolution!

By which I mean that more of us are coming back to the best parts of early
web 2.0 when things were interactive and dynamic, but we weren't yet
suffering from framework hell, separate front/back end, difficult APIs,
etc.

Loving hearing these stories.


.. figure:: htmx_django_revolution.png
   :align: center

   https://x.com/JackDLinke/status/1434566085639131138?s=20


Exactly my feelings as well. It's insane how easy many things actually are,
when you're not fighting with build tools and frameworks all day long.

HTTP, HTML, CSS and JS are packed with ton of features, but somehow people
love reinventing the wheel in JS.
