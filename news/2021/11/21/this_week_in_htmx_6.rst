.. _thisweek_in_htmx_2021_11_21:

===============================================================================================
|thisweek| 2021-11-21 **This week in htmx N°6** by  Deniz Akşimşek |thisweek|
===============================================================================================

- https://thisweek.htmx.org/issue/6/


htmx 1.6.1 out soon
======================


1cg htmx 1.6.1 slated for release tomorrow

- test suite here: https://dev.htmx.org/test/1.6.1/test/index.html
- release notes here: https://dev.htmx.org/posts/2021-11-22-htmx-1.6.1-is-released/


_hyperscript 0.9 released
=============================

Highlights include:

- when clause for show
- ES Module support
- continue for loops
- and much more!

- https://hyperscript.org/posts/2021-11-02-hyperscript-0.9-is-released/
