
.. _Hoarddit_2021_11_22:

============================================================================================
**Hoarddit - A Website to Discover** Art by Josef Erben (https://x.com/joseferben)
============================================================================================

- https://www.joseferben.com/posts/hoarddit_a_website_to_discover_art/
- https://x.com/joseferben
- https://x.com/htmx_org/status/1462415395458064397?s=20

Introduction
==============

Together with my SO we built hoarddit.com, a website that helps everyone
to discover art.

It allows you to virtually trade art pieces.

This post describes how we spent our innovation points and why hoarddit
is not an NFT.


Is It an NFT ?
===================

On hoarddit.com we state that **the project is not an NFT**.

Is that really the case? Let’s examine the definition of an NFT on Wikipedia:

    A non-fungible token (NFT) is a unique and non-interchangeable unit
    of data stored on a digital ledger (blockchain).

The art pieces on hoarddit are **unique units of data stored in PostgreSQL**.

The definition says nothing about the ledger being distributed. If there
was no mention about /blockchain, maybe one could argue that hoarddit
is an NFT platform by this definition.

However, **there is no blockchain involved**, there are no tokens on distributed
ledgers and most importantly, no one is getting rich over night.

In fact, hoarddit costs couple of bucks a month to host and **there are no
plans to monetize it**.


Boring Technology
======================

We chose boring and tested technology that we were familiar with.

**Hoarddit** is a **Django app using Postgres** and good old server side templates.
For some dynamic components HTMX was used. The design is powered by
customized Bootstrap 5.

For crawling and sending emails we are using Django Q.

Email sending uses the generous free tier of Mailjet. The whole thing is
deployed on a Hetzner VPS using Dokku.

Sentry and Uptimerobot are used for monitoring and reporting.

This is the final picture, but it was a process to end up with this stack.
A process that involved spending some innovation points.

