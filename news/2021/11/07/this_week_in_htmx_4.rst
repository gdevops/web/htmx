.. _thisweek_in_htmx_2021_11_07:

===============================================================================================
|thisweek| 2021-11-07 **This week in htmx N°4** by  Deniz Akşimşek |thisweek|
===============================================================================================

- https://thisweek.htmx.org/issue/4/
- https://www.youtube.com/c/DjangoConUS/videos



Commspace became the first corporate sponsor of htmx
=======================================================

extremely thankful to announce the first corporate sponsor of htmx & hyperscript:
@commspaceapp
thank you to @jvosloo and the whole commspace team!

— @htmx_org

💙 The founder of Commspace is Johan Vosloo, who is also a supporter of the Week.

- https://www.commspace.co.za
- https://github.com/sponsors/bigskysoftware
- https://thisweek.htmx.org/support


Michael Anckaert used htmx for a CI/CD service
================================================

I picked server side rendered HTML as the main technology for the user
interface.

Thanks to HTMX I was able to add some nice interactivity and UI / UX
improvements without having to resort to a large scale framework such
as Vue or React.

Using HTMX makes me very productive on the frontend, which as a more
backend leaning developer is a big bonus. Read more about HTMX and HTML
over the wire concepts.

- https://sinax.be/blog/general/pipeliner-a-cicd-pipeline-runner.html

An htmx meetup in Chemnitz, Germany is coming up
=================================================

25 Nov, 19:00–21-00, Q-HUB GmbH | Innovation & Startup Space Chemnitz.
Thomas Güttler will be speaking !

- https://www.meetup.com/de-DE/Web-Developer-Chemnitz/events/281827325/


.. _formdata_hyperscript:

How to submit data from a form in _hyperscript
=================================================

Deniz Akşimşek should be a matter of::

    make a FormData from #form
    fetch /url { method: "POST", body: result }

in the upcoming release it will become::

    make a FormData from #form
    fetch /url with method: "POST", body: result

`💬 November 2, 2021, 5:29 PM UTC <https://discordapp.com/channels/725789699527933952/796428329531605032/905146642435559544>`_

- https://hyperscript.org/commands/make
- https://hyperscript.org/commands/fetch
- https://developer.mozilla.org/en-US/docs/Web/API/FormData/FormData
- https://developer.mozilla.org/en-US/docs/Web/API/fetch#parameters


.. _hyperscript_vs_alpine_2021_11_07:

hyperscript vs Alpine.js
============================

Deniz Akşimşek ↪ Re. Rashad For those that have used hyperscript and
Alpine.js with TailwindCSS…

Microinteractions are one of the main use cases for _. Alpine fixes
the “where’s that event listener” problem, but does nothing about
the DOM APIs.

I think alpine is better for larger components like data tables, because
of x-for and reactivity.

For microinteractions, you will need to manipulate the DOM directly a
lot more, which is a dream in hyperscript.

`💬 November 5, 2021, 6:07 PM UTC <https://discordapp.com/channels/725789699527933952/864934037381971988/906243399634526218>`_

Editor’s note: Let me make it clear that this is not a dig on Alpine in
any way.
I gave my personal opinion to help people decide what to use.
We’re volunteer-run free software projects with a lot of shared goals. 💙

