

.. _htmx_newest_2021_06_04:

========================================================================
2021-06-10 Htmx: The newest old way to make web apps
========================================================================


- https://blog.logrocket.com/htmx-the-newest-old-way-to-make-web-apps/


Introduction
================

Htmx is a JavaScript library for performing AJAX requests, triggering
CSS transitions, and invoking WebSocket and server-sent events directly
from HTML elements.

Htmx lets you build modern and powerful user interfaces with simple markups.

This library weighs ~10KB (min.gz’d), it is dependency-free (i.e., it
does not require any other JavaScript package to run), and it’s also
compatible with IE11.

In this tutorial, we will be exploring the powerful features of htmx while covering the following sections:

- Installing htmx
- Sending AJAX requests with htmx
- Custom htmx input validation
- Triggering CSS animation with htmx

