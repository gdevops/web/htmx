

.. _javascript_right_in_html:

==========================================================
2021-06-15 **JavaScript Right in HTML ? HTMX w/ Carson**
==========================================================

- https://www.htmlallthethings.com/podcasts/javascript-right-in-html-htmx-w-carson



:Recorded: June 15, 2021
:Released: June 23, 2021

In this episode, Matt and Mike sit down with Carson to discuss HTMX, a
project that he built to help you do more with your HTML.

Similar (in concept) to how TailwindCSS can extend your HTML with more
CSS-like functionality, HTMX brings JavaScript-like manipulation to your
HTML.

The guys also discuss Carson's previous projects and go over his passion
for `locality of behaviour <lob>`.

