
.. index::
   pair: javascript ; scheme
   pair: gemini ; scheme


.. _htmx_scheme:

=========================
2021-06-22 htmx-scheme
=========================


experimental branch w/ protocol support in hx attributes: https://github.com/bigskysoftware/htmx/tree/feature_js_expression_support
==============================================================================================================================================

- https://github.com/bigskysoftware/htmx/tree/feature_js_expression_support
- https://discord.com/channels/725789699527933952/725789747212976259/856542961025941556

experimental branch w/ protocol support in hx attributes: https://github.com/bigskysoftware/htmx/tree/feature_js_expression_support

::

    it('handles generic protocol properly', function () {
            htmx.config.protocolHandlers['foo'] = function(){
                window.foo = "bar";
            }
            var div = make('<div hx-post="foo:blah blah blah">click me</div>');
            div.click();
            window.foo.should.equal("bar");
            delete window.foo
            delete htmx.config.protocolHandlers['foo']
        })

scheme
----------


- https://discord.com/channels/725789699527933952/725789747212976259/856613353635184660

::

    it's a way to add custom URI schemes (like https: javascript: ssh: gemini:)
    [21:15]
    @1cg you should probably use the spec term "scheme" instead of protocol

Updated the short Locality of Behavior essay
================================================

updated the short Locality of Behavior essay:

- https://htmx.org/essays/locality-of-behaviour/

if you are advocating for @tailwindcss , @Alpine_JS
or htmx/hyperscript and someone objects that they violate Separate of Concerns (SoC),
Locality of Behavior (LoB) gives you a conceptual model to respond with.

