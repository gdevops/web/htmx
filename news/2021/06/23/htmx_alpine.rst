


==============================================================================
2021-06-23 You might not need a frontend framework + tailwindcss + htmx book
==============================================================================


You might not need a frontend framework
==============================================================================================================================================

- https://buttondown.email/alpinejs/archive/alpinejs-weekly-63/


Thank you Afonso Cerejeira for this great introduction to Htmx, alpine.js
and hotwire.

You might not need a frontend framework

.. figure:: alpine/htmx_alpine.png
   :align: center

   https://buttondown.email/alpinejs/archive/alpinejs-weekly-63/


.. _book_htmx_alpine:

Book
=======

looks like some bad escaping on the form action
[17:18]
question: would people be interested in this book:
# HTML-Oriented Development
Building simple, maintainable and powerful web applications using the native language of the web

Table of Contents
---------------------

* Chapter 1 - HTML, A Reintroduction
* Chapter 2 - htmx: High Power Tools for HTML
* Chapter 3 - Effective HTML Templating
* Chapter 4 - URLs
* Chapter 5 - Practical Examples
* Chapter 6 - Other HTML-oriented Tools: tailwindscss, alpinejs & hyperscript
* Chapter 7 - REST


.. figure:: alpine/book_htmx_alpine.png
   :align: center

   https://discord.com/channels/725789699527933952/725789747212976259/856916182577971240


https://tailwindcss.com/docs/extracting-components
==================================================

- https://discord.com/channels/725789699527933952/796428329531605032/856955604898414642
- https://tailwindcss.com/docs/extracting-components

speaking of Locality of Behavior: https://tailwindcss.com/docs/extracting-components

seems to me that tailwinds has a worse DRY problem than htmx or hyperscript

htmx Frontend Revolution
===========================

- https://x.com/guettli/status/1407641912832663552?s=20
- https://docs.google.com/presentation/d/12dgaBnUgl4cmEkiOhUJL5hsbGQ6hB5sslDuozmBjVUA/edit#slide=id.p
