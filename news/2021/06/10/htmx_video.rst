

.. _htmx_2021_06_10:

========================================================================
2021-06-10 HTMX - Clean Dynamic HTML Pages Talk Python Live Stream
========================================================================

HTMX - Clean Dynamic HTML Pages Talk Python Live Stream
=============================================================

- https://www.youtube.com/watch?v=4wjqsPtj2QY


Join Michael and Carson (the creator of HTMX) to discuss how this alternative
frontend technology brings easy dynamic web apps while leveraging many
of the powerful features from popular web frameworks like Flask or Django.


.. figure:: images/htmx_intro.png
   :align: center


.. figure:: images/ui_examples.png
   :align: center

.. figure:: images/click_to_edit.png
   :align: center


.. figure:: hyperscript/intro.png
   :align: center


https://github.com/guettli/peps/blob/master/pep-9999.rst
=============================================================

- https://github.com/guettli/peps/blob/master/pep-9999.rst
