

.. _less_javascript_2021_04_08:

==============================================================================================================
2021-04-08 **Less JavaScript more HTMX by https://x.com/jerodsanto and https://x.com/feross**
==============================================================================================================

- https://changelog.com/jsparty/171
- https://x.com/jerodsanto
- https://x.com/feross


Description
===============

Jerod & Feross learn all about htmx (a pragmatic approach to web frontends)
and _hyperscript (an experimental scripting language inspired by HyperTalk)
with special guest Carson from Big Sky Software.

Thanks to Rajasegar Chandran for requesting this episode !

