
.. _jack_linke_2021_10_22:

=================================================================================================================================================
Talks: 2021 Friday, October 22 **Server-Side is Dead! Long Live Server-Side (+ HTMX)** by Jack Linke (https://x.com/JackDLinke)
=================================================================================================================================================

- https://github.com/jacklinke/htmx-talk-2021
- https://x.com/JackDLinke
- https://2021.djangocon.us/talks/server-side-is-dead-long-live-server/


On youtube
===========

- https://www.youtube.com/watch?v=t98bKdeUHsU
- https://www.youtube.com/c/DjangoConUS

Objectives
=============

By the end of this talk, audience members will have a better understanding
of what HTMX is, will understand concrete examples of how it can be applied
to their django projects, and will have a list of resources for further
learning and discussion.


Outline
=========

The current state (5 min)
--------------------------

- Django templates
- Ajax, transitions, and asynchronous front end approaches
- Heavy JS Frameworks (Vue, React, jQuery)

A different mindset (9 min)
------------------------------

- Getting back to the heart of HATEOAS
- Splitting up complicated views

Feature and approach walk-throughs (20 min)
----------------------------------------------

For a variety of common web application features, we will take a look
at a typical django approach and how one might approach the problem
with django + htmx.

Messaging inbox functionality (read, archive)
+++++++++++++++++++++++++++++++++++++++++++++++

- The default Django approach
- Django + HTMX approach

One-click settings
+++++++++++++++++++++

- The default Django approach
- Django + HTMX approach

Multiple forms in multiple tabs
+++++++++++++++++++++++++++++++

- The default Django approach
- Django + HTMX approach


Formsets and an HTMX approach
+++++++++++++++++++++++++++++++

- The default Django approach
- Django + HTMX approach


Tips, best practices, and pitfalls (6 min)
--------------------------------------------

- CSRF Tokens
- Lightweight JS libraries which compliment HTMX
- Simplifying things with django-htmx
- Resources for further reading and community

Jack Linke
==============

Jack Linke tends to learn the hard way - and shares the lessons from
those experiences with others through blogging, tweets, and speaking
engagements.

He has been developing software and hardware projects off-and-on for
most of his life, but much of his relevant web development experience
has been hard-earned over the past four years during development of
Watervize - a B2B2B SaaS web application (written in Django) to help
irrigation water utility companies improve efficiency, analysis,
and communication with staff and agriculture customers.

Jack’s technology interests include Python, the Django project, HTMX,
GIS, graph theory, data storytelling, and visualization.

He is a frequent contributor to the open source community and a
contributing member of the Python Software Foundation.

His speaking experience includes briefing Generals on topics ranging
from budgets to technical concepts, instructing at formal training centers,
and providing software demonstrations and feature walk-throughs.

Outside of coding, Jack solves unusual math and logic problems, and
makes a mess in the kitchen.


Transcription
================

.. toctree::
   :maxdepth: 3

   transcription/transcription
