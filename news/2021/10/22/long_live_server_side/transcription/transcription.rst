
.. _transcription_jack_linke_2021_10_22:

=================================================================================================================================================
Transcription: 2021 Friday, October 22 **Server-Side is Dead! Long Live Server-Side (+ HTMX)** by Jack Linke
=================================================================================================================================================


Server-Side is Dead! Long Live Server-Side (+ HTMX)
=====================================================

.. figure:: images/about_me.png
   :align: center

Welcome and thank you for joining me. Hopefully, next time we'll get to
do this in person. My name is :ref:`Jack Linke <jack_linke>`.

I've been a Django developer since 2018, so not that long, but really
dove in headfirst. I am a PSF contributing member, and I'm also the lead
developer for a web application for utilities districts that helps them
manage their resources.

I got involved with HTMX late last year. Really kind of at the beginning
of its development and increasing popularity.
So I was very fortunate to be involved. My talk today is entitled,
**Server-Side is Dead ! Long Live Server-Side (+HTMX)**.

And thank you again for joining me. So I started web programming at the
turn of the millennium back in the early days of Web 2.0.
JavaScript was still very young Ajax, which is the ability to asynchronously
do things like form submissions or updating parts of the page without
needing a full refresh.
It really wasn't even a thing yet. CSS still felt really experimental
if you'd even heard of it, but interactivity and dynamic content were
the new thing.
And in those days, everything was done Server side, often in Perl or PHP,
we were creating really cool database backed web applications that felt
powerful. It was something really new. A website wasn't just a static page
of text and images. It could now respond to your inquiries and return
customized content.

Sadly while I continued programming and other technical work life really
took me away from the web programming aspect of things for the most part
for about a decade and a half.
When I got back into developing for the web in 2018, **the whole environment
was completely different**. Half the internet was running on JavaScript,
separate teams were working on the front end and backend.

There was complicated workflows and APIs and it seemed like there was
layer upon layer of frameworks to enable that same kind of dynamic
interactive website.

So in some ways in 2018, I was really brand new to web development all
over again. But I remembered maybe with rose colored glasses, that web
development was in **many ways, much more straightforward 20 years ago**.


Does modern web development have to be super complicated ?
===========================================================

.. figure:: images/why_so_complicated.png
   :align: center

So it really brought to mind the question, **does modern web development
have to be super complicated ?**
So what's the current state ? Well Django provides you with templates,
but how do you apply interactivity if you want do partial page refreshes
or submit a form without the entire page reloading, what do you do ?


.. figure:: images/javascript_frameworks_fatigue.png
   :align: center


And there's a variety of different approaches, Vanilla JavaScript, you can
throw some Ajax in line in your page. jQuery allows you to do some really
cool stuff, but it's kind of outdated now, Single Page Applications gained
a lot of popularity.
So you have div after div after div in your page, you can use Vue directly,
which keeps things a little more lightweight.

You know, you just import the Vue script and you can do stuff pretty
straightforward. And then there's more **heavy things** like React or Angular,
but so many different directions. And a lot of these are really heavy,
require you to learn a whole lot.

.. figure:: images/frameworks_for_days.png
   :align: center


Here's a few of the frameworks that have been developed over the past two
decades. And most of the tools in this image are focused, at least in part,
on the front end. You can see Django in there at one point, but most of
these are front end tools and there's just a ton of them.

Keep in mind that this image hasn't been updated for two years.


.. figure:: images/frameworks_crazy.png
   :align: center


So just even in that time several more have come out. And if you look at
this roadmap of front end development, you'll see an incredible amount
of things that a new person really needs to learn to get all of the effects,
all of the benefits of modern web development.

**It's just a little crazy.**

Now what's amazing about this image is you can really ignore about half
of the boxes in it when you apply HTMX and still get most of the same benefits.


So what's the standard approach in Django for modern front end development ?
=============================================================================

So what's the standard approach in Django for modern front end development ?

How do we get the interactivity that we want ? Well, good question.
Django was really created and initially came to popularity at a time when
**server-side was really very much the norm**. You did all the development
on the server side and you pushed that content to the front, that's really
how the Django templates work.

Over the past decade there's been so many single page application approaches,
JavaScript front-end frameworks, different tools and options that it really
throws those who are new to Django for a loop and it makes those who've
been working with Django for a little longer feel like they've got a bunch
of new stuff to figure out.

There's really not a standard approach to the front end, other than the
**built-in templating**. And many people feel that that approach is a little
bit outdated.


One potential approach is HTMX, the whole reason that we're here talking today
=================================================================================

So where's the middle ground ? **Well, one potential approach is HTMX, the
whole reason that we're here talking today**.


.. figure:: images/htmx_definition.png
   :align: center


- HTMX is really just an extension to existing :ref:`HTML <tuto_html:html>`.
  So it's not something that requires you learning an entire new language
  or framework.
- It's backend agnostic so you can bring your own backend. It works well
  with Django, but it also works well with a variety of other back ends,
  FastAPI, Flask and in other languages as well.
- It's focused, it only does a few things. **It's really all about the
  interactivity between the front end and the backend**, and allows you
  to access Ajax, CSS transitions, web sockets and service sent events
  all directly in your HTML.

You don't have to incorporate a bunch of additional JavaScript files or
build out anything crazy. You can do it all right in the HTML.

And the nice thing about HTMX is that it's rather small once it's gzipped
it's only about 11 kilobytes. So how do we get from the traditional Django
template approach to a more modern approach with HTMX while sidestepping
all of those JavaScript frameworks and tools that we were talking about
earlier ? Well, I'm gonna take you through a few different examples.


The inbox functionality example
=================================

- :ref:`frow_essay_guttli`

.. figure:: images/inbox_functionality.png
   :align: center

The first one we're gonna look at is **inbox functionality**. So how do you
mark something as read or archived or something like that.
And in this picture, you'll see I've started with a basic bootstrap template
just to make things look pretty real quick.

And you'll see we have two messages here. The first message has been read,
it's background is kind of grayed out.

And the bottom message there is a new message. And you'll see, on the
right hand side, there's a button to archive it or mark it as read, pretty
straightforward.

This is a really common functionality that you'll need in a number of applications.

.. figure:: images/inbox_views.png
   :align: center

And if we look at the **views** here, on the left is kind of just the
standard approach and **on the right is the approach where we are making
use of HTMX**.

And you'll notice really the only difference between these two views
is that if we have marked a message as read, instead of returning the
entire template back to the user, we're including just a **fragment**.

And in this case, I've put the :ref:`fragment <frow_essay_guttli>` in
its **own directory fragments** under the messaging apps templates.
So we returned that with a template response, very simple.

So basically if we get a get request, we send the entire template back.
If we **get a post request, we only send that fragment**.


.. figure:: images/inbox_templates.png
   :align: center

And here's the differences between the templates themselves, on the top
is the traditional approach. And you'll see this is probably something
that most of you have all seen before. Pretty straightforward, just a form.

We've got our CSRF token in there and a button that submits the form.
Now, the thing is that will refresh the entire page on the bottom, very
similar, we've wrapped the whole thing in a div and I've kind of hidden
the form and some of the other content there, but we wrapped the whole
thing in a div.
And then you'll see instead of a button, I've got a span there just to
make things easy. And there's a few different HTML parameters
that are different than what you would normally see.

- :ref:`The hx-post <hx_post>`,
- :ref:`hx-swap <hx_swap>`,
- :ref:`hx-target <hx_target>`
- and :ref:`hx_include`.

Now **I've prepended those with data just to keep my IDE from screaming**,
because anything starting with data is generally acceptable.

Anything starting with something like HX is gonna be called out, even though
it's gonna work just fine.

So in this example when we click on the span, that includes that little icon,
the little envelope icon, what's happening here is HTMX is posting to the
messaging inbox HTMX view.

It's going to swap the outer HTML, which is that div and you can see the
target, we have a different div ID for each message. And then we're going
to also include the contents of the hidden input there named item.
And the value of that hidden input is the message ID.
So pretty straight forward when you hit the icon for archiving, it'll post
the message ID.


.. figure:: images/inbox_mechnisme.png
   :align: center

So in this example, we're using the same template fragment for the initial
display of each message and for the content that is swapped in and in the
main template itself, we just have something that looks like this, for each
message we include the message fragment, pretty straightforward.

The swapped content replaces the entire original content in this example.
So again when I hit that icon, the outer HTML, the entire div is replaced
with a new div that has the updated message.

So here's a quick video, just pretty straight forward, nothing super crazy
but you can see when we hit the icon, the page doesn't have to refresh
it just refreshes that small portion of the page.


One click settings changes example
======================================

.. figure:: images/one_click_settings_change.png
   :align: center


So the next example that we'll look at is one click settings changes.
So in this example, when I hit the checkbox, I want that update to be
made in the database without refreshing the whole page.
A template only based approach that's really not easily possible, you're
gonna refresh the whole page.

Ajax case
--------------------------

.. figure:: images/one_click_example_1.png
   :align: center

In the initial case here, you'll see that I'm using Ajax to show you what
a more realistic initial approach might be to enable that interaction
between the front end and the backend without page refreshes.

In the HTMX example, I've included a small amount of :ref:`hyperscript <tuto_hyperscript:hyperscript>`,
which we'll talk about a little bit later to help with animation. And here
we are going to return HTML directly from the view instead of using a
separate template fragment.

And then the last thing that's kind of different here is instead of
loading the response to the div that encompasses the content we're going
to load that response to a different location, a separate div.
So instead of replacing the entire outer HTML, we're going to replace the
contents of a particular div.

.. figure:: images/one_click_ajax.png
   :align: center

This is probably pretty straightforward if you've worked with Ajax before,
but it might be a little bit intimidating if you're not really versed in
JavaScript.

So in this example, we have a standard form with the check box, which would
be duplicated in real life with multiple check boxes.
And when the check box is checked or unchecked, we pass the value of
setting one or setting two or whichever one we've just checked to the user
settings view for processing on the backend.


HTMX case
--------------------------

.. figure:: images/one_click_htmx.png
   :align: center

In the HTMX version, we can skip any of that special JavaScript. We don't
need any Ajax or anything like that and we can just add a few bits to the
HTML for the checkbox itself.

Here we're posting the value of a hidden input named settings to the user
settings, HTMX view when we get a click event on the checkbox, the HTML
that we get back from the view will be put into the setting one response div
down there at the bottom.


So what do the views look like ?
-----------------------------------

.. figure:: images/one_click_views_classic.png
   :align: center

**So what do the views look like ?**

In the traditional approach if the request is a post, we just toggle the
setting. This is pretty straight forward here. You know, we've received a post,
we get the settings value from that post from the request and then we
either set or unset the Boolean.

Now in this example, in the model on the user model, I have set a model
method, toggle setting one and all that does is if it's a true it sets false
and if it's a false it sets true and then saves to the database.

.. figure:: images/one_click_views_htmx.png
   :align: center


In the HTMX version, we really do the same thing, but we're also gonna
return a **chunk of HTML** that displays a success alert and uses just
a little bit of :ref:`hyperscript <tuto_hyperscript:hyperscript>` to remove
that alert after two seconds.

.. figure:: images/user_settings_htmx.png
   :align: center

Here you can see the end result. We've got interactivity with those check
boxes with just a few changes to our HTML and a few updates to the view
itself, to return some HTML. This same method can really be used for any
other type of instantly updating settings.

So if you have say a select box, where the user can choose preferences
for receiving emails or SMS or something like that, or if you have text
inputs or any other inputs in a form, you can use the same technique and
expand it for that.

Forms in multiple tabs example
===============================

.. figure:: images/multiple_tabs.png
   :align: center


And the last example that we're gonna look at here is a case where you wanna
display multiple independent forms on the same page.

And sometimes it's easiest to put those in individual tabs or in some
other way, where most of the content is hidden away until you click on
something and it changes to the content for that thing.

So in this example, I've got five different forms here, but only one of
them is visible to the user at any time.

Each form is designed to submit separately from the others.


.. figure:: images/multiple_tabs_pattern.png
   :align: center

**This is really a pattern that I use a lot in my own projects**.
Here's an example from one of my projects where I have three different
forms available to the end user for specifying the water order requests
at an irrigation company.

And that's not important here, but just so you can see each form is somewhat
different, but we kind of hide the other two forms behind tabs, and it makes
it easy for the irrigation company to see all of the order form options
without it cluttering up the whole page.

You might use this sort of thing anywhere where you need lots of profile
settings, tenant configurations or variations on order forms like this one.


.. figure:: images/multiple_tabs_form.png
   :align: center

To demonstrate here, I'm using just a really simple form duplicated in
each tab. All we're doing is looking at the username and email for the
current user.


.. figure:: images/multiple_tabs_template_classic.png
   :align: center

In reality, you'd probably have a bunch of different forms and they might
have dozens of different fields on each of them.
In the traditional approach to the template we load each form into each
tab on the page.

So all of these forms are in the Dom content for the page itself.
And when you click on one of those, it just unhides the current tab and hides
the one that was visible before or as you can see here, literally is setting
the active class on the active tab.

.. figure:: images/multiple_tabs_view.png
   :align: center

In the view we have to check to see which form was intended for processing
by looking at the name of the submit button associated with that form.



.. figure:: images/multiple_tabs_view_issues.png
   :align: center

Some of the issues with this are you have to prefix each of your forms
when you're putting them all on the same page, into the same template.
Otherwise, you're gonna end up with forms that have the same ID and that's
really a no-no for HTML and will cause problems.

For multiple forms **the views can get really long and complex** because you
have to display each form on the get request and process each form on
the post request all in the same view, **it just gets really complicated**.
One other potential issue is the amount of content that's loaded to a view
like this.

If you have select boxes that have a bunch of options in each of the forms,
the page may take a long time to load because you're loading up the options
for each of those select boxes at the load of that view.
So maybe some things that you wanna avoid. So I've just kind of highlighted here,
the points that I made before about having to prefix each form so that
it's uniquely identifiable in the browser.

It doesn't look so bad here with only two tabs shown, but you can imagine
it really gets quickly overwhelming as the content that you're pushing
into that view increases. It definitely gets long.

So how would we approach this with HTMX ?
-------------------------------------------

.. figure:: images/multiple_tabs_htmx.png
   :align: center

So how would we approach this with HTMX ? Well, with HTMX we can split
things up a bit. In this case we're gonna use separate templates and views
for each tab. And that might seem like a lot more work, but I will tell you
**it's incredibly easier to troubleshoot and to read through and understand
which bits of code do what, when things are split out a little bit**.

We use a single div for the content of all the tabs, and we only load the
content for the tab that is currently selected.
When the page initially loads up, we will insert the contents for that
first tab. And then when any other tab is selected, we load up the contents
for that tab.

HTMX templates
++++++++++++++++

.. figure:: images/multiple_tabs_template_htmx.png
   :align: center

Here's the update with HTMX for the template itself. And if you look on
the left, we have the base template for the view. And one of the tab fragments
is on the right.

So what's on the right there is what will be loaded into the div at the
very bottom when any tab is selected.
Each tab has some HTMX attributes that do a get request for the contents
when that tab is clicked. And those contents, like I said, get loaded
into the div ar the bottom.
At the top there you'll see that we have another div and what we're doing
there is if you look at the HX trigger attributes of that div, it says
load after a hundred milliseconds.

So we are loading the contents of form one into the tab content div
**after the page has been loaded for a hundred milliseconds**.

At the end of the day, the important thing is that each form inside the div
in the fragment there on the right performs a post of that form to the view
associated with the tab. So there's no more need for prefixing the form itself.


.. figure:: images/multiple_tabs_view_htmx.png
   :align: center

And if we look at the views themselves, this top one, tabs HTMX view
loads up the main content, really all it's doing is loading up a template.
That base template that we looked at before, **there's almost nothing to it**.
And then for each tab that we want, we have a view that displays the form
when we do a get request and processes it individually when we do a post
request.


.. figure:: images/multiple_tabs_video.png
   :align: center

Here's a video real quick, just showing what it looks like.

It's a very simple example here, **but you could expand it considerably**
and it can differently be beneficial.


Examples conclusion
========================

Keep in mind that the three examples that I've gone through here really
**only scratched the surface** of what can be quickly implemented with HTMX,
without the need for extra, you know, heavy frameworks and learning new
programming languages.

Some tips, some best practices and resources
====================================================

.. figure:: images/tips_resources.png
   :align: center

So having looked at a few examples of actually using HTMX, let's close out
with some tips, some best practices and resources that will help you move
on with HTMX and get involved with the community.


Cross-Site Request Forgeries, CSRF
-------------------------------------

- https://owasp.org/www-community/attacks/xss/
- https://docs.djangoproject.com/en/dev/ref/csrf/#how-it-works

.. figure:: images/CSRF.png
   :align: center

So **Cross-Site Request Forgeries, CSRF**, it's an attack that forces an end user
to execute unwanted actions on a web application in which they're currently
authenticated.

Basically it can pose a problem when you're doing posts, posting a request,
putting a request, submitting forms, things like that it's a potential issue.
And Django has some built-in support to help you avoid CSRF attacks.
It provides a token from the backend that you can then put in the template
and it compares the token that it provided with the one that is submitted
when you post or put or delete from the front end.

So how do we include CSRF tokens with our requests when HTMX isn't always
wrapped in a form, we might be posting things when you click a button
or something like that, that doesn't have a form associated with it.

.. figure:: images/CSRF_headers.png
   :align: center

Well, there's two ways we can do that.

- We can either include the :ref:`HX headers <hx_headers>`, which will apply
  to that CSRF token a header in the post itself,


.. figure:: images/CSRF_javascript_snippet.png
   :align: center

- or you can just add this little snippet to the base template for your
  entire site.

And it will add that header on every request and just simplify things
considerably.

You will no longer have to think about making sure that you have the
CSRF token in each of your requests.

Split up the functionality into multiple views
--------------------------------------------------

.. figure:: images/more_views.png
   :align: center

One thing that you'll notice with HTMX and I kinda hit on it before,
especially in the tab example above was that you may end up with more views.
**And this is not necessarily a negative thing**.

Being able to split up the functionality into multiple views that do just
small things each can really help clarify what's going on in your application.
**So don't be afraid of making more views at the end of the day, it will
help I assure you**.


Complimentary JS libraries
----------------------------

.. figure:: images/complimentary_js_libraries.png
   :align: center

Now as I mentioned at the very beginning, HTMX is very focused. Its intent
is not to do everything that Vue or Angular or React can do.

It's focus is really on interactivity and that's the majority of it.

So if you find yourself trying to do things like transitions and animations
and things that are a bit beyond the scope of HTMX itself, there are a
couple of recommended complimentary JavaScript libraries that can help out.

Hyperscript
++++++++++++++

So first is :ref:`hyperscript <tuto_hyperscript:hyperscript>`.
It was designed by the creator of HTMX to be complimentary to HTMX.

It's got its own website with some really good examples, but keep in mind
that its own designer will tell you that it is a pretty speculative library
and subject to changes.

Alpine.js
++++++++++++

Another option is Alpine.js, which is very lightweight and focused.
It only has a few different features, but just like with HTMX, you can
get an incredible amount out of those few features.

So both of those are worthwhile to check out. Both of them are lightweight
in the sense of they aren't designed to do everything and they aren't a
huge code wise.


So your pages won't be drugged down in load time.

Django-htmx
---------------

.. figure:: images/django_htmx.png
   :align: center

Another thing to check out is Django HTMX developed by Adam Johnson,
among other things it provides a debug handler when the debug in your
project is set to true, it'll help you with debugging things.

It has a method for easily determining whether a request was made from HTMX
or if it's just a standard full-page request.

And then it has a bunch of other useful utilities that you'll probably
find helpful as you're developing with Django and HTMX.

Conveniently, Adam also includes a nice example app that demonstrates
the various tools available in his package.


Additional resources
=======================

- https://github.com/jacklinke/htmx-talk-2021

.. figure:: images/additional_resources.png
   :align: center

And last a few additional resources to check out.

- So https://htmx.org/ is the source for a references for documentation,
  for examples and for finding other resources.
- `Awesome-HTMX <https://github.com/rajasegar/awesome-htmx>`_ is a GitHub repository
   that lists out different blog posts, videos and examples for HTMX.
- There is the `HTMX Discord <https://htmx.org/discord>`_ which has a fantastic community.
  The designer of HTMX is on there on a regular basis, along with other people,
  interested in HTMX and there's even a Django and HTM discord board there.
- `The HTMX subreddit <https://www.reddit.com/r/htmx/>`_ that is worth checking out.
- And then I've also got `the notes and code for this presentation <https://github.com/jacklinke/htmx-talk-2021>`_
  and the examples that I presented here available on `GitHub <https://github.com/jacklinke/htmx-talk-2021>`_

Anyway, that's it.

Conclusion
============

.. figure:: images/thank_you.png
   :align: center


I appreciate you coming to my talk and hopefully it was helpful in showing
what sort of things you can do with HTMX with relatively little effort
and without having to learn an entirely new language like JavaScript or
get into the guts of it. I appreciate you coming and I hope you enjoy the
rest of the conference.

Thank you.
Goodbye.
