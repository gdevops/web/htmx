===============================================================================================================================
Transcription Friday, 2021 October 22 **REST, HATEOAS & Django - It's OK to not use JSON... or Javascript** by Carson Gross
===============================================================================================================================

- https://www.youtube.com/watch?v=L_UWY-zHlOA
- :ref:`Carson Gross <carson_gross>`
- https://www.youtube.com/c/DjangoConUS

I wanna point out that we have some actual evidence of the hard times, in to which REST has fallen here at DjangoCon
======================================================================================================================

.. figure:: images/rest_hateoas_django.png
   :align: center

   https://youtu.be/L_UWY-zHlOA?t=38


Good morning, welcome to DjangoCon. My name is :ref:`Carson Gross <carson_gross>`
and I'm gonna be giving a talk today on **REST, HATEOAS, and Django**.

Before we get going, I'd like to say thank you to the organizers of DjangoCon
for letting me give this talk. It's a little bit outside the normal content
that you might expect at a conference like this, but I think you'll see
by the end of this talk that it's highly relevant to Django developers.


.. figure:: images/rest_has_fallen.png
   :align: center

   https://youtu.be/L_UWY-zHlOA?t=61

So, in this topic, or in this talk, excuse me, **we're gonna revisit an old
concept that has unfortunately, fallen on hard times**.

And that's the concept of REST, which REST standing for **Representational
State Transfer**.

You may have heard this term, and we'll get into the details of exactly
what it means here in a little bit.
But before we get going on that explanation, I wanna point out that we
have some actual evidence of the hard times, in to which REST has fallen
here at DjangoCon.


.. figure:: images/not_json_apis.png
   :align: center

   https://youtu.be/L_UWY-zHlOA?t=66


There's another talk that's entitled, "Graphene-Django or: How I Learned to
Stop RESTing and Love the Graph."
And you may think that I'm going, I might criticize this other talk in some ways,
but in fact, I'm not. I'm quite enthusiastic about that talk and I think
it's evidence for my claim in this talk which is that the broad claim being
that **REST really applies mainly in a hypermedia or hypertext situation**,
rather than in a JSON context.

And so that's what I'm hoping to convince you of by the time we get to the
end of this talk, you'll agree with me that REST and HATEOAS, which was
another acronym that we'll explain in more detail a bit later, is useful,
but just not in the usual context its discussed in today, which is JSON APIs.


So let's talk a little bit about how web development grew up
================================================================


.. figure:: images/brief_history.png
   :align: center

Let's leave the GraphQL and all that stuff, all that good stuff to our friends
in the JSON API world, but we're Django developers or web developers, and
let's not give up on REST just because they don't like it.

So let's talk a little bit about how web development grew up, to give you
some background on where the term REST and the terms REST and HATEOAS come from.

Web development really started back in the 90's based on this notion of
hypermedia. And the hypermedia that we're most familiar with today is hypertext,
usually written in HTML. And this was a radically new way of building
and distributing software. You can compare and contrast it with the old
model of distributing binaries to people, they have to click and run an
installer, whatever it was.

The new hypermedia model was very radical as a software model. And so people
thought a lot about that. And one person in particular who thought a lot
about it, who not only built a lot of the original infrastructure but also
sort of put ideas into, did the intellectual work around the early web,
was a man named Roy Fielding.


.. figure:: images/roy_fielding.png
   :align: center


He recognized that the web was a fundamentally new computing paradigm,
and he wrote his PhD dissertation on it, on the work that he had done at
the Apache Project.

And it's from that PhD dissertation that we get the terms REST, RESTful,
and HATEOAS. HATEOAS as an acronym, it's not called out directly in that
dissertation, but the concepts are there, and so it got formalized a little
later.

It's important to understand though, when Roy Fielding wrote this dissertation,
there were no JSON APIs, this was a description of the web, of the web
architecture that he had been helping to build.
And so it didn't apply natively to the JSON world, that sort of came later.

And so it's an important thing to keep in the back of your head as we're
discussing exactly what these acronyms really mean.


.. figure:: images/architecture.png
   :align: center

So this new network architecture that Fielding was describing, REST, has
the following characteristics, he calls them constraints.

- It was client-server architecture, obviously.
- It was stateless, that was relatively interesting, that was interesting
  but not totally unique.
- It was cacheable,
- it was layered,
- and **it had a uniform interface**.

These first four items are interesting and I encourage you to go do some
reading on them, but we're not gonna focus on them in this talk so much.

Instead, we're gonna focus on the **uniform interface**.
And the reason we're gonna focus on it is because it's the most radical
constraint of the bunch in my opinion.

.. figure:: images/uniform_interface.png
   :align: center


.. _the_uniform_interface:

05:14 we're gonna focus on the **uniform interface constraint in REST**
==========================================================================


This is what really differentiates RESTful systems from non-RESTful systems.
So let's ask the question, **what is this uniform interface ?**

And this will help us to understand what REST is, this is you know, again,
my assertion is that this is the crux characteristic of RESTful systems.

So the **uniform interface** consists of four **additional constraints** on the
system architecture.

1) :ref:`Resource identification <resource_identification>` has to be found in the requests,
2) :ref:`resource manipulation <resource_manip>` is done through representations,
3) :ref:`the messages are self-descriptive <self_descriptive_messages>`,
4) and then finally, we get to everyone's favorite, **hypermedia as the engine
   of application state**, sometimes pronounced, :ref:`HATEOAS <htmx_hateoas>`.

And so what do each of these mean ? These **four constraints** form the
**uniform interface constraint in REST**.

So what do each of these mean in brass tacks terms ?

Well, consider this snippet of HTML and recall again, that REST was defined
in terms of HTML, not JSON, so put that JSON knowledge that you have away,
:ref:`the hierarchy, the Richardson Hierarchy <comment_martin_fowler_2021_10_26>`
of REST maturity and **all that stuff, put it away**.


.. figure:: images/put_that_json_away.png
   :align: center


.. _resource_identification:

06:29 Let's just talk about HTML, **resource identification**
=================================================================


Let's just talk about HTML. Consider this snippet of HTML, this would obviously
be in a larger document, but just consider this by itself, we're omitting
some of the tags.

Retrieved from https://example.com/contacts/42 so in a normal web application,
this HTML would be retrieved by issuing a GET request.
I hope you know what a GET request is, to a given URL, like so.
And thus, the **resource identification** is embedded in that request.
So this is a **resource, contacts/42 is a resource** and the URL is embedded
in the request.

So we have this situation where **resource identification** is in this request
that's been issued to retrieve this HTML, so **that's satisfying that first
constraint** of :ref:`uniform interface <the_uniform_interface>`.

.. _resource_manip:

Secondly, manipulating through resources
===========================================

.. figure:: images/manipulation_throught_resources.png
   :align: center

Secondly, **manipulating through resources**. Well, note that an end user who
retrieves the, who receives this HTML is able to manipulate this, able to
manipulate this entity through this representation.

They've got options in the form of links here to perform updates and you know,
depending on what they click, they might get a form, they might get some
other piece of HTML that gives them further options to update this resource.

And so the browser here doesn't know what a contact is, rather, it's just
getting a **representation** of that contact and with this **representation**, allows
the user to actually mutate and update to manipulate this resource.

Very interesting, a very new way of approaching, presenting something like
this to a user in a software system.


.. _self_descriptive_messages:

Self-descriptive messages.
=============================

.. figure:: images/self_descriptive.png
   :align: center


Self-descriptive messages. Well, as such, these messages you can see are
self-descriptive.

They contain all the information necessary to interpret themselves, and present
a final interface to a user to interact with. So this is all that's necessary
for a browser, which again, has no notion of what a contact is, to present
a useful user interface for a human to interact with, to make modifications
and updates to this particular resource.

And there is a little bit of a symmetry here, I noticed, between this idea
and object oriented programming, in that in a RESTful system, you're presented
with both the data and the operations on that data in one package.

So it's sort of like a class, in that sense. Like when you get an instance of a class
in an object oriented system, you're getting both the data associated with
that instance as well as the methods that are available on that instance.
**So I thought that was an interesting parallel**.

.. _hateoas_2021_10_22:

And finally, we come to the last and probably the most misunderstood constraint, which is HATEOAS
===================================================================================================

- :ref:`htmx_hateoas`

.. figure:: images/hateoas.png
   :align: center


And finally, we come to the last and probably **the most misunderstood constraint**,
which is HATEOAS or Hypermedia,
I like to say Hypertext just 'cause it's more familiar, As The Engine of Application State.

So what the heck does that mean ? Well, what that means is that the hypermedia
or the hypertext, or the HTML, depending on how concrete you wanna get,
**contains the state of the application**.

So given an entry point into my contacts database, say it's /contacts.
I don't need to know anything else about the state of any of the contacts
that are in that system. Rather, when I click on a link to view contact 42,
the state of that, of this contact is given to me in hypermedia.
So there's no special side state associated with this contact.
Everything that I can do with this contact is presented here in the HTML.
So the HTML is encoding exactly what I'm able to do in this app, there's no,
there are no flags or anything like that going on, there's no side state
associated with it. There's no model living on the side in my web application.

Rather, I've got my DOM, which is a representation of the parsed HTML, which
has come down from this remote system. And that DOM is what is providing me with
all the information about the state of the remote system.

**So everything is encoded in hypertext, and hypertext is therefore, the
engine of application state**.

If for some reason, this contact became, was put into a state where it
couldn't be archived, this link might not appear in that situation.
And that would be showing me in that case that okay, there's some other,
there are operations that are no longer available, or maybe there are
operations that have become available.
And that would all be encoded in the representation of this contact.
Rather than set as a flag on a local piece of data that I had in a data
store, say, a contact data store sitting in a JSON store on the side,
just pick something at random, that told me whether, what operations I could
make visible to my end user.


API churn
============

.. figure:: images/api_churn.png
   :align: center


So one interesting thing about this to note is this means that API Churn,
which you'll read quite a lot of complaints about online, doesn't really
matter nearly as much.

We can actually change the API around contacts pretty dramatically here, right?
We can add new operations, we can even remove operations. And to a first
order in any event, users will just see the new operations that are available
or that are no longer available on a contact and they can choose to use
them or not use them.

And so your API doesn't have to be as stable as it does if, for example,
someone is writing a JSON client against an API. And if you change that,
then suddenly that code that had been written against that JSON API
may no longer be valid.
That's one of the chief sources of flexibility in a truly RESTful system,
is this ability to have a single entry point and then allowing the hypertext,
the hypermedia to be the engine of application state for you.

**So, that's a really nice aspect of this, of RESTful systems, that I think
has been lost as REST has been dragged into the JSON community**,
and out of its native HTML or hypermedia environment.


13:05 So, if you consider the endpoint https://example.com/contacts/42/email
===============================================================================

.. figure:: images/consider_end_point.png
   :align: center

So, if you consider the endpoint, https://example.com/contacts/42/email
the client again, is the browser.
And it doesn't expect that that URL is there. It just renders a link.
It doesn't care. (chuckles) It doesn't care if the link, if this, in an
abstract sense if this endpoint exists or not. All it knows is hey, you
told me it did so I'm gonna render it to the end user, and that's it.


.. figure:: images/web_1.png
   :align: center


**So that is what REST and HATEOAS is. Is this extremely flexible mechanism
for presenting server state to an end user**.
And again, it was a description of this original web model, it was not
intended (chuckles) to be part of the discussion in the JSON world,
in the JSON API world.

I already said, wasn't its native, that wasn't the
native environment that it came out of. And one thing to note is the
Web 1.0 applications, so web applications built, what today might be termed,
the old way, naturally implement a RESTful architecture, and not always
the best way.


.. figure:: images/django_1.png
   :align: center

There were some shortcomings when it comes to HTML, that we'll address
here in a little bit, we'll talk about. But nonetheless, if you've ever
built a bog-standard Web 1.0 Django application, I'd like to congratulate you
because you have created a more complete RESTful API than probably about
99.9% of all JSON API developers. And JSON API developers are the people
who typically worry about RESTful-ness these days.

So somewhat ironic, if you're just building old school web apps, you're
doing a better job of REST than probably some very highly paid API developers,
who are wondering why they're using REST at all.

This original understanding of REST has been almost completely lost in the industry
=====================================================================================

.. figure:: images/angular_gafa.png
   :align: center

So, for historical reasons, and this is a long story that I won't get
into too much but this original understanding of REST has been almost
completely lost in the industry. And the reason for that is that the industry,
in an understandable effort to improve usability of web applications began
using JavaScript and AJAX to build more dynamic websites. Totally understandable.

There were definite usability issues with the original web. And I'm not
gonna claim that there weren't.
I will claim that it was a better implementation of the RESTful networking
architecture but I won't claim that it was the best user experience.


This has led to the rise of The Mighty JSON API
================================================

.. figure:: images/angular_gafa.png
   :align: center

And because of this, this has led to the rise of The Mighty JSON API,
which is what so many people use today to build their web applications.
And these are APIs that use JSON to communicate with the backend, and JSON,
I hope everyone is familiar with, looks something like this and this is
the JSON that might correspond to that contact that we were just looking at.
The contact HTML that we were just looking at. So you can see, look, it's nice,
it's small, feels a lot cleaner in some ways, right? This is great.


This is definitely not HATEOAS
=================================

.. figure:: images/no_actions_on_data.png
   :align: center

Everyone loves it. Everyone loves JSON. Certainly less data, maybe it's faster,
I don't know. I think people over-emphasize the size of these, of payloads,
depending on what the backend's doing but whatever, it is, it should be
a little bit faster anyways.
But note that **it doesn't encode any actions that are available on this data**.
So if you were in JavaScript and receiving this piece of data, you'd have
to know, okay well I've got this contact, now I need to know what to do,
where to call to do things to it, if I want to email this contact or whatever.
**You need to know what those endpoints are**.

It's not encoded in this JSON, is it ? So this is definitely not, (chuckles)
**this is definitely not HATEOAS**.


.. figure:: images/frustrated.png
   :align: center

We're not encoding actions in this JSON payload. And a quote from Roy is,
"I'm getting frustrated by the number of people calling any HTTP-based
interface a REST API.
Today's example is the SocialSite REST API." I don't think that exists
anymore, but. "That is RPC, it screams RPC and there's so much coupling
on display that it should be given an X rating."

This is what he's talking about. He's talking this type of API, just something
that happens to be over HTTP but **doesn't satisfy these other constraints
of the RESTful architecture and in particular, the uniform interface**.


.. figure:: images/we_could_fix_this.png
   :align: center

So, we could fix this if we wanted to. We could fix this by encoding some
links and the return to JSON. And so you'll see this pattern sometimes.
It pains me to say that the, the Wikipedia article on HATEOAS doesn't use HTML.
(chuckles) It uses JSON, and it gives something like this as an example,
kinda sad, but it does.

And what you would do here is you would have a links field in your JSON
object and then you have some URLs encoded in this manner that could then
be, at least make you feel like hey, you know, we're encoding the actions
on this thing.
So we're satisfying HATEOAS. But from a practical standpoint, if you think
about this, this is being consumed by some JavaScript consumer on the
front end. What's it gonna do with these links ?
Is it just gonna dump them into the UI ?

Probably not. And this is what we found over time is that these links have just,
they've been, they've proven difficult to produce and they have proven
not particularly useful.
The one case where I can think that they've ended up being useful is in
page data. I've seen libraries that use the paging links to actually, to
good effect internally, just to hide from users that they have to deal
with paging, which is somewhat nice.
But it's very, it's met with very limited success in the JSON world.

**And that shouldn't surprise us because JSON is not a proper hypermedia.
This is just not a hypermedia**.

And so it doesn't really make a lot of sense to apply REST to this world.
And that's what we're seeing.
That's what we're seeing the JSON API community stumble towards, I think.
They're recognizing, okay REST was you know, there were some things that
kinda worked with it, you know, that URL structure and using these HTTP verbs
or action types, but by and large, HATEOAS in particular is just not worked out.
We're not doing that, we're gonna do something else.

And they've moved on, and they're focusing on creating more expressive endpoints
with things like **GraphQL**. And so I expect that's what the other talk that's
being given on REST at this DjangoCon is about.
It's saying hey, **forget about that stuff, let's look at this tool instead
and trying to address some of these problems**. And that's understandable.
And so I applaud this realization that's occurring in the JSON API space.
I think REST and HATEOAS was cargo culted into the JSON API world. I think
once you went to a JSON API, you should be thinking RPC. And I'm not gonna
condemn RPC, a Remote Procedure Call, architectures, that's what Roy Fielding
was discussing earlier in that quote I showed you.

**I'm not gonna condemn that, but it's a different architecture than REST.
And then the web originally was**.
And so those are just different things and that's okay. They're different
architectures and they have their own strengths and their own weaknesses.

I do not however, even though I think we should applaud our friend in the
JSON API community **for finally coming to their senses** and recognizing
that REST was snake oil, when applied to JSON.


21:27 I don't think that that means that we should toss the concept of REST and HATEOAS into the dust bin
============================================================================================================

.. figure:: images/however_not_dust.png
   :align: center


**I don't think that that means that we should toss the concept of REST
and HATEOAS into the dust bin**.
Instead, I think what we need to do is we need to think about the original
model of the web and in particular, to think about hypertext.
And how hypertext works, and where REST actually works, where any person
who's just building a Web 1.0 basic web application, is building a RESTful
system.

Let's focus in there, let's focus in there. And you might say, well that's
great Carson, but I feel bad because I'm not building a View or a React
front end, and my clunky, old, Web 1.0 template-based server template-based
Django application kinda stinks. It's not, it doesn't have the inner
activity that I want.

.. figure:: images/web_1_bis.png
   :align: center


How should we address that instead ? Can that be addressed in a way that
allows you to remain in the **RESTful HATEOAS** style of architecture that
the web was originally built in ?


22:47 And I'm glad that you asked
===================================

- :ref:`tuto_html:html`

And I'm glad that you asked. I'm glad that you asked. So let's think about
why these older web apps had usability issues.
And my theory, my theory on this is that traditional server rendered web apps
have usability issues because **HTML was never completed as hypertext**.
What does that mean ? Well, what that means is that HTML reached a certain
point of development, and then for whatever reasons, people moved on to
other interesting stuff. **They never completed it**.

.. figure:: images/html_was_never_finished.png
   :align: center


They never made it possible to issue a PUT from HTML, even in HTML5, right ?
You can do it, but it's not officially supported.
And so they, for whatever reason, they just kinda stopped pushing HTML
forward **as a hypertext**.
But, good news, and I criticize JavaScript very often, but I have to say
thanks to JavaScript we can fix that.


.. figure:: images/we_have_fixed_that.png
   :align: center


And in fact, **we have fixed that. And we fixed that with a library called htmx**.

And this library attempts to address these shortcomings that have dogged
server rendered template-based web applications for so long.
And **it aims to complete HTML as a hypertext, as a hypermedia**.


So **htmx started**, it's actually fairly mature
==================================================

.. figure:: images/htmx_has_started.png
   :align: center


**So htmx started, it's actually fairly mature**.

It's enjoyed a resurgence in the last year (2020), I rewrote this library and
renamed it last year during the start of the COVID lockdowns.

But **it started back in 2013** as a library called :ref:`intercooler.js <intercooler>`
and it was based on jQuery and so last summer (2020), I re-wrote it without any
dependencies so now it's a dependency free JavaScript library that ironically,
you use to avoid writing JavaScript (chuckles) in your web applications.

.. figure:: images/htmx_attributes.png
   :align: center

And htmx augments HTML with :ref:`custom attributes <htmx_attributes>`, and
using these attributes, you can overcome the restrictions normally imposed
on you when you're doing HTML-centric development which by definition,
is a RESTful development, almost by definition.

You have to try hard. (chuckles) You have to try pretty darn hard to do
non-RESTful development if you're sticking within the HTML model.
So what does htmx fix that's gonna allow you as a Django developer to stay
in the original model, this original restful model of web development ?

.. figure:: images/htmx_ref_a_form.png
   :align: center

Well, ask yourself some questions. What's wrong with HTML ?
Well one thing that's wrong with HTML, maybe, is that **anchor tags** (<a>)
and forms (<form>) are the **only elements** that are able to interact
with the server.

And **that seems like an unnecessary restriction**.

Well **htmx removes that restriction**. It allows any element to make
server requests. And using attributes like :ref:`hx-get <hx_get>`, :ref:`hx-post <hx_post>`,
:ref:`hx-put <hx_put>`, or :ref:`hx-delete <hx_delete>` you can make any
element on your, in your document, make a request to the server.

Why should only click and submit events trigger them ? That's a good question
================================================================================

.. figure:: images/html_click_submit.png
   :align: center

Why should only **click and submit** events trigger them ? That's a good question.
So, in normal HTML, the only events that you can, that you have available
to interact with the user with are clicks on links, and then submits on forms.

Those are the only two events, of all the millions of events that occur
in a DOM that can trigger an HTTP request. **That seems a little silly**.

And htmx supports an attribute, :ref:`hx-trigger <hx_trigger>`, that let's
you specify, here's another event that I want to use to trigger this request.

And why should GET and POST be available ? It's somewhat crazy that in
this day and age, with the default HTML that's available to us, we can
really only use GET and POST. We have to use a JavaScript in order to
get access to PUT and DELETE and other things that we want to use, right?

So htmx again, kind of exposes all of this functionality to you in **hypermedia**,
you don't have to kick out to JavaScript to get at this stuff.
You can access it right there in **hypermedia using attributes**. And then
finally, and this is the big one I think from a usability standpoint.


In the original HTTP and HTML model, **you replaced the entire screen**
========================================================================

.. figure:: images/replace_entire_screen.png
   :align: center

   :ref:`hx-target attribute <hx_target>`

**In the original HTTP and HTML model, you replaced the entire screen**.

So if you click on a link or your submit a form, that's gonna replace
all the content with an entirely new document.
And **htmx takes that restriction away from HTML** and says hey, you can
issue a request to some URL and then we can place the returned content
from that request, **anywhere in the DOM**.

You can target an element, a different element using the :ref:`hx-target attribute <hx_target>`.
And so you can really say hey, go get this bit of content and then put it
into that div down there, or whatever you want to do.

And so again, **htmx expands HTML**
======================================

And so again, **htmx expands HTML**, what HTML can do, by removing this restriction,
by removing the restriction that the target has to always be the entire document.

And so htmx, you can see really goes through these four constraints that
were maybe accidentally part of the HTML when it was originally being built,
and **just one by one removes them**, giving you access to the full power
of HTTP, and building a RESTful system **within HTML**, using just these :ref:`attributes <htmx_attributes>`.


.. figure:: images/html_expected.png
   :align: center

   :ref:`tuto_html:html`

And so it's important to understand that the way that htmx works, unlike
other JavaScript libraries that you might use, is **it's expecting HTML back
from the server**.

We're staying within this original model of the web where communication
is happening via HTTP requests, that shockingly, are returning HTML (chuckles)
as the name might suggest.

And then that HTML is swapped into the DOM. So we've really returned to
this RESTful model. And at the same time, what we've done is we've allowed
users within this original RESTful model to build much richer user experiences
for our end users.

So we are, we're tearing apart this idea that to build good UIs, you have
to abandon RESTful-ness. We're saying no, you can build good UIs within
the RESTful model, you just have to do it in a different way, you have to
do it in an **HTML-centric way**.

And if you do that, then you get a lot of these benefits of the older
model development, **not the least of which, being the simplicity of it**.

But you're able to stay within that model but still provide the user experience
that users demand in the modern world.
And you can do that in Django just as easily as any other backend.

So, I want to go through a couple of examples for you
==========================================================

.. figure:: images/htmx_demos.png
   :align: center

   https://htmx.org/examples/

So, I want to go through a couple of examples for you so that you can see
exactly what's going on here.

I'm actually gonna do the first two, given where my time is. But we'll go
through the demos and I'm gonna show you exactly how HATEOAS is working here.

.. figure:: images/htmx_demos.png
   :align: center

   https://htmx.org/examples/


Click to Edit (https://htmx.org/examples/click-to-edit/)
===========================================================

- https://htmx.org/examples/click-to-edit/

.. figure:: images/click_to_edit_bis.png
   :align: center

   https://htmx.org/examples/click-to-edit/

So let's go to examples, and let's look at **Click to Edit**. And I want to
show you the code here. And so here we have a situation where we've got
a div that's representing a contact again, my favorite example.

::

    <div hx-target="this" hx-swap="outerHTML">
        <div><label>First Name</label>: Joe</div>
        <div><label>Last Name</label>: Blow</div>
        <div><label>Email</label>: joe@blow.com</div>
        <button hx-get="/contact/1/edit" class="btn btn-primary">
        Click To Edit
        </button>
    </div>


And note that in this HTML, there's a button (<button hx-get="/contact/1/edit" class="btn btn-primary">)
which issues a GET to a URL, and it's going to target this outer div,
and it's gonna **swap the outer HTML** (<div hx-target="this" hx-swap="outerHTML">).

Don't worry too much about the attributes, this talk isn't about the
details of htmx. Rather, what I want you to focus on is I want you to
focus on this concept of REST in here.

So this is a very RESTful situation that we've got here. **We're encoding
all of the actions directly in the HTML**. And when you click on edit,
the content that's gonna come back is a form, a familiar form tag
that's gonna do a PUT, as one might expect, maybe PATCH if you wanted to
quibble about things.

::

    <form hx-put="/contact/1" hx-target="this" hx-swap="outerHTML">
      <div>
        <label>First Name</label>
        <input type="text" name="firstName" value="Joe">
      </div>
      <div class="form-group">
        <label>Last Name</label>
        <input type="text" name="lastName" value="Blow">
      </div>
      <div class="form-group">
        <label>Email Address</label>
        <input type="email" name="email" value="joe@blow.com">
      </div>
      <button class="btn">Submit</button>
      <button class="btn" hx-get="/contact/1">Cancel</button>
    </form>

To a URL that is going to update this contact. And again, it's going to
replace the, this form with whatever the response is.

So let's go down here and look at a demo and I want to show you what the
requests look like, so this is the initial state of this demo.
And when I click to edit it, I'm gonna issue a GET to this URL, and I'm
gonna get back this form, and **that form has now been swapped** in the
place of that UI that we had previously, of the initial state, right ?

Initially we had a div, and that has now been replaced with this HTML,
and just this HTML.

Note that there's no document, there's not a full document here.
We've just gotten back a snippet of HTML. And that HTML encodes all the
actions that are available on this contact. Well great.

So we can update this from Joe to Joey, and we can click submit, and sure
enough, we issue a PUT to a given URL and we get back the new representation,
the new updated representation of that resource. All very, very RESTful.

.. figure:: images/put_contact_1.png
   :align: center

   https://htmx.org/examples/click-to-edit/


Very, very RESTful. And so I think you can see that this is great, we're
staying within the original model of the web, but we're not, our scroll
state isn't getting completely you know, nuked when we hit enter.

It's all in line, and we're able to edit, do this all in line because we
have htmx to help us drive this user interaction with the server in a way
that's not available in standard HTML, so we're able to do a better UI
while still sticking with this original RESTful model of the web.
So that's one example. And a pretty good one.


33:25 A more significant example that I really like, is **active search**
===========================================================================

.. figure:: images/active_search.png
   :align: center

   https://htmx.org/examples/active-search/


An even better example, because some people say, oh that's cute, you know,
htmx might be good for a small project, but what about larger projects ?

Well, **a more significant example that I really like, is active search**.



And I show you this again, I don't want to focus too much on the details
of it because there's a little bit here. There's a little bit more here
than I'd like to go into at this point in detail, but I want to show you
what is possible **within the hypermedia RESTful model**.

.. figure:: images/active_search_bis.png
   :align: center

   https://htmx.org/examples/active-search/

So let's quickly run through this and then I just want to show you, just
do you believe me that it's possible to write **very good UX in this RESTful model**.


::

    <input class="form-control" type="text"
           name="search" placeholder="Begin Typing To Search Users..."
           hx-post="/search"
           hx-trigger="keyup changed delay:500ms"
           hx-target="#search-results"
           hx-indicator=".htmx-indicator">

So here we have an input, it's got a bunch of junk on it, but what it
does at the end of the day is **it issues a post to search**, and the trigger
here is on a keyup, so when a keyup occurs. And it has a couple of modifiers.

First of all, the input value has to change for it to issue a request.
And then secondly, it's gonna delay before it issues a request, 500 milliseconds.
And that's gonna be something called **debouncing this request**.

So what means is every time you type a key in this input, it's not gonna
send a request, that would obviously nuke the server, we don't want to
do that. And do this is an htmx way to say **hey, wait until the user stops
typing for 500 milliseconds and then you can issue a request**.

We're gonna target a search result div somewhere else, maybe something
with a, it might not be a div with **something with the ID search results**,
and then there's some indicator stuff in here which you don't need to
worry about, just some niceties. But what's really nice about this is
here's a live demo.

.. figure:: images/search_contacts.png
   :align: center

   https://htmx.org/examples/active-search/


And again, we're starting with that, we're starting with that HTML in
this initial state, and then if I type, A, it's gonna search, and sure
enough you're gonna get some results back.

You're gonna get everyone who has an A in either their first name, last name,
or email. And note that it replaced this content that was below this input
with all of these contacts. Pretty cool. Pretty cool, and I was able to
do that just by hitting the A key in here.

And if I hit E, it's gonna filter it down even further, all right ?
And what is it? A, E, what are we looking at here? A, E, dot? (chuckles)
If I do AE., sure enough it filters it down to anyone with a, apparently
vitae is in this dataset. (chuckles) And if I move the arrow key around,
I'm not gonna be issuing requests, but if I delete, I am gonna issue a request,
it's changed, and so this is going back to that original search.

And if I do AE., you can see that it waited until I was finished
to issue that request. It didn't issue three requests every time
as I typed it. And so you can see that what's coming back again, are just
snippets of HTML, and using just snippets of HTML, as well as some features
that are available in htmx, we're able to stay in a RESTful model and
present what I hope you agree is a **fairly sophisticated and dynamic user
interface to the end user, all right ?**

Don't worry too much about the details. Again, the idea here is just that
using this, using hypermedia, using HTML with just a little bit more juice
added to it **allows us to build some really interesting user interfaces
within the RESTful model**.

Allows you, the Django developer to keep using Django the way you want to
to produce this HTML from the backend, but also satisfy this need for more
elaborate and more interesting user interfaces.

37:15 So, htmx and Django
=============================

.. figure:: images/htmx_and_django.png
   :align: center

   https://x.com/stschindler/status/1434499868236685313?s=20


So, **htmx and Django**. All these demos that I've shown work great on Django.
Django is a wonderful backend for producing HTML. And htmx works great with it.
And so if you use htmx, suddenly you can use all of these tools that you
are familiar with in Django that you've used for so long for producing HTML
on the server side to interact with your user. And this was a, someone
took a crack at this actually recently put a tweet out saying,

::

    "We rebuilt a client's webapp with server-side rendering in Django plus htmx
    as a proof of concept.

::

    We're 100% sold and we'll use it as our default stack in the future, previously React."

**Warms my heart to see this**, and I asked and he said that it took about 25%
of the time that a normal Django and React combination takes in order to
re-write the app. Just because it's, with htmx you're going with the grain
of Django, you're using the tools that Django's built up over more than a decade
to do effective server-side rendering. And it all just works.

.. figure:: images/htmx_and_django_2.png
   :align: center

   https://x.com/stschindler/status/1434499868236685313?s=20

So, great. With htmx, **you can stay in that preferred backend environment**.

You can write Python, you don't have to write a bunch of JavaScript on
the front end and the Python on the backend and have two different languages.

Instead, we're using the RESTful model, **we're exchanging hypertext** with
the backend and you get to use the tools you want to use as a Django developer,
and still provide to your end users excellent dynamic user experience.


.. figure:: images/htmx_and_django_2.png
   :align: center

   https://x.com/stschindler/status/1434747663208132608?s=20


And so, and this is where **he mentioned that it took about a quarter of
the time**. So what I want to conclude with is that it's okay to not use
JavaScript in JSON.

I went on Python, I went on "Talk Python" and one of the comments was
that people often feel bad, Python developers feel bad because they know
they should be learning JavaScript.

Well I don't agree with that. I think that you can learn htmx and it's not
that much code, and you can spend most of your time in Django using the
mature tools and the deep extension system the way it was originally designed,
and still provide your end users excellent user experience.

**You don't need to, you don't need to adopt a crazy JavaScript based front end.**


.. figure:: images/htmx_and_django_restful.png
   :align: center

You can use HTML. And if you do that, you're actually gonna be using
REST and HATEOAS, with all the advantages that come along with that without
even really thinking about it very much. You just keep doing what you're
doing with a **few new tricks added to HTML** and you're **building a RESTful system**.
**You're using hypertext as the engine of application state.**

And so I wanna end this talk with a plea **that we let the web be the web**.

There's a time and a place for RPC-based applications, for these really
crazy highly stateful web applications that are being built today, but
there's also a lot of times and places, maybe more so where the original
web model makes more sense. **It's simpler, it's more dynamic and it's easier
to keep stable and with htmx**, you can provide **excellent user experience**
even though you're staying in that model, even though you're staying in
that Web 1.0 model.


And in addition to that, let's let Django be Django.
========================================================

And in addition to that, let's let Django be Django.

Let's not turn Django into just a DOM JSON producer that just sits there,
wondering when node is going to finally drop the ax on its neck.

No, Django has great features. You guys love Django, use Django, use those
great features. Don't feel like you've got to turn, just turn Django into
this dumb backend that produces JSON, it doesn't have to be that way, so.

.. figure:: images/out_of_touch.png
   :align: center

This is my, I always keep this in the back of my mind when I'm working
on htmx because it's so different. (chuckles) **But I do think it's the
rest of the internet that's wrong on this stuff guys**. You're allowed to
use Django and you can build great user interfaces with it **and don't
feel bad, (chuckles) don't feel bad**.

So I hope that this has shown you, first of all, what REST is, how it
applies to Django and how htmx can let you build excellent user experiences
while staying within that model.

So, thank you very much for listening and I hope you have a great day and
I hope the rest of this conference treats you really well. If you have
any questions or comments you're welcome to join the htmx Discord.

My name's Carson again, but you can go to htmx.org/discord and I'm on
there pretty much all the time. And you can tell me why I'm wrong
or you can agree with me, or something in between those two.

I'm a pretty good sport about all that stuff. All right, have a good one
and thank you for listening. And again, thank you to the organizers of
DjangoCon for giving me a chance to explain what REST and HATEOAS is,
and why you might consider using it for your Django development.

Thanks a bunch, bye-bye.
