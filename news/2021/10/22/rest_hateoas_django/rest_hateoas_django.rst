
.. _carson_gross_2021_10_22:

======================================================================================================================================================
Talks: Friday, 2021 October 22 **REST, HATEOAS & Django - It's OK to not use JSON... or Javascript** by Carson Gross (https://x.com/htmx_org)
======================================================================================================================================================

- :ref:`carson_gross`
- https://x.com/htmx_org
- https://2021.djangocon.us/talks/rest-hateoas-django-it-s-ok-to-not-use/
- https://bigsky.software/



On youtube
=============

- https://www.youtube.com/watch?v=L_UWY-zHlOA

Introduction
==============

- https://x.com/fielding
- https://roy.gbiv.com/untangled/2008/rest-apis-must-be-hypertext-driven

In this talk we will revisit the basics of REST (REpresentational State Transfer),
a term coined by `Roy Fielding <https://x.com/fielding>`_ to describe the web network architecture,
in contrast with older client-server network architectures.

In particular we will focus in on the :ref:`Uniform Interface <the_uniform_interface>` constraint and
get to the bottom of what that means, discussing each of the following:

- :ref:`Resource identification <resource_identification>` in requests
- :ref:`Resource manipulation <resource_manip>` through representations
- :ref:`Self-descriptive messages <self_descriptive_messages>`
- :ref:`Hypermedia as the engine of application state (HATEOAS) <hateoas_2021_10_22>`

with a particular focus on the last two.


REST: Wandering in the Desert
=================================

After gaining a deeper understanding of REST, we will look at the history
of REST in development.

We will look at how the term was appropriated for use in XML APIs, when
AJAX first emerged, and how it was then retained when APIs switched to JSON.

I will demonstrate why this is inappropriate by focusing on the ideas
of the self-descriptive messages and HATEOAS constraints of REST.

We will look at recent developments in the JSON API space and how they
are moving away from the concept of REST.

REST: A New Hope
======================

Finally, having established that REST does not apply well in the JSON
API world, we will ask where it is appropriate and answer: in hypermedia systems.

And, it will turn out, tools like Django are wonderful for producing
hypermedia systems ! In reexamining the hypermedia approach, and how it
trivially satisfies the REST-ful architecture, **we will look at a tool,
htmx, that can help you create more advanced user experience while
staying within the hypermedia architecture**.

It’s OK to not use JSON… or Javascript!

Carson Gross
=============

- https://x.com/htmx_org

At Big Sky Software we look for hot new industry trends and then build
the opposite of that…


Transcription
===============

.. toctree::
   :maxdepth: 3

   transcription


Comments
============

Ollie (2021-10-25)
---------------------

- https://discord.com/channels/725789699527933952/725789747212976259/902274937992138842

Ollie — Hier à 21:18
Hi all!  I just watched Carson's talk (https://www.youtube.com/watch?v=L_UWY-zHlOA)
from DjangoCon and liked it a lot. I've got a few conceptual questions:

1. I like the idea of enabling standard HTML elements to trigger
requests etc. That said, that seems to be a slippery slope of building
a website that cannot fall back to default behavior when it's used without
JavaScript enabled. This can be seen in the Active Search example:
https://htmx.org/examples/active-search/, which doesn't work at all
with JS disabled. Does anyone of you have any experience in trying
to strictly use HTMX for progressive enhancement? Should I be able
to simply wrap the <input /> element into <form /> elements properly
and can expect the browser to just work fine with that if JavaScript
is disabled?

2. Hotwire's (https://hotwired.dev/) default behavior in response
processing seems to be to extract the content of the turbo frame the
interaction originates from, from the response. That enables me to
keep the rendering code untouched, still render the full page and
Hotwire still optimizing the default replacement.
HTMX has hx-select to achieve something similar. I.e. if I decorated
an HTML element with the id foo to hx-select="#foo" that would result
in pretty much the same behavior. Is that understanding correct?

3. Hotwire's concept of Turbo Streams (https://turbo.hotwired.dev/handbook/streams)
seems to find a similar idea in hx-swap-oob (https://htmx.org/docs/#oob_swaps).
A fundamental difference though seems to be that with Hotwire, the
response defines the type of swapping (e.g. replace VS. append).
With HTMX, I don't seem to be able to define the swap mode for OOB swaps
in the response? Did I miss something here? (modifié)


1cg — Hier à 21:54 response
++++++++++++++++++++++++++++++++

- https://discord.com/channels/725789699527933952/725789747212976259/902283995000209419

1) yes, it's a slippery slope and you have to work to ensure that you
are using htmx in a progressively-enhanced manner if that's what you want.
Some patterns (e.g. lazy load) simply won't work without javascript

2) Yes, you are understanding it correctly.  htmx is more manual
and low level than hotwire, attempting to build up from HTML rather
than provide a total, transparent solution.

3) you can use any sort of valid hx-swap value (e.g. beforeEnd)
for hx-swap-oob: https://htmx.org/attributes/hx-swap-oob/
Again, you will find it more manual than hotwire


menip (2021-10-26)
-------------------

- https://discord.com/channels/725789699527933952/725789747212976259/902351921589911602

menip — Aujourd’hui à 02:24
This was a great talk ! As someone that didn't touch computers until
well after web 2.0 came around, the history presented was quite insightful.


.. _comment_martin_fowler_2021_10_26:

cg — It really is a shame 2021-10-26
+++++++++++++++++++++++++++++++++++++++

- https://discord.com/channels/725789699527933952/725789747212976259/902374879628759070
- https://www.martinfowler.com/articles/richardsonMaturityModel.html

That's fantastic to hear. **It really is a shame** that the thought leaders
who pushed REST in JSON APIs (e.g. https://www.martinfowler.com/articles/richardsonMaturityModel.html)
never went back to reexamine the concept.

It all makes a ton more sense in terms of hypertext.
Maybe the tides are turning...


