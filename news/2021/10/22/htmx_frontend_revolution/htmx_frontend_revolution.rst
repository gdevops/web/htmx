
.. _thomas_gunter_2021_10_22:

=================================================================================================================================================
Talks: 2021 Friday, October 22 **HTMX: Frontend Revolution** by Thomas Güttler (https://x.com/guettli)
=================================================================================================================================================

- :ref:`guettli`
- https://x.com/guettli
- https://2021.djangocon.us/talks/htmx-frontend-revolution/
- :ref:`frow_essay_guttli`
- https://docs.google.com/presentation/d/12dgaBnUgl4cmEkiOhUJL5hsbGQ6hB5sslDuozmBjVUA/edit#slide=id.geee3da42b7_1_231


.. figure:: images/back_to_the_roots.png
   :align: center

   https://docs.google.com/presentation/d/12dgaBnUgl4cmEkiOhUJL5hsbGQ6hB5sslDuozmBjVUA/edit#slide=id.gc7189c61bd_0_32


On youtube
============

- https://www.youtube.com/watch?v=z0yPTv15Fjk
- https://www.youtube.com/c/DjangoConUS

Essay
======

- :ref:`frow_essay_guttli`


About This Talk
======================

I developed my first CGI script in 1998. Since that day I have seen a lot
of hypes coming and going.

The talk will consist of three parts:

- My humorous odyssey of trying to find a great frontend framework
- Retrospective: What do I want from the frontend ?
- I finally found: HTMX: HTML Fragments over the wire
- How to use HTMX with Django

Besides simplicity, good Web Vitals performance (SEO) is a benefit of
this method.

The talk will express my personal opinion, feelings and fun. It won’t
dive into technical details.


Thomas Güttler
================

I am Thomas Güttler. Born 1976 in Germany. I like web development and
try to avoid JS as much as possible.

Inventor of the term `Frow (Fragments over the wire) <https://github.com/guettli/frow--fragments-over-the-wire>`_


.. figure:: images/htmx_for_frow.png
   :align: center

   https://docs.google.com/presentation/d/12dgaBnUgl4cmEkiOhUJL5hsbGQ6hB5sslDuozmBjVUA/edit#slide=id.gd7a4b51cfc_0_23


Transcription
================

.. toctree::
   :maxdepth: 3

   transcription/transcription
