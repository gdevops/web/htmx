
.. _transcription_thomas_gunter_2021_10_22:

=================================================================================================================================================
Transcription: 2021 Friday, October 22 **HTMX: Frontend Revolution** by Thomas Güttler
=================================================================================================================================================

- :ref:`guettli`
- https://www.youtube.com/watch?v=z0yPTv15Fjk

Introduction
=============

.. figure:: images/htmx_intro.png
   :align: center

Hi and welcome here to Django Conference U.S. 2021. I'm :ref:`Thomas Guttler <guettli>`
from Germany working for descript.
And today I want to talk about HTMX Make web development simple and fun again !


My opinionated Odyssey
=========================

.. figure:: images/odyssey.png
   :align: center

So this won't be an academic, too detailed, or dry talk, **I want it to be
personal, opinionated, and fun**.

I studied computer science roughly 20 years ago, and **finding the right
frontend framework is really hard today**.



Start of the Odyssey
====================


.. figure:: images/start_of_odyssey.png
   :align: center

In the past, things were much easier since you had less options to choose from.
1999, I wrote some simple CGI scripts with Shell and Perl on Linux.
It was simple and fun. Not fun were the languages.

- Perl was ugly.
- Shell, not secure and not reliable,
- and Java and C++ too complicated.
- 2001, I found Python.
  It was fun, easy to read.


**2008, I started to use Django**
====================================

**2008, I started to use Django**, and I don't regret this since 13 years.

.. figure:: images/hypes_past.png
   :align: center

I have seen a lot of hypes coming and going. To name some, Perl, XML+XSLT,
Java Applets, Macromedia/Flash.

And I am deeply relaxed and guess that some things we love today will be
called deprecated or **technical debt** soon.


In 2014, I came across Angular GAFA
======================================



.. figure:: images/angular_gafa.png
   :align: center


In 2014, I came across Angular and I thought, "Wow, that's the future."
The server sends JSON to the browser, and you can build real applications.
But customers kept us busy doing the things we did before, Django with
some jQuery.

So I did not really use Angular in a day-to-day basis, and I somehow stored
in my brain JSON is clean and HTML is dirty, but this equation is completely
butchered. But more on this topic later.

Six years went by in a flash, Angular is not dead, but if you can start
from scratch, **it is very likely that you won't use Angular today**.


.. figure:: images/angular_gafa_trends.png
   :align: center

In the picture you see the Stack Overflow tech trend and you see clearly
from the year 2013 to 2021 that Angular is in a big downtrend today.

Last year I thought, hey, let's look at the modern way to do web development
with frontend tools again and I looked at React and Vue.


.. figure:: images/react_view_gafa.png
   :align: center

First I played around with React and you see some code snippet here and somehow
I liked it because it was a mixture. So there is HTML and there is code
and it is combined with something I like because as a developer you can
change all moving parts in one place, it's somehow a little bit like PHP.

But like PHP executed in a browser. Then I had a looked at Vue, that's
nice too. A little bit more clean and I think both ways are great if you
want to do a web-based real application, but somehow I did not enjoy it
because **you have then two parts, you have the backend and the frontend
and it does not feel very simple and straightforward for me**.

A small spare time project
============================

.. figure:: images/small_project.png
   :align: center


In 2021, so this year, a friend told me about how she manages her work.
She has a vegan lunch service organized in Excel, it was a mess.

Up to now, the people ordered via E-mail and WhatsApp and the Excel sheet
was edited by her for every order by hand. Since it was a spare time project
and not for money, one thing was important for me, it should be fun,

I want to help her, but on the other hand, I need to love the technology
at stake, and that's very different from a business context.

I knew I would be coding in the evening after the kids went to bed and this
means only 30% of my mental energy will be available.
So it was very important for me to reduce cognitive load. So this is my
spare-time project and I had far too many options, analysis paralysis,
so first I created a list and then a spreadsheet of all my options.

.. figure:: images/spare_time_project.png
   :align: center

For the backend, it was easy, I want to use Django, but with the frontend,
I was not sure. I don't get paid by the hour, I want frontend and backend
to be easy and I don't have a frontend team.

.. figure:: images/front_end_easy.png
   :align: center


And nobody says to me, "Hey, Thomas, use tool A "for task B..."
So this freedom of choice is sometimes difficult. So I had the frontend
blues, I still haven't found what I'm looking for.


What do you want, what do you like ?
======================================

.. figure:: images/what_do_i_like.png
   :align: center

So I sat down and relaxed and said, om, so Thomas, what do you want, what
do you like ? First of all, I like the Django forms library, I think it's
a cool way to create HTML forms on the server and send later, validate
the user input which you get from the browser.

And I like the Django ORM, I think that's a cool way to work with a database,
I like HTTP, HTML and CSS and I want good WebVitals, so fast page loads,
that's what I want.

And JavaScript, I don't like it so much.


.. figure:: images/what_to_improve.png
   :align: center

And then the next question is, what do you want to improve ? So the full
page reload after submitting a form, this post, redirect, get pattern, no,
I don't like this, I think that's outdated in the 21st century, no.

And jQuery, no, that's outdated too.

And do it yourself, so write a JavaScript library for submitting and reloading
snippets, I don't think I'm the right one to implement the JavaScript library.

.. figure:: images/one_page_several_parts.png
   :align: center


So let's paint a picture of what's on my mind. So I have one page, and on
this page there are several parts, so we have your part one, part two,
part three. And I want to be able to update each part on its own.

.. figure:: images/update_parts.png
   :align: center

So for example part one could be a news ticker, part two could be complex
form with a lot of input fields and part three could be a chat or something
like this. And if you paint this picture to somebody then you get the
answer immediately, they will tell you, hey, Thomas you need a single page
application.

.. figure:: images/spa.png
   :align: center

But wait, wait, wait, thinking fast, thinking slow, let's wait, I have
several pages, I have several pages and each page should be some kind
of interactive, I don't have one page, I have several pages.

And if you reject some modern frontend frameworks, people start to argue,
they will tell you, I hate form validation and the client side is great,
you can do this with JavaScript, but wait, wait, wait, wait.


.. figure:: images/form_validation.png
   :align: center

I need to validate the data on the server side, and an evil hacker could
send me any kind of data, and working around the client side validation,
so that's different, I need to validate on the server, and if I do so,
it's additional work to do the validation on the client side, it makes
no sense for me.

So HTML5 has already some cool stuff, so you can do some minimal validation
on the client side and yeah, maybe I will later add some validation on
the client side, and some people argue, hey, that's great, so you can
write JavaScript validation which gets executed on the client side and which,
if you use JavaScript on the server, you can use the same validation there,
yes, of course, but somehow for me, that's not making big sense,

I have my Python on the server side and I'm really happy with it, I'm fast
with it and it makes no sense for me to use JavaScript on the client and
on the server.

No, no, you can do it if you want to, but I won't do it. And the next big
hype is server-side rendering.


SSR (server-side rendering) Hype
====================================

.. figure:: images/ssr_hype.png
   :align: center


I looked at server-side rendering in React and Vue and it's possible, it
works, it's fine, but all this hydration and rehydration, no, no, no, it
does not make sense to me, I want good web vitals and of course if the server
sends HTML directly to the clients, that's super fast, you don't need to
load JavaScript libraries to get the first page, so are there no alternative
to server-side rendering today?

.. figure:: images/ssr_gafa.png
   :align: center

So I searched around and tried to find an answer and I came to this page,
it's from Google about web development. About very basics, it's called
rendering on the web and here you can see five columns and the first column,
that's very old, traditional way of web development, so we create HTML
on the server and send this HTML to the browser.

On the right hand side, this column is full client side rendering, it's about
modern React or Vue-based applications.

And if you read this article, then at least I think the first column is,
**the old way of doing web development has a lot of benefits**.

So I still had the frontend blues, am I too old, am I too stupid ?
Am I maybe too lazy ? Don't know. Pa-pam !

And then comes the revolution unpoly + htmx
=============================================

- https://x.com/triskweline

.. figure:: images/revolution.png
   :align: center

And then comes the revolution. By chance I came across `Unpoly <https://unpoly.com/>`_.
A big thank you to `Henning Koch <https://x.com/triskweline>`_, the author
of this library, Unpoly.

He created a really cool comparison where he compared the page speed of Angular
with his approach. His approach is **sending HTML fragments over the wire**.

First, I was unsure, isn't this considered dirty, what will people say
if I go this way ? Fragments over the wire is a real revolution in the
truest sense of the word because it means you evolved something which
has been the state in the past.

It took me some time to get the useless equation, JSON is clean, HTML is
dirty out of my head.

There are things which are clean, HTML, CSS, SQL, Python, all of these things
are very cool and clean.

.. figure:: images/which_libs.png
   :align: center

Analysis paralysis again, **which fragment over the wire framework should
I choose ? Unpoly, Hotwire, HTMX ?**

I was meandering forward and backwards, but the cool thing about a spare
time project, there is no pressure, no deadline, you have freedom, you
can do relaxed decisions.

In business context, this decision would be much harder, there's a deadline
and you need to convince co-workers and employee or client to try out
something new that's sometimes really hard.

I had the freedom of choice and my choice was HTMX
=======================================================

**So I was lucky, I had the freedom of choice and my choice was HTMX.**


12:31 Partial updates with HTMX
=====================================

.. figure:: images/partial_updates.png
   :align: center

So again, this picture, I have one page and several parts on this page.
And **now let's have a look at how partial page updates work with HTMX**.

So you have some plain old boring HTML here, you have a button and some
extra attribute, these attributes are new, these are from the HTMX
namespace, so they start with a prefix **hx**.
:ref:`hx-get <hx_get>`, here's an URL and a swap method, for example,
outerHTML.

And if you press this button, what happens ?
The library access this URL, so HTMX makes an hx-call to the URL, fetches
some HTML fragment, for example this Simple div here, this is the response
from the server. And this snippet gets inserted into the, down inside your
browser.

And you can define a swap method for example, outerHTML, so this means
you have a button here and if you press this button, **this whole button
gets replaced by this simple div**.

And this is a basic way of how HTMX works and this basic pattern makes a
lot of things possible.

13:55 So once again, how does it work ?
===========================================

So once again, how does it work ?

.. figure:: images/fragments_over_the_wire.png
   :align: center


- First step, the user clicks on a button,
- seconds step, HTMX receives click event in the browser,
- step three, HTMX does an HTTP GET or POST to the URL.
- And then the server sends a fragment, also called snippet to the browser
- and HTMX merges this fragment into the page.

**Yeah, that's it.**

HTMX is declarative like HTML, itself
======================================


.. figure:: images/htmx_is_declarative.png
   :align: center

And while creating these slides, I realized that HTMX is somehow different
to other ways of web development, **it's declarative**.

And that's something I like, HTMX is declarative like HTML, itself.

HTMX is declarative like CSS, like SQL, it's declarative, like the Django
admin or maybe like Terraform, or K8s manifest.

So declarative is somehow, a very professional way of working and doing
things.
Here are some facts about HTMX,

HTMX
======


.. figure:: images/htmx.png
   :align: center


HTMX is written in JavaScript so that you don't need to write JavaScript
anymore.

It works with every language on the server side, this means you can use
Python, Go, Rust, whatever you want on the server side.

The library is very small, and the initial page you send from the server
to the client, usually just contains HTML with some additional attributes.

So this can give you very good web vital scores because the Largest
Contentful Paint measures the loading performance and since no JavaScript
is involved during this loading, the LCP score is usually very good.

Because you only need the HTMX things if you have some kind of interactivity,
but **the first load is completely without HTMX and this is then very fast**.

So let's have a look at some example here.


So let's jump to the demo right away (htmx examples)
======================================================

.. figure:: images/htmx_examples.png
   :align: center

   https://htmx.org/examples/


And all these example are available online on htmx.org/examples.
Let's have a look at click to edit.


.. _click_to_edit_thomas:

**click to edit (https://htmx.org/examples/click-to-edit/)**
===============================================================

- https://htmx.org/examples/click-to-edit/

.. figure:: images/click_to_edit_thomas.png
   :align: center

   https://htmx.org/examples/click-to-edit/

So let's jump to the demo right away. We have here first name, last name
and an email address, and if I click here, click to edit, **I suddenly have
a form**, now I fill in this **form** and I can submit this **form** and that's exactly
what I was looking for.

So we have a part on this page and **this part is interactive and that's cool**.

All these other things like scroll position and so on, stays the same.
So that's what I was looking for and the cool thing is that all these
examples have this show here and there you can see the log, so you can see
what happens behind the scenes.


.. figure:: images/click_to_edit_thomas_debug.png
   :align: center


::

    <div hx-target="this" hx-swap="outerHTML">
        <div><label>First Name</label>: Joe</div>
        <div><label>Last Name</label>: Blow</div>
        <div><label>Email</label>: joe@blow.com</div>
        <button hx-get="/contact/1/edit" class="btn btn-primary">
        Click To Edit
        </button>
    </div>

At the beginning we had an initial state, we had a div, with an :ref:`hx-target <hx_target>`
of this and a swap method and we had a button and this button here is
:ref:`hx-get <hx_get>` and then an URL.

And if I click this button, click to edit, what I did, then send HTMX
does an hx call to this URL and fetches what it gets from, and HTMX gets
in fragments.

So server sides sends HTML to the browser, but not whole HTML, **it's just
a fragment**, so let's have a look at the second step.

::

    <form hx-put="/contact/1" hx-target="this" hx-swap="outerHTML">
      <div>
        <label>First Name</label>
        <input type="text" name="firstName" value="Joe">
      </div>
      <div class="form-group">
        <label>Last Name</label>
        <input type="text" name="lastName" value="Blow">
      </div>
      <div class="form-group">
        <label>Email Address</label>
        <input type="email" name="email" value="joe@blow.com">
      </div>
      <button class="btn">Submit</button>
      <button class="btn" hx-get="/contact/1">Cancel</button>
    </form>

That's the response from the server, and in this response, you see this
says from here, and you see there is no HTML head tag, there's no head or
body tag, **it's just a fragment**, and it's not JSON or something like this,
it's just **plain HTML**, and it's an **HTML fragment**.

Okay and this is a form, like you know it, it's an input, type, text for
example here and this form here has :ref:`hx-put <hx_put>`, this means
if I submit this form, HTMX sends the data via HTTP put to this URL.

Okay, this way I can update the state on the server and the server sends
me some data back.

For example, roughly the same div again, but this time, this time my data
is in the div here and that's exactly what I was looking for.

I'm very happy
----------------

It took me some time to find it really, several weeks until I found the
library I want to do my frontend work with and now I found it, **I'm very happy**.
So...

**cascading selects (https://htmx.org/examples/value-select/)**
==================================================================

- https://htmx.org/examples/value-select/


.. figure:: images/value_select.png
   :align: center


So let's have a look at our second example here, **cascading selects (value select)**.
I guess most developers already implemented some kind of cascading select.

It's quite straightforward, so you have first the first select and then
the second select.

And if you choose for example Toyota in the first, the second one updates
and now you only see options which correspond to this first one.

So if I choose BMW here, sends the model, list updates to BMWs.

So that's very simple and straightforward and **the cool thing is that you
don't need a single line of JavaScript to implement this here**.

So HTMX is very flexible, also it's quite simple. Let's have a look at
how it all works.

::

    <div>
        <label >Make</label>
        <select name="make" hx-get="/models" hx-target="#models" hx-indicator=".htmx-indicator">
          <option value="audi">Audi</option>
          <option value="toyota">Toyota</option>
          <option value="bmw">BMW</option>
        </select>
      </div>
      <div>
        <label>Model</label>
        <select id="models" name="model">
          <option value="a1">A1</option>
          ...
        </select>
    </div>


You have the initial state. Here, it's a select, and here this hx attribute,
get, so if you select something here, this URL gets called and the new models
are fetched from this URL and this time the :ref:`hx-target <hx_target>`
is a different one, so not, this select gets updated, but the second one
here says, with ID models, gets updated by this target selector.

Yes, and that's all you need to implement cascading select, it's very
straightforward, **it's declarative, no single line of JavaScript on your
side, I like it**.


.. _infinite_scroll_thomas:

**infinite scroll (https://htmx.org/examples/infinite-scroll/)**
===================================================================

- https://htmx.org/examples/infinite-scroll/
- :ref:`django_htmx_fun_guttli`

.. figure:: images/value_select.png
   :align: center


So let's have a look at our third example, `infinite scroll <https://htmx.org/examples/infinite-scroll/>`_.

So how does it work ?
No, no, let's wait, let's have a look at it in action, so I scroll down
and you see this loading indicator again,

::

    <tr hx-get="/contacts/?page=2"
        hx-trigger="revealed"
        hx-swap="afterend">
      <td>Agent Smith</td>
      <td>void29@null.org</td>
      <td>55F49448C0</td>
    </tr>

I scroll down, I see the loading indicator and new data get fetched from
the server and how does it work ? The magic is behind :ref:`hx-trigger="revealed" <hx_trigger>`.

This means as soon as this HTML snippet is visible for the user, then it
triggers and says, :ref:`hx-get <hx_get>` URL gets called, the server
sends an HTML fragment to the browser and the HTMX inserts and swaps this
fragment into the bottom and here's the method **afterend**. Okay, that's all.

- So the trigger revealed happened.
- Server sends data to HTMX,
- HTMX inserts this new data afterend.

So again, no JavaScript involved, it's just declarative, it's straightforward
and it's very simple, **I like it**.


**CSS animations (https://htmx.org/examples/animations/)**
===============================================================

- https://htmx.org/examples/animations/


.. figure:: images/css_animations.png
   :align: center


So let's have a look at our last examples, `CSS animations <https://htmx.org/examples/animations/>`_.

Let's have a look at the `demo first <https://htmx.org/examples/animations/>`_.

::

    <style>
    .fade-me-out.htmx-swapping {
      opacity: 0;
      transition: opacity 1s ease-out;
    }
    </style>
    <button class="fade-me-out"
            hx-delete="/fade_out_demo"
            hx-swap="outerHTML swap:1s">
            Fade Me Out
    </button>

So we have this button here and I press it and the button fades away.
Isn't it cool ? And it works very straightforward and simple.

You have this CSS class on this button which is called **fade-me-out** and
then you have the usual hx attributes

for example swap the outerHTML. And there are some thing new, this here,
this means the swap should last one second. And during this one second,
HTMX adds an additional CSS class to this button and we can do some CSS
with this special class and this special class is a given name and HTMX
swapping gets added to this existing CSS class.

This means this selector here matches during the swapping phase and now
we have the possibility to do some CSS transitions during this swap.

So we have opacity zero, **this means that at the end it should be completely
transparent**. And you can do any kind of CSS transitions you want, for example,
ease-out, but this ease-out, it's on CSS transitions, it's not HTMX.

The HTMX part only says, adding this special class during this web phase.
So yeah, and that's all that is needed, so the transition should happen
on the opacity and it should last one second and it should be zero.

Yeah.

And that's so straightforward and so simple so that **you can do really
nice CSS transitions during your web request**, during your hx calls to
the server.

Yeah.

Very straightforward, I hope you liked it.

24:54 What does HTMX mean for me as a Django developer ?
=============================================================

- https://docs.djangoproject.com/en/dev/ref/utils/#module-django.utils.html


.. figure:: images/django.png
   :align: center

So we are here at the Django conference, up to now I only talked about HTMX,
so what does HTMX mean for me as a Django developer ?

I changed the way I develop software with Django and HTMX because now I
**write small methods, returning small fragments**.

This means function-based views make more sense for me, so I switched
from class-based views back again to function-based views.

The next thing which changed in the way I develop is that I used `format_html() <https://docs.djangoproject.com/en/dev/ref/utils/#module-django.utils.html>`_
now again. It's a very cool method of Django to create Safe HTML strings
directly in Python and it's very, it's like :ref:`locality of behavior <lob>`,
so all the things I want to change are in one place, I don't need to switch
between the Python code and my template code.

I have the HTML and the Python in one place, in one file and this is very
natural for me, it feels easy, it feels like very, yeah, fluent software
development.


.. _naming_pattern_htmx:

26:13 Naming pattern
========================


.. figure:: images/my_naming_pattern.png
   :align: center

So I created a small **naming pattern** for me to better understand the code
and I want to show it to you.

So I have three kinds of methods and the first kind of method is function-based
view which returns a whole page.

So this means this is an HTTP response which contains HTML with an HTML tag,
with a head tag, there's a body tag and so on, and to make this more obvious,
this function-based view is such view I call it foo_page, so I use this
suffix here, **page, to make it more visible**.

And it returns, as I said, **a whole page**, and the URL is then called just
foo here.

Okay, that's the initial page.

So, usually it's the first page which gets opened by the user and then later,
from this page, I do this call back to get some updated fragments or things
like we have seen in the infinite scroll example.
This method with return **fragments**,
I call this like this, I have the name and then underscore hx, and this
means for me, okay this function-based view returns **HTML fragment**.
So URL, I call it the same, here you see the hx.

And I think it's perfectly fine to leak this information into the URL
namespace because the average user will see this one here, just the first
page, and only experienced users who open Chrome Dev Tools can see that
these hx, and these get called by HTML, by HTMX.

Okay, these two kids are function-based views and sometimes I need a
**utility method** where I create some HTML and this HTML gets used in
several places, in several function-based views and then I create a
method with a name, **_html** and there I immediately see, ah,
that's a method which returns HTML directly.
So this is **for internal use for me**.

Yes, I've found this naming pattern to make my code more readable, if you
like it, use it, if not, then don't use it.

HTMX is great for
========================================

- https://web.dev/lcp/

.. figure:: images/htmx_is_great_for.png
   :align: center

So let's summarize what we have seen. HTMX is great for, it's great for
**Search Engine Optimization** because you can get super fast, `Largest Contentful Paint (LCP) <https://web.dev/lcp/>`_
not pain, paint, so Largest Contentful Paint.

So the first page gets loaded very fast. If you compare this for example
to loading Gmail, loading Gmail for me means, bzz...

There's a loading indicator and you get a real application with Gmail and
that's not what I want, I don't want a real application, I want a page
which loads fast.

So what you can do with HTMX professional, serious, but maybe a little
bit boring, stateless web applications, like shops, ERP systems, database
frontends and so on.

And I think it's very super-cool to crate an MVP, so you get a prototype
very fast and out and later you can optimize and later you can add some
sugar like client-side validation or something like this.

So what's it not well-suited for ?
--------------------------------------

So what's it not well-suited for ? It's not well-suited to create an
offline first applications and this does not make sense with HTMX because
HTMX needs this server as central part, so at least I have no clue how
to write an HTMX-based application as an offline application.

Okay, since there are great interactive chart libraries out there, and it
makes no sense to try to re-implement this somehow with HTMX, of course
these interactive chart libraries are written in JavaScript and then
used, no need to reinvent the wheel and the same for what you see is what
you get editors, like tiptap.

They are available, they are written in JavaScript, they have great just
use and that's not just because HTMX is great for one thing, it does not
mean it can be used in every case.


It depends
==========

.. figure:: images/it_depends.png
   :align: center


**So it really depends on what you want**. If you want to write a real application
which a user opens once in the morning and he keeps it open the whole day,
then like Gmail, write a fed-JavaScript frontend.

But if you want an application which loads fast, where the first page
gets loaded very quickly, then HTMX makes a lot of sense.


31:26 Rethinking "smart" re-use of API
============================================


.. figure:: images/rethinking.png
   :align: center


What made me think JSON over the wire and fed JavaScript frontends are
great seven years ago ? One thing was this picture here.

Seven years ago I thought, hey, Angular is great because I create a JSON
API once and then I can use this API in two cases, I can use it for a
machine to machine communication and I can use it for HTML frontends.
**But this is really nonsense.** And I will show you why.

32:11 Serve the matching format
===================================


.. figure:: images/serve_the_matching_format.png
   :align: center

   https://youtu.be/z0yPTv15Fjk?t=1931

So let's have a look at this picture, again I have the SQL on the server
side, and I send HTML directly to the user and if I want machine to
machine communication then I need to create a different API,

I can't use the same API here because let's have a look at improve daily
and A/B testing.

For my user interface, I want to improve daily and I want to do A/B testing
and other fancy stuff and now let's have a look at the stable API.

I don't want to improve daily here, I don't want to do A/B testing on
stable APIs.

**It's nonsense that you save some time by creating, by splitting frontend
and backend because later**, I will need machine to machine communication
and if I split my frontend and backend, now then, in 10 years I'll save
me three hours of work.

.. warning:: That's nonsense !

So usually you create a user interface first and then focus on the user interface
-------------------------------------------------------------------------------------

So usually **you create a user interface first and then focus on the user
interface, make it a great HTML user interface** and if you later need
some machine to machine communication and some APIs, yes of course create
them, but **please don't put the one thing upon the other, this at least
for me makes no sense**.


The end of the Odyssey ?
==============================

.. figure:: images/end_of_odyssey.png
   :align: center

   https://youtu.be/z0yPTv15Fjk?t=2023


Now, is this the end of my odyssey ? I don't know.

Maybe it is just the beginning, at least I solved my frontend blues for
now. But the next question is how to create HTML on the server side ?

At least I want to work with Python and Python has several hundred ways
to create HTML on the server side, but somehow I have not found my preferred
way up to now.

I really like the Python f-Strings, but, and they don't escape the data
which I put into the strings, so it's very likely that I could get something
wrong here, so that's up to now I use the form of HTML, but it would be
really cool to have a mixture of formal HTML and f-Strings, maybe like
in JavaScript, there's something called template literals which uses
backticks and this would be really cool if Python would have these backstrings
with the possibility to auto-escape the data, yeah, that would be heaven.

At least, but this would need a change in Python itself and I'm not a
Python hacker, so I have not worked on the core up to now.

Maybe someone of you has an idea how you could get something like this
JavaScript template literals with backticks into Python, this would be
really cool, yeah.


35:20 django-htmx-fun
========================

- https://github.com/guettli/django-htmx-fun
- https://github.com/guettli/django-htmx-fun/commits/main
- :ref:`django_htmx_fun_guttli`

.. figure:: images/django_htmx_fun.png
   :align: center

   https://youtu.be/z0yPTv15Fjk?t=2120

So here's a link to my github project called :ref:`Django HTMX Fun <django_htmx_fun_guttli>`.

Check it out, test it, it's a very simple application.

Using this naming pattern I talked about and some simple Django application
to demonstrate how Django and HTMX could be used together.

But there are several out there, just Google for it, Django HTMX and you
will find several of these applications.

Check it out, have fun.

Last but not least, about my team, I'm from descript, Germany, Dresden
and we are developing web-based software projects with Django since 2008.


.. _ref_descript:

descript team
================

- :ref:`descript_team`

.. figure:: images/descript_team.png
   :align: center

So we know what we are talking about and we are looking for new crew mates.
So if you are a developer and you like Django, yes, if you're looking for
a job, please contact us.

Let's get in touch
===================

.. figure:: images/lets_get_in_touch.png
   :align: center

Okay, that's it, thank you very much for listening and I want your feedback.

Please tell me if there's something which I could improve, if there's
something which was difficult for you, please speak up.

You can contact me and follow me on Twitter (https://x.com/guettli)
or send me an email, yes, I would like to hear from you, see you, bye!
