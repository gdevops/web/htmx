.. index::
   ! hotx (HTML over the X)

.. _hotx:

=================================================================================================================
2021-10-18 How about **HotX** ? **HTML over the X**, where X can be http request, websocket or something else ?
=================================================================================================================

Maybe instead of fighting, and rewriting an article, it's better to come
up with a new acronym than the hideous HATEOAS.

How about HotX ? HTML over the X, where X can be http request, websocket
or something else ?
