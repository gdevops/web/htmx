.. _thisweek_in_htmx_2021_10_17:

===============================================================================================
|thisweek| 2021-10-17 **This week in htmx N°1** by  Deniz Akşimşek |thisweek|
===============================================================================================

- https://thisweek.htmx.org/issue/1/

.. figure:: this_week_in_htmx_2021_10_17.png
   :align: center


Carson gave a talk on htmx at JetBrains JavaScript Day 2021
===============================================================

- :ref:`carson_gross_2021_10_14`
- https://www.youtube.com/watch?v=R2Q0L9PDHtc (4:27:47)

In his talk htmx: Writing JavaScript to Avoid Writing JavaScript, Carson
delivered a rationale and demonstration of htmx, and obligatory discussion
of REST/HATEOAS as usual...

to an audience for whom it was quite out of the ordinary.
Was he provocative enough to get some JavaScripters curious? We'll have to see.

💙 to the conference organizers and attendees!

https://youtu.be/R2Q0L9PDHtc?t=15881


Ben Pate committed to refactoring SSE & WebSocket support into plugins
===========================================================================

- https://discordapp.com/channels/725789699527933952/725789747212976259/897947897377742888
- :ref:`benpate`

::

    1cg pull web sockets and server sent events out into extensions 👍 5👎 1

    benpate Just say "go" and I'll volunteer to do it

    1cg go

    1cg 1.7 we will pull them out

    benpate F. What have I done?? 🇫 1😂 1

`💬 October 13, 2021, 8:44 AM UTC <https://discordapp.com/channels/725789699527933952/725789747212976259/897947897377742888>`_

Carson explains how to make third-party components work with htmx
====================================================================

- https://discordapp.com/channels/725789699527933952/725789747212976259/897967236420096000
- :ref:`back_button`

::

    1cg Ah, OK. So, here's the deal

    1cg and I need to document this, sorry

`💬 October 14, 2021, 10:01 AM UTC <https://discordapp.com/channels/725789699527933952/725789747212976259/897967236420096000>`_

When using a JavaScript library that makes a lot of modifications to
the DOM, the modified DOM being saved into history is usually undesirable
and can lead to bugs. Carson explains how to avoid it.

In summary, initialize your libraries in htmx.onLoad(), and uninitialize
them on the htmx:beforeHistorySave event.

    Fubarrr [...] If you want your back button to behave as you expect
    it to… this pattern of “undoing” all the DOM manipulation your JS
    widgets did initially on page/fragment load, is critical. [...]

`💬 October 14, 2021, 10:44 AM UTC <https://discordapp.com/channels/725789699527933952/725789747212976259/897967236420096000>`_

https://discordapp.com/channels/725789699527933952/725789747212976259/897967497335148605


Thomas Güttler reminded us about the htmx tag on StackOverflow
==================================================================

- https://discordapp.com/channels/725789699527933952/725789747212976259/897569632708345927
- :ref:`guettli`


:ref:`guettli <guettli>` I think up to now I am the only one who tries to support
new htmx users which ask a question on Stackoverflow.
It would be very nice, if some more users could subscribe to the
tag htmx, so that you receive an email if a new htmx question got
asked: Of course I will up-vote your answers 🙂

`💬 October 12, 2021, 7:41 AM UTC <https://discordapp.com/channels/725789699527933952/725789747212976259/897569632708345927>`_

You heard him, folks.

https://stackexchange.com/filters/409974/htmx


**hyperscript** adopted the microbundle build system
========================================================

Don't worry, :ref:`hyperscript <tuto_hyperscript:hyperscript>` will always be available as a single file that
you can load from a CDN. However, we can now make ES module builds
available as well!

https://github.com/developit/microbundle


URL literals for **hyperscript** were considered
==================================================

- https://discordapp.com/channels/725789699527933952/796428329531605032/897491928990515250

::

    alleho how can you fetch a url that has been built and placed inside
    a variable?

`💬 October 12, 2021, 2:32 PM UTC <https://discordapp.com/channels/725789699527933952/796428329531605032/897491928990515250>`_

This question started a whole discussion that ended with URL literals
as a potential new feature. Here's a peek at how that might look::

    set url to https://cors.example.com/api
    append /user?id=${the userId} to url.pathname
    fetch url

htmx was brought up in a Hacker News thread about Laravel Livewire
====================================================================

- https://discordapp.com/channels/725789699527933952/725789747212976259/897949295674818560
- https://news.ycombinator.com/item?id=28850757

Let me check again... Nope, that's not LimeWire.

::

    1cg htmx swoopin' on livewire on orange site:

    1cg [Screenshot: "How Laravel Livewire works", top comment is about htmx]

`💬 October 13, 2021, 8:49 AM UTC <https://discordapp.com/channels/725789699527933952/725789747212976259/897949295674818560>`_

Hacker News is far from a good barometer for the general community
of developers (someone tell Paul Graham!) but the attention feels too
good to ignore it.

https://news.ycombinator.com/item?id=28850757

💙 to Livewire and the Laravel community!

© 2021 This Week on HTMX Contributors
