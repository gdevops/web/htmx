.. _thisweek_2021_10_15:

=================================================================================
2021-10-15 **@DenizAksimsek has generously offered to create weekly summaries**
=================================================================================


@DenizAksimsek has generously offered to create weekly summaries of the
goings on on the htmx discord and so forth: https://thisweek.htmx.org

thank you deniz!


.. figure:: weekly_htmx.png
   :align: center
