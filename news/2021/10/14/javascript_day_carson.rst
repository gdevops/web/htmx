.. _carson_gross_2021_10_14:

=============================================================
**2021-10-14 Carson Gross at JavaScript Day**
=============================================================

- https://www.youtube.com/watch?v=R2Q0L9PDHtc (4:27:47)


.. figure:: carson_gross.png
   :align: center



johnpitchko 15/10/2021
==========================

- https://discord.com/channels/725789699527933952/725789747212976259/898571645114019890

@1cg had some shower thoughts today after watching your seminar yesterday.

had some shower thoughts today after watching your seminar yesterday.

1) I'm curious in your tale of divergent web technologies, is there a
   place to talk about the brief time that XML and SOAP was the rage ?
   In some ways, I see HATEOAS and hypermedia having links to XML.

2) I feel that I don't truly buy any argument that JSON is better because
   it is smaller/faster. In order to render a page based on JSON data
   received from a web server means that the client has had to have
   first downloaded all the UI and template elements.

   This makes me think of the legacy thinkclient deployments of previous years.
   A client needs to download a bunch of styling and functional code
   (including some he or she may not even use)  to make use of the JSON.

Caveat: I am not a smrt man, and I may be misinformed/out right wrong
about many things, so welcome to be guided towards 'the truth' wherever it may be


1cg — 15/10/2021
====================

- https://discord.com/channels/725789699527933952/725789747212976259/898594180631236618

heya john, it would be interesting to go back and look at the conversations
that were going on when XML/SOAP was big.
My take is that the reason REST was inappropriately imported into the JSON
conversation was because REST/HATEOAS was still plausible with XML (although
still incorrect, since REST/HATEOAS requires a consumer with agency: https://intercoolerjs.org/2016/05/08/hatoeas-is-for-humans.html),
since it kinda looked like HTML, and then it just stuck when people flipped
to JSON, where it was obviously inappropriate, but the thought leaders
couldn't let it go.

for 2, my take is yes, the HTML payload might be a bit bigger, but latency
is a bigger issue than payload size and also the browser is lightening
fast at parsing HTML.
Compare that with javascript executing to produce either HTML to be parsed,
or just DOM objects.
**The browser/HTML parsing is gonna be a ton faster.**

