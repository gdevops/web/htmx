.. index::
   pair: Lisp ; Richard Gabriel
   pair: maintenance; locality
   pair: Audio; Conference
   ! Worse is better

.. _htmx_carson_2021_07_09:

===================================================================
Audio conference with Carson Gross, July 7th, 2021 | 01:10:24
===================================================================

- https://djangochat.com/episodes/htmx-carson-gross
- https://djangochat.com/episodes/htmx-carson-gross/transcript
- https://x.com/carltongibson

.. figure:: ../../../../images/carson.png
   :align: center

   https://x.com/htmx_org

Carson is the creator of htmx (formerly intercooler.js), which allows
for AJAX, CSS Transitions, WebSockets, and Server Sent Events directly
in HTML: no JavaScript required.


Carlton Gibson 0:05 welcome to another episode of Django chat
===================================================================


Carlton Gibson 0:05
Hi, welcome to another episode of Django chat
fortnightly podcast on the Django web framework. I'm Carlton Gibson joined
by Will Vincent Hello Will.

Will Vincent 0:12 Hi, Carlton.

Carlton Gibson 0:13 **Carson gross,the author of the very, very, very super exciting HTMX library**
==========================================================================================================

- :ref:`htmx_tuto`

Hello, Will. And this week we're very pleased to have
on as Carson Gross, who is author of the very, very, very super exciting
HTMX library.  Hello, Carsten, thanks for coming on the show.

Carson Gross 0:23
Yeah, thank you very excited to be here. **Very excited
to see the Django community taking a take a look at HTMX.
Yeah, no, I mean, it's, it's been a real revelation**.

So for the audience. Let's, let's go back a bit. And can you give us a little about
your backstory, and then we'll get into the backstory of HTMX and what
it is, and what you know why we're also excited.

Sure. So I actually, I have to admit, I've never done any serious
Python development, at least not anger. But I've been programming for
a long time, I programmed in Java just forever. And I started doing web
application development back in the late 90s. And just right when things
were starting, certainly get going. And I always kind of was in the Java
world, until probably around 2005. And then I started picking up rails
and started doing a lot of rails development. And I still work on rails,
and a, there's a startup that I work for from outside startup anymore,
but a software company that I found it with a couple of other people. It's
based on rails, and so the original library that turned into a sinise was
called intercooler gas, and I built it sort of in a Rails with a with a
Rails back end against a Rails back end.

Will Vincent 1:48
I've heard you say that you built intercooler because
you hate JavaScript. But now you're an expert in JavaScript, because
you're bearing the burden for the rest of us. Is that yes.

Carson Gross 1:58
Yeah, there is. The irony of me not wanting to write
any JavaScript is that I've had to write an awful lot of JavaScript to
DC. And I still I write in a very idiosyncratic way I tend to write in
the older, yes, six, I think it's called form. And I just, I don't use
the newer stuff. HTMX is iE 11. compatible. My understanding is that I
11 has been formally destroyed here and a few months, but I've always,
I've always coded, I found a way that I like to where I can can write
JavaScript without creating too many bugs. It's a very functional way,
but it's very Socratic. So when normal, you know, quote, unquote, normal
JavaScript developers come and look at my code base, they are often
very confused. I have found that other people coming from outside the
JavaScript community find it fairly approachable, but it's definitely
not the standard way to write JavaScript.

Carlton Gibson 3:02
Can I ask you about integral to them? Because if
you're using rails like, Okay, so let's we need to fill in some context
for people who don't know about HTMX. Let's talk about briefly what HTMX
is. And then we'll come back to the history.

Carson Gross 3:14
Yeah. So HTMX is a good way to think of it as a way
to start in on it is, you know, when you've got a normal web app with
anchors and forms, right, we're just writing a normal vanilla web app.
The problem that you run into from an interactive standpoint is that every
time you click on a link or submit a form, you're going to do a full page
reflection. So you get that clunky, you know, full screen render.
**And so one step into the mindset of HTMX and intercooler was well, why don't we make
it so that those can replace a part of the screen, rather than having to
replace the entire screen ?** So that's a step towards that and thinking about,
okay, let's take, take the hypertext that we've got HTMX, and let's extend
it so that it can do to do more stuff as a hypertext. And so replacing
parts of the screen is one, maybe responding to other events, not just
clicking submit, is another way to improve the way HTML works. Maybe you
don't want only anchor tags and forms to submit to make server requests,
right, very often you have other like devs, or whatever, that are making
requests these days. And then, you know, I also the last kind of
component in there is making all of the HTTP actions available. So it's
not just getting posts. Most web apps, unfortunately are hamstrung by
the defaults in HTML, where it's really it's only easy to issue, get
some posts, but there's other stuff is deletes. And so you know, there's
all the restful types of actions that you want to use, and so taking all
those and Natan surfacing them in HTML is sort of what? What HTML does,

Carlton Gibson 5:05 **it was a couple of data attributes in a Django view**
=============================================================================

yeah. Okay. And and just as an example, the form I
was working on last week, he just took on the, it was just a select box
that took on the change event and when it when you triggered the change,
it would go and fetch the some related data and put that straight into
the page. You know, **it was a couple of data attributes in a Django view
on the back end, just waiting there. And that was it. And it was as nice
as as, as anything. It was**.

Carson Gross 5:28 Yeah, it's just one example. Yeah, exactly. So you
can have an app that has the nice features and come away from the more
extensive front end frameworks like react. But hopefully, without all that
heavy weight without only day coding, every circuit is necessary for that.

Carlton Gibson 5:45 Yeah. But I mean, literally, I didn't write any
JavaScript, it was just a few HTML attributes. So that was cool. Let's swing
back to a server with HTML template. So it's this this cool library, and
you talked about building it, working with rails, and but rails has, for my
history, it was p, Jax, and then turbolinks, and then the hotwired stuff.
And so I found HTMX in a few weeks after the hotwire announcement, where
they had a nice website, and there was a lot of hype in it.  And someone
mentioned, Adam Johnson mentioned HTMX. So what's this?  Check that out
as well? It's in the same space. So how did you what, how come you came
to write into cooler rather than going along with the

Carson Gross 6:28  a **philosophical difference**
======================================================

Yeah, you just using turbo x. Little bit easier for
sure. P jacks, I mean, I used to be Jack's back when that was the big
option. And terminal lengths, obviously in Rails community is very big,
and people use it outside of rails as well. And it's a good piece of
technology.  But I when I was looking at it, I felt like it just needed
more. It needed more of a formalization. The turtle legs and hotwire
both can have a magic they reflect I think both libraries reflect the
personalities of the people are developing.  So DHH really likes magic,
but he wants things to just work.

And to me when I when I especially as I got into it and started to figure
out what I wanted intercooler it to be, I felt more like I was coming
at this from the direction of I want to improve HTML is hypertext and
not provide some total solution that is magic. And everything works,
you know, but it's hard to understand what's going on.

So it shouldn't I hope is fairly incremental. And you can you build up,
you know, a few attributes, and you understand what's going on.

**It's not just automatic. And so it gives you more control**.

And again, I think it's that it's just a **philosophical difference**.

I was trying to think more, let's go with the grain of HTML and take it
further rather than let's just make it all work magically.

Carlton Gibson 8:01 That's kind of a good answers for a Django podcast,
because quite often people say, well, what's the difference in Django
and rails?  And one answer to that question is that Django, you kind
of do it, it's not quite automatic, you know, rails gives you the full
controllers, and the AJAX forms and all the things just out of the box, and
that's lovely.  Whereas Django, you have to wire that stuff up yourself.
But the, you know, there's a bit more flexibility perhaps, or, you know,
who knows, you know, you can,

Carson Gross 8:26 I think also you, you understand the tool a little bit
deeper, you have to be careful with too much magic, because you can, you
can have sort of a helpless feeling when it doesn't do what you want it
to do. Right? It's,

Carlton Gibson 8:40
it's great until it doesn't work.

Carson Gross 8:42 **HTMX is purely extending HTML to hypertext**
=================================================================

Right. And so, you know, there's, that first week
can feel real, it can feel wonderful. But then later on, when you need
a little bit more flexibility, where you're trying to understand why
something isn't working exactly the way that you want. Being a little
bit more explicit, is often is pretty useful, especially with editors
being as good as they are these days. It's I've kind of moved away from,
you know, trying to minimize everything. And it's okay to be explicit at
times. And so HTMX is back now, it's much more explicit than some of the
other options that are out there for the style of development. I should
mention like livewire as well. That's another example. And in both of
those cases, and how I was the same way in that it requires WebSockets,
there's a server side component to using that isn't necessarily an HTMX
does not have that explicitly. I've been trying to focus just **it's purely
extending HTML to hypertext** and therefore, it's not it doesn't make any
specific requirements on your back end. **So it can be used with anything,
and Django is a great example of something that can be used with**.

Carlton Gibson 9:51 Okay, so then I have one more sort of question from
the history and the backstory side of it is why when there was intercooler
is there now HTML, what's the story there between the change in the library?

Carson Gross 10:04 So, last summer, I had intercooler, excuse me, I think
it created back in 2013. And so it's been around for quite quite some
time. And I came out of the jQuery world, though I was I did a lot of
jQuery development. And so intercoolers Yes, was based on jQuery is all
the jQuery infrastructure. And last summer, you may recall that the world
kind of shut down. And so we, I was sitting around and thinking, all
right, what do I want to do with myself to keep from going insane? And
I, it always bothered me, but HTML ating increasing? I didn't at the
start, but it increasingly bothered me that intercooler was dependent on
jQuery. And JavaScript did come a long way in the last, you know, seven or
eight years. And so I said to myself, all right, well, let's try and see
how hard it would be to break that dependency on jQuery and just write in
pure, pure JavaScript. And along those same lines, one of the things that
always bothered me when I tried to tell people about Unicode Yes. And I
have to say, I've gotten quite quite a lot of grief on the internet when
I tried to talk to people about the style development, particularly in
the JavaScript communities, which is it's understandable. But they were
they the library was being compared with react, digest, or with whatever,
whatever the big JavaScript library of the time was. Initially, it was
what was Google's Angular, angular? Yes. So it was compared with Angular
was, or whatever. And, and I saw, I didn't like that dot j s aspect of
the name. Because I felt like what I was really trying to do was push
HTML as hyper tags and see how much expressiveness we could get in HTML
itself as a hypertext. And so I started looking around for better name
and HTMX.org was available. And I was like, Well, I mean, do I got to
take that? 2020. And so that, and you know, that obviously, I did a lot
of documentation, rewrites and so forth, and really tried to clean the
library up as well. I'm much happier with he I think intercooler is a
great piece of technology or not, if you're using it don't feel like you
have to move HTML anytime soon. But I think it's July too much, Claire
is a much cleaner implementation of the concept.

Will Vincent 12:36 And the history, I'm curious, so 20 2013, when you
first started on intercooler, I remember, I think it was around Was it
2012 when react, so the idea of the virtual Dom came out, because that
was that took a while for most people to wrap their heads around. But
then it was pretty amazing. But then you looked at that and said, well,
let's go the complete opposite way to achieve something similar. I think
that's amazing. Like, cuz,

Carson Gross 13:02 yeah, my, my company, the company that I run is called
Big Sky software. And our tagline is that we find hot new industry trends,
and then do the opposite of that. So I've always been a little bit of a
contrarian by nature. And that's good in some ways and bad knowledge. But,
you know, I just, it really came out of the original concept came out of
a situation where I was writing some pretty hairy jQuery code, update a
table dynamically. And the performance was just atrocious. And so I just
now almost out of desperation, tried rendering it on the client side, and
then slamming it into the DOM. And it was instant. And so I said to myself,
well, self, let's not do a bunch of front end stuff, I was just trying to
formalize this notion of, you know, Ajax grabbing some HTML and slamming it
into the DOM. And it's obviously become much more sophisticated than that
at this point. But it came out of that, that world, I just, I don't know,
I sometimes I feel like I'm not as smart as some of these other people. And
just like, I can't, I can't figure that out. I'm just gonna do it this way.

Will Vincent 14:15 This, you know, all great truths start off as
blasphemy. So you know, who's to say?

Carlton Gibson 14:21 I mean, is it past me, I have to whisper like, because
there was this thing that I was using a long time ago, jQuery dot load,
right, which would get your HTML and it will replace the content of the
Dave and I was like, well, this is quite good. And then I wasn't allowed
to use that for a long time.

Carson Gross 14:38 Why now? Why not? Bring it back? Maybe we'll talk
about I prescriptive. You wanna? You want to get really, really obscure?

Carlton Gibson 14:46 Well, let's save that for a minute because I wanted
to ask just a more sort of, kind of abstract question because everyone's
been telling me that the modern web has to be done with you know,
isomorphic Java. JavaScript on, you know, server sends out the thing,
it gets rehydrated back on the client and all of these things. But the
performance of that and the the bandwidth required for that seems to
be quite high. And then all that, for a number of years, I felt as if
I'm swimming against the current of this kind of you just must do it
this way. Otherwise, you can't possibly compete. And yet here we are, we
he makes comes along and you know, the related libraries that are in the
similar kind of domain. And it's like, no, but we can just render the, the
HTML on the server and injected in the DOM and that work. I mean, just that
that kind of theoretical level, what do you think's going on? And you think,

Carson Gross 15:44   **I think there's a sociological aspect**
=================================================================

Well, I think I really, you know, I think there's a sociological aspect
to it to the age you just have to be cognizant of
which is the book. First of all, the industry is prone to trends. There's
no doubt about that. And so, you know, we've been down some bad guys,
Google, you know, everyone's dressed in Google, everyone trusts Facebook,
like with Angular, I think Angular 1.0, anyways, is widely read, it's been
a pretty terrible idea. So there's a lot of bad ideas that come along,
we just have authority based on where they come out of. We also were
forward looking industry, we don't do a great job of taking what works
and building on it incrementally. And that's, you know, I think there's
some personality traits to like good developers that make that difficult,
you want to be innovative. And there's the whole job situation, right,
like being left behind technologically is a fatal can be a fatal situation
in the tech industry. And so that puts a lot of pressure on people look
for the new stuff. So you know, but when you look at if you if you're
able to take a step back and look at things from from a little longer
perspective, and I don't blame people who can't, you know, someone who's
only experienced with web development is react, and are like that entire
infrastructure and can't really be just they don't they don't have that
that longer term perspective. On the way things are. And the the, you
know, the original web architecture, I don't feel like it was communicated
well, rest and HATEOAS kind of got tied up in the API community. Yes. And
the web development community kind of lost touch with what what the web
architecture was, the web was just kind of this VM slash janky. Ui markup,
UI, markup language, that lived almost by accident on everyone's computer,
rather than an innovative new network architecture, which is what you
know, Originally, it was intended to be so. So you know, but again, I
don't blame people. For I do blame a little bit, the Facebook and Google
people, I'm sorry to say, just because I feel like they, they're the if
anyone had the ability to communicate the original web architecture, well,
to the broader development community, it was those companies and, and I
don't see a lot of that. But, you know, feelings, feelings. Better. I
heard somewhere his uh, his thesis in his PhD thesis that outline the
web architecture is pretty impenetrable. It's not easy to

Carlton Gibson 18:31 get even the O'Reilly books are meant to be a gloss
on that they're quite impenetrable.

Carson Gross 18:38 I feel I often we early on I would do, I'd go into
rest in Haiti, awesome. Nice talks very early. And I could just see the
eyes glazing escaped. Alright, we'll focus on the practical stuff first,
and then maybe we'll talk a little bit more theoretically.

Will Vincent 18:55 So um, that I wanted to ask about so when I was learning
the web, I remember being like, why do I need all this JavaScript? And
it sounds like I've, again, I've heard you say, you know, the W three
c kind of basically stopped with HTML, which is kind of true. I mean,
it was created and then they stopped it. posts with forums and, and yet
there was all this other information there. So I guess my question is,
had anyone else tried to tap into these things? Because it seems like
in hindsight, it was just sitting there. And everyone just popped in the
JavaScript train and didn't take a solid look at it.

Carson Gross 19:28 Yeah, I agree. I it makes me wonder at times. In
Zoo lander, there's that that line where he says, am I the only one who
notices this?

Will Vincent 19:39 Am I taking crazy pills?

Carson Gross 19:42 And, and when I the more JavaScript I saw the more I
was like, Man, what about hypertext? So you know, I don't know what to
say other than I'm glad that I was able to work on this and it seems like
it's working out pretty well. I think a lot of people when they, when they
see it, they're like, Oh, yeah, that that seemed the same. And so there
is something there, I tried to open an issue on the discussion forums for
HTML for like the web. I don't know what it was someone sent me a link. So
these people do browser stuff. And so I tried to open up a discussion there
about extending HLS hypertext, but it didn't go particularly well. And I
understand that, you know, this random guys saying, Hey, we should think
about HTML as extending it as a hypertext. And they're like, cool. Who
are you?

Carlton Gibson 20:38 The way you get in is like, with the image tag, the
image tag, right? They just implemented it, everyone went with it. And
then all of a sudden, they said, Well, we better standardize it. So if
you can just keep pushing.

Carson Gross 20:48 Yeah, I think I'm not gonna I doubt I'm gonna work on
Chrome anytime. I'm more than okay with that. But yeah, do you know there
is kind of a weird, like, wow, why didn't we just do this? Yeah, I mean,
integral is all and it was 2013. So it's been around a while, but it's,
you know, it can be hard to sell. I'm just one guy, we got a small, but you
know, good little community around HTML, it's an inner core. But you know,
there's just, there's a lot of momentum. And JavaScript say, well, well
about it, you can do everything you need to with it. And so, you know,
if you're not, if you're not convinced, if the hypertext architecture
doesn't speak to you, then, you know, I can see why you might be more
inclined to look at a JavaScript solution for things.

Carlton Gibson 21:46 Okay, well, just from a Django point of view, like feed
for, you know, decade or more, we've been building API's with Django rest
framework, and, you know, the clients have been coming and hitting that,
and the poor Django templating language has been sat there all the time,
but we use it for a bit, but our you know, I've got to do it in front. But
actually, you know, having picked up HTML and started to implement, it's
like, now I can just create my, my HTML template, I can send the server,
I don't even actually need a rest DRF view or rest framework view, I can
just do it. And then all of a sudden, I think, right, I need a, I don't
know, I need a crud for a related objects that is triggered by this drop
down, I just create the Create, view, the update view to retrieve you for
that. And it's all really vanilla Django, like the most basic Django in the
world, but it's coming to the browser, really smooth and page quick. And,
you know, I go and do the lighthouse, the what is it the audit thing
that you've got in Chrome for the speeds? And it's lightning? You know,
it's like, yeah, green,

Will Vincent 22:48 you can cache it pretty easily, too. Yeah.

Carson Gross 22:51 Yeah. Yeah, that's one really great thing about
Ajax. That's exactly right. So you can, you can rely on old and very proven
server side technologies. And in fact, as you're probably noticing the
encoding difference between HTML and JSON is almost nil, by just the size
hazard in size, a barely notices. And it can often feel faster, because
you just you don't have a lot of JavaScript live in between the HTML
and the actual Dom itself. So you know, it's, I think one of the great
things about HTMX is that brings these these tools that are very
mature. And we have a lot of experience, whether we some of the older web
developers have a lot of experience with caching, you know, you brought
up caching, and that's a good example, like a well cached web app to be
very, very fast. **And that's, that can be hard to do**.

If you've got if you're fronting an arbitrary query API in JSON, right? Like caching that
can be really difficult. And but if it's, if it's just an endpoint with
a gap, you know, you can catch that. And you can use whatever, you know,
mature caching technology, right to to make that much, much faster. And
it does bring up something that I think is worth mentioning about when
you mentioned building JSON API's and Django the, your your web apps tend
to be kind of neatly, like they tend to have very specific needs, like
you have to tune a query to make a particular UI work quickly. Right,
you got to do joins a group bys, or whatever it is, on your back end,
there's something very specific for a particular UI to make it work
correctly. And that's intention with a more general JSON API, when you
give the we give out a JSON API to the wider world. If you're going that
direction. Those tend to need to be very general. And so you need to
have you need to have a more generalized API. And the reason why we have
technologies what's the I always want to say jQuery What's the Facebook
qL? standard? graph QL. Thank you, I want to say jQuery, but we can call

Carlton Gibson 25:07 it, we can call it that.

Carson Gross 25:10 Reason why GraphQL is becoming so popular is because if
you do try to build an API, without a powerful, expressive query language
like that, in JSON, your front end, developers are going to ask for like
1000 different endpoints for all their real needs in the app. And so one
of the really nice things about using something like this gem x is that
those needs get moved back to the back end, where they have SQL, and they
can do whatever damn cool thing they want to do against the database,
right? And then, you know, maybe you need to tune the database with,
with indexes and all that sort of stuff. But that's, that's an achievable
engineering goal, versus this more generalized JSON find. And so I think
those are two separate use cases. There are two different technical
requirements. And you'd see this about, you know, before, before jQuery
slash, graph QL. You would have API developers that were they were just
there in despair, because they were constantly unit, you'd be on version
35 of your API before you even shipped one point over your web app. Because
there's so much churn and just small little changes to make your UI work.

Carlton Gibson 26:27 This is awesome, actually, because we've, you know,
worked on teams, where I've been on the back end of that conversation,
and it's like, you know, twice weekly meetings on, you know, we need this
endpoint, it's got to have these fields, and it just the, the, the feedback
cycle, there is just so painful and slow and difficult that you just, you
come in here with us and you write the endpoint, and you know, then there
won't be the problem. Right? And, yeah, it's like we've talked about from
Episode One of the podcast we've talked about is Django have a back end or
front end. thing, and actually, it's a front end thing. And it certainly
is, if you're, you know, serving HTML, and then using something like HTML
to get your interaction on, on the browser. Is that yes, you can write
the view serve the templates, and you're the front end, you're creating it.

Carson Gross 27:17 And that's the that's, that's the architecture of
the web to which Oh, that. Yeah, that's actually how it's supposed to
work. Believe it or not. I know. I know. Everyone uses JSON now, but it's
not actually JSON isn't hypertext guys.

Carlton Gibson 27:35 Then it's easy enough to add, oh, we actually we do
want a JSON representation of this data for some other use case. Well,
fine, you can add that. But it doesn't have to be your sort of core
starting point. It just smooths the development process so much.

Carson Gross 27:49 Yeah, exactly. And so I 100% agree that you should
have a JSON API probably. And it should be public. And it should be
rate limited. And it should have all these sort of you know that that's,
you know, for automation purposes, that's great. But that's not the web,
that's probably not for most applications anyways, not what your web app
should be talking to. Okay.

Will Vincent 28:12 So, can I ask about some of the limitations because
I mean, we're both fanboys. Now, Adam Johnson, a whole bunch of people I
really respect are huge fanboys. But there are some limitations. I mean,
so maybe we could just talk about this. So one is, so an easy one is just
maturity of the ecosystem, right? Like in terms of, so maybe we could
start with that, like, how, where is that at? And do you see a path for
people to add things on? I mean, because you have fantastic documentation
on your on your site, by the way, I love that examples page where you
just go, boom, here it is, like, oh, how can I do it? It's just like,
right there. But you can't do everything.

Carson Gross 28:50 Right? Yeah, that's, I mean, it's a problem. I'm just,
I've got there are some people who contribute, but it is really it's a one
man show to a large extent. And I'm just a dude who lives in Montana. Now
Google without Facebook, I don't have the the resources that they have. And
the I just, you know, with intercooler, I don't want to say I gave up. But
if we both of these libraries, they're kind of they're finished. I think
I've got the API reasonably correct. And the implementations probably
good enough. So I'm not planning on chaining them a whole bunch. So Well,
yeah, you know, nobody ever got fired for using react. And I you know, I
understand that dynamic. There's, there's only so much that I can do as a
small software shop. And I think intercooler to an extent, since it's been
around for so long. It's got 4000 some odd stars on on GitHub. So it's a
you know, it's a medium sized JavaScript library, relatively mature.
HTMX is based on it. They seem like stuff He's maturing, it's not as mature
and it's still maturing a little bit. But, you know, I would, I would say,
if, if you're, if you're maybe a more senior developer, and you had some
pull in your organization, and you really liked this style of development,
the discord is active. Your you know, as long as you're not trying to get
anything too crazy, you're getting probably get the help that you need,
but I don't want to present it as if there's no risk. It's not you're
not gonna be able to hire an hcms consulting firm to come in, and and
help you but you know, I'll help you to jump on HTMX.

Will Vincent 30:37 Yeah. Well, not yet. Well, there is.

Carson Gross 30:40 Yeah,

Will Vincent 30:41 I'm sorry to interrupt. I was just gonna say, um, so
Adam Johnson, again, has this Django HTML package, which bundles up a lot
of things. I was just sort of wondering if, if you could clone yourself a
bunch of times, what would you work on to expand the ecosystem as opposed
to working on the backbone?

Carson Gross 30:58 You know, that's a good question. I would need to think
more about that. I think just just just advocate just advocating maybe
trying to, I just had so little success trying to explain the original,
the like, the web model, to people who just putting some time and trying
to think about what the best way to reach people is, and that honestly,
you know, I'm allergic to social media, but I'm on Twitter, because
you got to, I don't know what else to do. I tried Reddit for a wise can
take the trolls. It was too, it was too emotionally difficult for me to
do. So I think you did a lot of stuff that I sort of outside development,
which I'm just not great at, unfortunately. So for me, I'll just probably
cry and along and this and coming on podcasts. And, you know, we'll see
what happens. It's, but I've been really happy with the as much uptake as
it's gotten, especially with the Django community finding because Django
has just been so underserved, traditionally, by the front end libraries
that are out there, and this dovetails really well with it. So you know,
that's my hope. But as far as technically, like, if I close up and have
more engineers, I probably just write more tests.

Carlton Gibson 32:16 Can I ask what you use for testing? I haven't dug in
and seen but I'm always trying to choose a JavaScript test framework. And
I never know which one to go for is it just

Carson Gross 32:24 chose? Yes. Trying. Okay. But I just picked whatever
the partner I mean, this is why people don't use HTML, right? I just
picked most popular ones out there. At the time I started.

Will Vincent 32:36 Yeah, cuz it's was the Cypress. Now I think is the I
think is the hotness

Carson Gross 32:41 is that the new cool one. So the button I used to be
is a different one that was more browser friendly. There every one of the
problems that happen in writing a lot of JavaScript is that I try and make
everything very browser centric. And the JavaScript world has moved away
from that more towards like this mixed mode or mode oriented world. And so
it's, it's actually, it's gotten harder and harder to do browser testing,
which is for all my stuff. I just want to do browser testing. And so it
wasn't a great experience compared to what I was using previously. But
that wanted about died. Okay.

Will Vincent 33:17 Can I asked? So in terms of limit, though? I mean,
if you were going to build Gmail, or something super dynamic, do you feel
like there's a possibility the ht MX could eventually do that, or like,
Where is that cut off? Because there is still, you know, oh, yeah, a cut
off somewhere?

Carson Gross 33:35 Absolutely. So I think Gmail should be built and
something like for it to be right. Like, that's

Will Vincent 33:42 Google Maps, I don't know what a good example is,
or something

Carson Gross 33:44 Google Maps for today. So there you go. Google Maps
is definitely over the line. So if the more the more client side state
is requires for an app, you know, just so like a 3d game, or even a to
like a 2d game, this is just not something that's gonna be done well, and

Will Vincent 34:04 he's a flight simulator with them. They got the whole
world digitized. Yeah.

Carson Gross 34:09 But Google Maps is a good example of something that
wouldn't be done. On the other hand, the settings page in Google Maps
would be probably something that can be done very well.

Will Vincent 34:20 Isn't that crazy? You always have to, like, click the
Save button at the bottom. And I forget that every single time I flipped
the switch and for keyboard shortcuts, and then it's like I didn't,
it's so ridiculous.

Carson Gross 34:31 Yeah. So that's the sort of stuff where you know,
HTML is lightweight enough that you can use it for parts of your app. And
so there, there are parts where it probably makes a lot of sense. And
then there are parts where it doesn't make a lot of sense. And not just
in the more complex world, right. There may be parts of your app could
just be old, very old, simple. click on links and submit forms works
fine for that part. And you don't need to add HTML. You don't get much
by adding this text to it. So Just leave it that way, that simplicity
is its own reward. But maybe in some parts of your app, you want to do
something like active search, right, where as you type the results filter
down based on some server side filter, and so then just add a few extra
nice attributes just there and implement it just for that spot. And so,
so I think it goes both ways. html can be too complex for some things,
you have proof of concept, before you're really into something. And then
it can be too simple for others, like you're not going to obviously,
in the main, right of a video chat system,

Will Vincent 35:38 well, you can do WebSocket stuff, right? Like that's
sorry, if I interrupted you curl, then, like WebSockets is something
that's there, but not being really used in the same way HTTP two, it's
sort of like sitting there. But

Carson Gross 35:51 yeah, there's WebSockets, and also server side events,
support and HTML, I do have to admit, those are both much more raw than the
AJAX support. And so, you know, those are, I would say, more speculative,
but they're there. And, you know, it's a hopefully a reasonable approach
from as far as a declarative HTML centric way of using those technologies
goes. But it's definitely a newer, it's a newer area of webinar, and it's
harder to be sure that we got it right.

Carlton Gibson 36:23 I think it's interesting with both I mean, service and
events is when you just need to receive a stream of, you know, asynchronous
events from the server, you don't need to send any data back. So they're
perfect. You can open a long, a long request, you just don't close it,
and you just send a new event when one's ready. But quite often, you
don't need that, like you don't you don't even need that to leave that
connection open and poll, whereas you can just make an AJAX request, you
get it back. And then WebSockets are what you're sending to directional
traffic won't quite often you don't need that, it turns out.

Carson Gross 36:53 So yeah, yeah. Yeah, I know, I feel like you can do
quite I've done quite a bit with just good old Ajax and maybe some point,
there's a way to pull in HTML, that makes it pretty easy. And you can
accomplish quite a bit. You'd be surprised can see.

Will Vincent 37:10 I can ask about hyperscript_ carthon? Because I have

Carlton Gibson 37:14 a question because I'm, well, because this leads
into hyperscript_, it's it's a

Will Vincent 37:17 more elegant way to do it. Okay.

Carlton Gibson 37:20 So I'm guessing I've been using it. So you talked
about state client side state and maintaining state and like, for rich
things, I've been using Elm for a while, but much closer to the to the
HTML stuff. I've been using Alpine Alpine j. s, because I saw it on there,
that I think it was the tail when people they were like, if you would be
using jQuery, you should be using our PI. And that was kind of how they
started. I'll give Alpine ago. And it's the same sort of thing, you just
define a few attributes. And, you know, it creates a component and you
can you can save a, like a, an object there. And that object can have
methods, and it's just nice, really quite small. And they say it's like
view but I wouldn't know having not really looked at. And that's great
for you know, when I want to maintain that better state, but you've got
this other thing, which I'm, I'm always in fear of trying. It's called
hyperscript_, right? And it's so fresh and new, and it looks amazing. I'm
just terrified. What is it? What's going on there? And


.. _programming_lang_person:

Carson Gross 38:15 **I'm a programming languages person**
===========================================================

why is it how you right ? That'd be appropriate. And that
is the appropriate response. So what is `hypertalk <https://en.wikipedia.org/wiki/HyperTalk>`_ ?
Well, so we talked about how HTMX came out of intercooler. And it was sort of a clean out,
debater cooler, a lot of stuff that I learned, and we're working on
intercooler and things that I wanted to improve about it.

And one of the things that was in intercooler that is not an HTMX was
something called the attribute was icy action. So I see dash action.
And that was a little way to do some client side scripting, mainly in in with jQuery stuff. But it
had this kind of funny syntax and a way to do to insert weights, like you
could put a weight three, three, or 300 milliseconds or whatever in the
middle of it. And it was a really simple programming language. I mean,
I didn't admit it at the time, but it was a really simple programming
language. And so when I created HTMX, I looked at that attribute and was
like, you know, this is this needs a better treatment. This needs a real
programming language person, I worked on programming language called Gansu
for a long time, which is a JVM obscure JVM programming language. And I
just looked at that and said, you know, what, if I'm going to do that,
I need to do it, right. This is too janky because, you know, Alpine JS
exists. And so if you just want to put a little bit of JavaScript in the
DOM, which will rely on a there's a concept I want to talk about, around
that. You can use something like that. Time.

And so, you know, like I said, **I'm a programming languages person**.
And it's always dangerous when a
programming languages person sees a problem like this. Yeah, exactly. And
so I decided, Okay, well, what this really is, is this is a programming
language that is responding to events. And so I reached back into my
memory deep into my memory banks, and I remembered a scripting language
called hypertalk. And hypertalk was a scripting language that was part
of the hypercard development environment on the Mac. A long, long this is

Will Vincent 40:37 missed, right? This is what miss used?

Carson Gross 40:40 Yeah, Miss was written in hypertonic. And it's one of
the really unique things about hyper dock is that it had event handlers
baked into the language, you didn't call, you know, something dot add event
handler, he just said on click, do this. And I thought to myself, Well, that
sounds like exactly what I needed. And hyper dog, some people, most people
have heard of it, but some people have experienced with appletalk. And
that experience is not good in general. And I appreciate that. But both
both appletalk and hyperscript_ are based on hypertalk. And so basically,
what I'm doing with this programming language is trying to create a purely
front end oriented scripting language and a true scripting language that
allows you to do things that you would do normally, for me anyways, in
jQuery, you might use vanilla j. s, but it's still pretty ugly compared
to a lot of the jQuery stuff. And so it's that's what it boils down to,
there's some interesting characteristics about the language I'm sure we'll
talk about. But at a high level, it's a front end scripting language,
it's designed to make it easy to handle events, and then do simple things
like adding classes to other elements.

Carlton Gibson 42:03 My sort of gloss on jQuery was always select a thing
do with changes, you know, do select something, and then you added a
class or animate it or remove it from the

Carson Gross 42:15 **I think hyperscript is intended as a modern replacement for jQuery**
============================================================================================

Yeah, exactly. So and **I think hyperscript_ is intended
as a modern replacement for jQuery**, if you were to ask me, you know,
what are you targeting ? It's trying to be a modern replacement for
jQuery. Maybe slightly insane. For jQuery,

Will Vincent 42:30 Can I just add I, **So currently, you're
a college professor teaching compilers classes**, right, just in terms of
I mean, that's not like you're coming in, I saw when I came in, I taught
like a, you know, JavaScript web class. And I was like, Yeah, do the
fluffy stuff. They're just bringing you into, you know, the depths of a
CS undergrad degree. So as I just I saw that, I was like, Man, that's,
that's legit. I feel like that's where philosophically, I'll probably
end up in 20 years is also trying to, you know, write a new language to
solve all my problems, but I'm nowhere near there.

Carson Gross 43:05 Yeah, yeah, it's, it's actually believe it or not,
creating a programming language is easier than it sounds. If, if you
are in recursive descent person, the person disempowers isn't taught in
academia, I teach it because I don't like the way people teach parser
generators instead, which is a, it's much better for producing papers in
academia. But, but I think it's much worse as far as learning how
programming languages actually work. And so it's unfortunate because I
think creating a burning language is a mystery for a lot of people. And
it doesn't need to be the parser is actually easier than the tokenizer in
many situations, like breaking the string up and into tokens is that it can
actually be like the tokenizer. And hyperscript_ is totally insane. Because
it's because of some of the syntactic stuff that we support. And I should
mention hyperscript_ is, is much **it's gonna strike many people as strange,
because it's more of a natural language**.

So you would write something like:

- **on mouse enter toggle dot visible**,
- **on patch help until a mouse leave**.

And so it's got this English syntax to it. That's very can be a
little jarring. It's very easy to read, but

Carlton Gibson 44:26 can be a little just one of the things that terrifies
me is I spent a long time with that script. I tried to automate my Mac
to do something with it supposedly English syntax. And I'm just like, yeah,

Carson Gross 44:37 yeah. No, I know. And hyperscript_ again, for me is like,
that's my final project. I don't expect it to be too widely adopted. But
there's some cool stuff in it. There's some cool stuff in one aspect that's
cool. That I kind of alluded to earlier is that you embed it directly in
your HTML and so Like, as with Alpine and as with tailwind CSS, you put
everything into the HTML, it's HTML centric in that sense. And what that
gives you is that when you're curious as to what a button or whatever is
doing, you can look at that button and see everything that it's doing
there in the HTML. And that is, in contrast, with the jQuery approach
of adding, you know, over in some other JavaScript file, you would add a
handler, you know, kind of dynamically. And so I'm trying to popularize
this terminology of

Carlton Gibson 45:36

locality, `a great post on that <https://htmx.org/essays/locality-of-behaviour/>`_ for that

Carson Gross 45:39 **I'm trying to, again, popularize this notion of locality of behavior**
=============================================================================================

for this mode, right, where you're, you're localizing
all of the behavior of a particular code unit, in this case, a button
and an HTML file on that button. So you don't have to go search around
in 15 different files to figure out what that button does, and why it
looks like it looks like so HTMX also has this characteristic where we
can just look at the input and see Oh, it's posting to that, and then
doing something with the results.

And so I'm trying to, again, popularize
this `notion of locality of behavior <https://htmx.org/essays/locality-of-behaviour/>`_
to explain the popularity of libraries like tailwind. **tailwind, is the
most successful, but also Alpine HTMX**

And maybe at some point, I think

Carlton Gibson 46:25
the thing that comes up with their own dime in Django,
is we have these things called class based views. You know, your view is,
like, the thing that turns an HTML request into an HTML response.
It's the the fundamental handler of the framework. And we have these class
based views, which are great, and they're powerful. And they, you know, let
you define standard stuff very simply. **But they have this kind of awkward
hierarchy**. There's too many have too many classes, classes and too many
mix ins. And if you would, there's this whole website devoted to making
the class hierarchy browsable, so that you can navigate the class based
views. And it's like, that's the problem is that you've got five files
open in the editor, you can't learn by the time you get to the fifth one
and go, Oh, that's where it comes from. You can't remember what you were
looking for when you started this little search and to have everything
in the screen in front of you is just so important. It's so empowering.

Carson Gross 47:14  **worse is better** by Richard Gabriel, the **primary feature for easy maintenance is locality**
=====================================================================================================================

- https://web.stanford.edu/class/cs240/old/sp2014/readings/worse-is-better.html

Yeah, there. Richard, Richard Gabriel was a famous Lisp
developer. He wrote, `The worst is better paper <https://web.stanford.edu/class/cs240/old/sp2014/readings/worse-is-better.html>`_,
if you've ever read that, but that's pretty good. cs paper to read.
**Yeah, worse is better**. That's about why Unix beat Lisp.

So he's a lisp guy trying to explain why the
Unix and C world be less. But he has a quote that I liked a lot. It
says the **primary feature for easy maintenance is locality**. locality is
a characteristic of source code that enables a programmer to understand
that source by looking at only a small portion of it. And so I include
that as the sort of meat off quote in my essay on the html.org website,
about locality of behavior. And I think that captures that idea very
well. If you if you want to be able to maintain and understand code,
you have to be able to sort of see it on one spot. The more places you
have to go to understand why, how something works, the harder to maintain
that code, you're going to find it.

Will Vincent 48:17 Do you see a potential? Is there a potential downside
to putting so much just in HTML, both with HTMX and these other projects? I
mean, I don't see it, but I feel like there must, there must be, you know,
there must be some downside or potential downside?

Carson Gross 48:37 Yeah, I mean, it can be it can be overwhelming. If
you try to do too much, right, you still you still need to factor things
properly. But, but sad to me, you want to factor you want to pick the right
axis that that you're on. And so pulling something out to a component might
be appropriate. But then that component, looking at that component should
be understandable just by looking at wherever that component happens to
be defined. So you still need to factor properly. There's no doubt about
that. And, you know, there comes a point where we have extract method
for a reason, right? If you want to, there's going to come a point where
there's just too much code in line, and it's time to pull stuff out to a
method. And that's okay, because you can see, okay, this calls that method
and I can, hopefully, if my editor is powerful enough, I can control
click on that and jump over and look at the definition. And it's got a
good name, and I you know, I can live with that level of abstraction and
still understand what's going on here. Hopefully, the method isn't named
to do it or something like that, right. So there's a danger there to do it
to do it again. So you know, there, but that's, in my mind, that's just
proper software engineering. And I think that we we've fallen into this
idea of separation of concerns. rule to buy by default, just because that's
the way the web was set up, almost by default, and you know, tailwinds i
think is the most successful. Going against that grain. You know, if the
concerns are not separated at all, if you're using tailwinds, and that
to me when I first saw, you know, my mechanic, my shoulders test out,
and it just felt wrong, but, but it's quite popular. And it's proven to
be a very successful way to build when it really grows on you. So

Carlton Gibson 50:30 yeah, my first couple of hours, were just like, well,
I can't do this. No, I refuse No, no. Hang on, this is quite fun. And then,
you know, when you create reusable bits,

Carson Gross 50:41 yeah. So you know, I think that you still have to,
you still need to be a software engineer. But I think you can let go a
little bit of separation of concerns for its own sake. And maybe like a
don't repeat yourself for its own sake, maybe it's okay to repeat some
stuff a little bit, if that makes it clear what No, I

Carlton Gibson 50:59 mean, definitely. There's, there's times where you, you
know, you might have three lines in a view that strictly it's boilerplate,
but it's much better there. Because it's, it's visible forever, then it
is abstracted some hook on some superclass, you know?

Will Vincent 51:14 Yeah. And that's actually even just within the generic
class based views in Django. Someone brought to my attention, they're
asking a question that, like, Where's the success URL on was update
and delete? And it's like, well, it's actually implicitly goes to get
absolute URL in your models. So it would probably be it would definitely
be clear, to put that in there. But it's repeating yourself. So that
was the tension. So I would say in that case, maybe, you know, so yeah,
it depends. So we always try to we always try to avoid that. We're saying
that in our podcasts, but it depends. There's a trade off.

Carson Gross 51:52 There's a trade off. And it's a there's no math equation,
you can use here to figure out what the right thing is. Right. That's
unfortunate. It's just what feels right.

Carlton Gibson 52:02 **hyperscript does make front end scripting quite nice if you have to deal with any asynchronous operations**
======================================================================================================================================

Well, I wanted I wanted to ask you about one more
thing about hyperscript_ I've seen I've seen you talking about it on
Twitter, or a couple of blog posts. But I haven't had the chance to dig
in you said something that TypeScript is something like async transparent,
or they may not have been the exact phrase, what's going on it?

Carson Gross 52:18
Yeah, that's Thank you. Thank you for bringing that up.

So hyperscript_, it's, like I said, it's a little bit of a meme language
I, whether or not it's used very much in reality is it ? I mean, obviously,
I want people to use it. But but it's definitely **very speculative**.

But one of the interesting aspects of it is, is, as you said, it's what's called
**async transparent**. And what that means is that in hyperscript_,
if you do an asynchronous operation, for example, there's a, you can say,
wait three seconds, you can just, let's validate the script. So on click,
do something, and then wait three seconds, and then do something else,
add or remove class from whatever. And you can write that in hyperscript_
linearly. So you just write, you know, do action one, wait three seconds,
do action two. And the hyperscript_ runtime will wrap all of that will
the execution environment wraps all that up and makes it such that you
don't need to deal with callbacks in JavaScript? To do that, you would
use a set timeout and a callback, right. And if you were getting really
fancy, and using fetch or something like that, you might use a weight
and you know, an async tag on your on your function, and then await
a fetch, right. And in hyperscript_, you don't need to do any of that
the hyperscript_ runtime is responsible for if any expression within the
runtime produces a promise, which is the JavaScript API for asynchronous
neighbor, like a low level API for it, it will basically await that promise
resolving before it contains. And that happens at the expression level,
so that you can write hyperscript_, basically, without worrying about it,
you can just put a fetch, yeah, you can put a fetch right in the middle
of a function. And just keep writing the function the normal linear way
without having to do on a wait, or make the function, a synchronous, so
on and so forth.

And so it really takes away all that not all of it, you
can't take away all of it. But **in normal day to day programming, we take
away the vast majority of explicit asynchronous code**.

Carson Gross 55:42 I'm trying to improve the scripting infrastructure of the web
===========================================================================================

And that came out in this idea that I went on with, you know, with hyperscript_
I'm trying to improve the scripting infrastructure of the web, and
your normal front end script.
Like script writer shouldn't have to worry about asynchronous stuff.
That's wait. That's like that's just too low level news.
Want to issue a request and then take the results and put it
into something or whatever it is, whatever it is that you want to do.
And so that's what asynchronous transparency is that removing the
need to worry about whether or not a particular operation is synchronous or
asynchronous, you can just write code in the normal linear way. And then
the hyperscript_ runtime works at around.

**And I would say, technically, that's the most interesting aspect of the language.
As far as you know, programming languages go, I think that's, that's fairly unique**.

The runtime is not fast. I would not write a Bitcoin miner in it.
**But but it does make front end scripting quite nice** if you have to
deal with **any asynchronous operations**.


Event Driven Control Flow
---------------------------

- https://hyperscript.org/docs/#event-control-flow

And **there's more than you might think**, for example, animations
like CSS transitions, those are asynchronous, right? Like if you add a
class to something or you set a particular property, on a, like a length
property or capacity property, the transition that occurs is a synchronous,
you actually have to listen for events in order to do that, right ?
If you want to wait until it completes to go and do something.
**And so hyperscript_ has a bunch of infrastructure to make all that work**.

And it does allow for some interesting, there's some really interesting aspects to it, for
example of a loop construct and hypersphere, to wait for an event. And so
you can have a for loop effectively to repeat, but you can have a for loop
that waits until an event occurs for the loop determinate.
And there's a `good example on the website <https://hyperscript.org/docs/#event-control-flow>`_
where there's a button, you click one button, and it makes another button
start throbbing. And then you click the button again, and the button will
stop robbing. But rather than immediately, like stopping the throb, they
will actually complete the loop, and then stop.


And so you get a nice, smooth animation completion. Because the loop isn't,
you're not kidding, you're not removing a class or canceling something,
right? You're actually saying, You're just sending an event to this
other button, and then the loop will complete its animation loop and
then come back and check. Oh, okay, I've received the events. And now I
can finish so you can have a nice smooth finish your animation. So pretty
speculative stuff. Very, very, I don't know, I don't want to say advanced,
but it's pretty

Will Vincent 57:21 cool. If you've had them if you experience these issues
at all are super cool.

Carson Gross 57:27 Yeah, it's an interesting technology, again, much more
speculative than HTMX, I highly recommend HTMX to most web developers,
hyperscript_ to be a little bit of at this point, anyways, I was

Carlton Gibson 57:41
getting on the scale of one to 10, how advisable
Is it like that you lunatic flow ? So that's fine. That's good. That's
clear. That's

Carson Gross 57:49 why it works.

Carlton Gibson 57:52
So I have one more question about the general
ecosystem, the from my own work. So I've been working with HTMX, I've
been working with Alpine I've been, you know, tell when I've been using a
lot more but looking at then the sort of the Borderlands where it's like,
well, should I do this in one? Should I do this in the other?

And then I'm thinking about interrupt interactivity. And I don't have an
answer. Next, I'm still exploring, and I'm seeing where the API's slopped
together and where they don't. And, you know, what's your feeling on the
interop? In these kind of sprinkles approach? libraries?

Carson Gross 58:22 **I think, you know, HTMX, and Alpine are a solid, a solid combination**
============================================================================================

Yeah, well, I think, you know, HTMX, and Alpine are a solid combination.
Because Alpine gives you sort of 90% of what
you 80 to 90% of what you gives you plus the jQuery aspect of, you know,
you get that **locality of behavior** that you want to jQuery doesn't
really give you but you can do your front end scripting in a sane world.

**So I think HTMX and Alpine pair really well together**.

And, you know, I would recommend people use HTMX for driving server
interactions.  And then Alpine when you need a little bit more client
side, interactivity.  Both of those libraries are nice, and that they
can be incremental.  So you can start with a basic web app. That just is
the normal, you know, Django 1.0 version of the web app, and then and add
these behaviors, as you know, where they add the most value. Like one one
important concept in software development is a complexity budget, you know,
where you want to spend your complexity budget in on your app.  And do like
the settings page probably shouldn't have any of your complexity in it. And
if you use react or something like that, you're going to be surprised how
much of your complexity and pleasure ends up being dumped into these,
you know, relatively simple web pages. And so, I think that's, that's,
if you're sort of coming in from a pragmatic standpoint, particularly
if you have a technology like Django that's already there and works in
standard patterns.

I would try Think about it very incrementally. And there's going to be
spots where you need more front end. And for those outline or, you know,
if you're crazy, maybe trying to get hyperscript_, right?  Or jQuery is,
you know, to find my word for that stuff. And then the more you move
towards sort of server interactions.

Carlton Gibson 1:00:22 Joking, Joking aside on that point, I could I could
doubt I mean, hyperscript_ script isn't big. Right. So I could add it for
one little page.  And I could use it for one little bit. And I could try
that out. And that wouldn't be I wouldn't commit me in any great way.

Carson Gross 1:00:35 I think one of the strengths of Alpine and HTMX and hyperscript to an extent is that they're incremental
================================================================================================================================

Yeah, yeah, exactly. It's not an again, **I think one
of the strengths of Alpine and HTMX and hyperscript_ to an extent is that
they're incremental**.  You don't have they don't require you to buy into
a huge ecosystem to get the first thing done, you know, with with HTML
tags, and any of that, you can add three attributes to your web app, and
have have an active search filter on some important page in your web app,
that adds a huge amount of value for your end users.

And all you've done is included a 10k library and put in, you know,
three or four attributes anyway, shut them out.

So you're not having to rewrite the world in order to get what you want
to have happen. happen.

Will Vincent 1:01:23 That's very cool. I guess one thing I did, I wanted
to add is on the, you know, Google, Facebook and their cultures thing,
I've, I've seen a bunch of interviews with the people who created these
projects within those companies.  And I'm certainly guilty of saying,
Well, you know, it's Google and Facebook, they can do anything.  But most
of those creators, certainly of react would say, they built it to solve
a Facebook specific need. And they had to fight every step of the way to
open sources that Facebook itself was always kind of against it. So that
it's almost a little minor miracle that even comes out.

And then they get the added. I don't know, I don't feel sorry for them.
But everyone's like, hey, you're the XYZ company. Why doesn't do all these
things when they're saying, you know, it's a miracle we've and put this
out there. So sort of a cost of doing it within those companies, though,
of course, as you said, when it comes from those companies, it's going
to get all this momentum and, and push, but it's it's not as if Mark
Zuckerberg is saying we need to promote react, I don't think

Carson Gross 1:02:24
Yeah, yeah, that's true. And there's so much randomness
in the world, you know, like, it's just, it made sense. And then in one
spot, and then someone picks it up. And you know, one thing you can look
back on historically is picking up. And maybe this will let me nerd out
a little bit.

But the idea of rest and HATEOAS in the API community, right, because we
started with the recipe house was a description of the web. It was it
was a description of this hypertext based system where the networking
infrastructure behaved in a new way, it didn't behave the way old
tech clients did. It was a new architecture where a hypertext was
being exchanged.  And when that proved not the UX and issues around
that became obvious.  People started doing Ajax and Ajax started off
as XML. And they took the language from that original model from words
buildings thesis of rest, and HATEOAS, and they tried to, to use it and
the API world. And now when people hear rest and chaos, they think they
think API's.  And it really doesn't apply very well. And in my opinion,
because it especially once you get a JSON, you're no longer you don't
have a hypertext.  And so all of the infrastructure, I think all of the
network infrastructure that made a lot of sense, when we were describing
this hypertext system that the web was the original web was, it just it
kind of bled into this just stopped making sense, particularly in JSON,
it's almost in my, in my mind, kind of cargo coltie. But unfortunately,
none of the big software companies out there that could explain this,
do so and it's not in their interest. And it's not, you know, it's this
is a little nerdy.  And I was always annoyed by the guys that were like,
well, your API is me, isn't restful, you know, so I get it. Right. How
many times in the early 2000s? Were we on message boards being? You know,
oh, this is a third level of

Carlton Gibson 1:04:30
Python with pythonic. It's not pythonic it's really we start days.

Carson Gross 1:04:35 Yeah, exactly. Exactly. But, you know, it just it's,
it's funny how this stuff works out. So, you know, people hate chaos,
without even understanding really what it is at this point. And, you
know, if you're using a JSON API, yeah, of course you hate it, because
it's not hypertext. hypertext can't be the engine of application state,
or it's going to be real hard to maintain using an application state. If,
if you're going down that road, so hopefully you know that these things
do appear to go in cycles. And so hopefully we can bring people back
to this original model for the web. I think the examples, you know, you
pointed out the examples, and I think that's probably the best move I've
made was creating that examples page, I need to do that for hyperscript_
as well. That just shows, here's what you can accomplish, with a little
bit of a little bit of hypertext, with a little bit of an app with an
extension to hypertext. And that speaks to people a lot more than me
sitting around, you know, explaining why they're wrong about the web. I
do like doing that. Sometimes. I really like to give that talk at Google.

Carlton Gibson 1:05:46 Well, you should come to a Django con. And given
that,

Carson Gross 1:05:49 Yeah, that'd be fine.

Carlton Gibson 1:05:51 Speaking of which, I mean, you've there are a
few people giving talks at the Django con is just coming up on HTML. And
so we'll put that that will those talks will be before this show comes
out. So we'll put the links for those in the show notes as well. She
really strikes you really struck a chord in our community?

Carson Gross 1:06:10 Yeah, I'm very excited. You know, it's just like, I
never would have expected. Like I said, I've probably written 100 lines of
Python in my life. If I do start another project, I'm going to take a hard
look at Django just because the community is obviously, you know, Python
is just grown leaps and bounds. I think even JavaScript has kind of been
eclipsed by Python at this point. And so. Yeah, but so it's, I wouldn't
have expected this. And that's kind of the way life goes. I wouldn't have
expected that rewriting. intercoolers. Yes, either. But then they said,

Carlton Gibson 1:06:51 it's good that you managed to make use of the
confinement time.

Carson Gross 1:06:54 Yeah. Jeremy saved for that first couple of weeks.
That was pretty crazy. So

Will Vincent 1:07:00 thank you so much for taking the time and agreeing
to come on our Django podcast to talk about this. **As we said, I'm hearing
a lot about HTMX in the Django world. So hopefully, this podcast opens
it up to even more people**.

Carson Gross 1:07:14
Yeah. And I want to encourage people to you know, there's a `CMS account
on Twitter <https://x.com/htmx_org>`_, which it's just me, fancy going on.

I try not to be too I'm pretty informal person. So I say some pretty silly
stuff. But the discord as well, which is you can get on@html.org slash
Discord. If you need help, don't struggle.

`Stack Overflow is still pretty raw <https://stackoverflow.com/questions/tagged/htmx>`_.
As far as HTMX explanations go. Hopefully, it's you know, a lot
of it's simple enough, you don't need a ton of help. But if you do need
out the community on the discord channel is pretty is very solid. There
are some people that know the why and know the infrastructure know,
the library should say very well. And we try to keep it very friendly.

Will Vincent 1:07:59
Yeah, there's also there's um, `Django has a forum <https://forum.djangoproject.com/>`_
that Django project comm official forum that is not as used as it should
be. But people that had discussion is completely top notch. So I imagine
they'll be more HTML on there as well.

Carson Gross 1:08:16
Okay, is there a way on that forum to listen for each
jennex ? Questions? I need to hold on me write this down. What's it called?

Will Vincent 1:08:24 forum dot Django project com (https://forum.djangoproject.com/) ?
Yeah, you can. It's it's what is it? It's it's not Discord.

It's some discourse, the discourse the other one. But yeah, you've created
an account. And you can think you can follow stuff. And you can see if
you've, you know, things that you've commented on have action.

I mean, for example, so one of the posts
I have is talking about where to put the logic in Django, and that's two
years old. And that's had a lot of discussions because it's, you know,
sort of the equivalent of where's the state? But I, you know, I opt in
to seeing when people comment on that.

Carson Gross 1:08:59 Yeah, I'm trying to on all these platforms set up
alerts for when someone mentions a shin is just so I can jump in, and
help if possible,

Carlton Gibson 1:09:08

too, but Oh, she wanted to shout out while you've
got the have we covered all the bases?

Carson Gross 1:09:15 **I'm very happy that it's proving useful and kind of giving Django some new legs**
==========================================================================================================

While I've got you here ? No, I think, you know, again,
just I'm really thankful that people are picking this up and willing to
take a look at an older, you know, way of doing web development, sort of,
I would say the original way.

And the you know, what, I encourage people to just maybe have a little
bit of courage and, and going against the grain because I do think that
for a lot of web apps, there's a lot of great technologies that are just
laying around waiting for web developers to pick up a news that would
simplify things dramatically and make both their lives as well as the
lives of their users a lot better.  So you know, I'm extremely thankful
to you guys for This is all JavaScript stuff. So thank you for talking
to me about it. And I'm very happy that it's proving useful and kind of
giving Django some new legs, you know, as far as being exciting times.

Carlton Gibson 1:10:12 Thank you.

Will Vincent 1:10:13 Yeah. Thank you. As ever, we're at Jango chat,
calm chat Django on Twitter.  And we'll see everyone next time.

Carlton Gibson 1:10:20 Bye bye. See you next time. Bye bye.

Carson Gross 1:10:22 Cheers
===============================


.. _hyperscript: https://hyperscript.org/
