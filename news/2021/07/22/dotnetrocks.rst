.. index::
   pair: Audio; Conference (2021-07-22)

.. _htmx_carson_2021_07_22_dotnet:

=====================================================
Thursday, July 22, 2021, 1749 htmx with Carson Gross
=====================================================

- https://x.com/htmx_org
- https://dotnetrocks.com/?show=1749


.. figure:: dotnet_rocks.png
   :align: center

Why should form tags and submit events have all the fun ?

Carl and Richard talk to Carson Gross about htmx, a small Javascript
library that extends HTML through attributes so that almost any element,
on any event, can trigger a GET, POST, PUT, PATCH, or DELETE.

Carson talks about building sophisticated web apps with HTML, rather than
tons of JavaScript, and really getting into the original hypertext web
metaphors - **arguably the way Tim Berners Lee intended**.

With a simple learning curve, it doesn't take much effort to get started
with htmx, just add a few attributes and start exploring what HTML really
could be doing for your apps !
