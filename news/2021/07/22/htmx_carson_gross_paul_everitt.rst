.. index::
   pair: Audio; Conference

.. _htmx_carson_2021_07_22:

================================================
2021-07-22 Simple, Fast Frontends With htmx
================================================

- https://x.com/paulweveritt
- https://x.com/htmx_org
- https://blog.jetbrains.com/pycharm/2021/07/webinar-simple-fast-frontends-with-htmx-with-carson-gross/
- https://www.youtube.com/watch?v=cBfz4W_KvEI


.. figure:: carson_everitt.png
   :align: center

The web has changed in recent years. Modern websites mean lots of JavaScript
on the frontend, sometimes supplanting HTML all together.
You’re left with an incomprehensible, brittle pile of 3,000 npm dependencies,
knowing it will never work in 2 years from now, much less 20 years.

It doesn’t have to be that way. In this webinar we introduce htmx, part
of a recent trend to slim down frontends and return to the original
principles of the web, while still providing faster interactivity. htmx
has taken off recently, and Carson Gross, its creator, is dropping by to
explain the why, how and what. With htmx, you use HTML -- yes, HTML -- as
the organizing idea.

We’ll give you an orientation then show lots of examples in action.

Carson is a WebStorm fan, and since his motto is the HOWL stack --
hypertext on whatever language -- he and Paul will collaborate to show
usage on plain servers and Python backends.

Speaker: Carson Gross

Carson runs Big Sky Software, teaches CS at a university, and is the CTO
of LeadDyno.

He is the creator of htmx, hyperscript and intercooler.js, and has been
doing web programming since the late 1990s.
His technical approach is to look for hot new trends in the industry and
then do the opposite of that.

Outline
============

- https://blog.jetbrains.com/pycharm/2021/07/webinar-simple-fast-frontends-with-htmx-with-carson-gross/

- Quick demo
- What is htmx?
- The problem being solved
- Static demos
- FastAPI demos
- Django demos
- Advanced discussion



Simple, Fast Frontends With htmx
====================================


The web has changed in recent years. Modern websites mean lots of JavaScript
on the frontend, sometimes supplanting HTML all together.

You’re left with an incomprehensible, brittle pile of 3,000 npm dependencies,
knowing it will never work in 2 years from now, much less 20 years.

It doesn’t have to be that way. In this webinar we introduce htmx, part
of a recent trend to slim down frontends and return to the original principles
of the web, while still providing faster interactivity. htmx has taken
off recently, and Carson Gross, its creator, is dropping by to explain
the why, how and what. With htmx, you use HTML -- yes, HTML -- as the organizing idea.

We’ll give you an orientation then show lots of examples in action.

Carson is a WebStorm fan, and since his motto is the HOWL stack -- hypertext
on whatever language -- he and Paul will collaborate to show usage on plain
servers and Python backends.

Speaker: Carson Gross

Carson runs Big Sky Software, teaches CS at a university, and is the CTO
of LeadDyno.  He is the creator of htmx, hyperscript and intercooler.js,
and has been doing web programming since the late 1990s.
His technical approach is to look for hot new trends in the industry and
then do the opposite of that.

Transcription
==============

.. literalinclude:: transcription.txt
   :linenos:
