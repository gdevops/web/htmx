.. index::
   pair: Django ; django-forms-dynamic

.. _django_forms_dynamic_2021_12_17:

====================================================================================================================================================
2021-12-17 **django_forms_dynamic Resolve form field arguments dynamically when a form is instantiated, not when it's declared** by Jamie Matthews
====================================================================================================================================================

- :ref:`tuto_django:jamie_matthews`
- :ref:`tuto_django:django_forms_dynamic`
