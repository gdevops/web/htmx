
.. _thisweek_2021_12_19:

==========================================================
2021-12-19 Thisweek Issue N°8: **Hyper Media Presence**
==========================================================

- https://thisweek.htmx.org/issue/8/


Daniel, A Tour of myPrayerJournal v3: Conclusion
=====================================================

The thrilling conclusion to the Prayer Journal Pentalogy.

When I say “simplicity,” I am speaking of a lack of complexity, not a
naïveté of approach.

Daniel - East TN BTW - I can’t remember who said they weren’t sure about
writing about their code publicly. But, whoever you are, in this post
I actually talk poorly about my own code. 🙂

Daniel - East TN They can’t talk bad about you if you beat them to it! LOL

Daniel - East TN you can't X if you don't Y meme gif

Discord · December 7, 2021, 9:44 PM UTC

If you’ve made anything with htmx/_hyperscript, please share it! It’s lovely to hear from you. 💙

https://blog.bitbadger.solutions/2021/a-tour-of-myprayerjournal-v3/conclusion.html


Dave Syer, Client Side Development with Spring Boot Applications, Spring Blog
==================================================================================

HTMX is a library that allows you to access modern browser features
directly from HTML, rather than using javascript. It is very easy to use
and well suited to server-side rendering because it works by replacing
sections of the DOM directly from remote responses.

It seems to be well used and appreciated by the Python community.

https://spring.io/blog/2021/12/17/client-side-development-with-spring-boot-applications

Arham Jain, Building a simple typing test website using hyperscript
=====================================================================

I’m not claiming that hyperscript will replace Javascript, but it is very
interesting to use.
The strangest thing about this project was that I found it slightly more
difficult to write code, but much easier to read my code back.

What we believe in. 💙

https://arhamjain.com/2021/12/18/hyperscript-simple-type.html


htmx on JavaScript Jabber podcast
====================================

JavaScript Jabber @JSJabber

Check out this week's episode of #JavaScriptJabber with Carson Gross @htmx_org.
Come to know all about the pros and functions of HTMX and InterCooler.

#JSJ: HTMX and InterCooler

<https://rfr.bz/t3i5x11> <https://rfr.bz/t3i5x11>

Twitter · December 14, 2021, 11:42 PM UTC

https://javascriptjabber.com/htmx-and-intercooler-ft-carson-gross-jsj-513


_hyperscript on devMode.fm podcast
========================================

devMode.fm podcast @devmodefm

🎙 devMode podcast episode 123: "Hype for Hyperscript" 🌟

@gaijinity & guest host @ben_pylo talk with @htmx_org & @DenizAksimsek
about their web scripting language Hyperscript & why the world really
does need another scripting language! #webdev #frontend

Twitter · December 14, 2021, 2:51 PM UTC

https://devmode.fm/episodes/hype-for-hyperscript


The new _hyperscript Cheatsheet
=================================

Keeping all the features of event listeners, ways of finding elements,
the intricacies of variable scopes and the syntax of every single command
just a glance away — the _hyperscript cheatsheet!

hyperscript-cheatsheet.pdf

Fubarrr ↪ Re. CapnKernel That’s awesome! You’re awesome! Awww, shucks,
thanks! Yes, I do indeed take full credit for the fact that there now
exists a probably 90% complete Hyperscript Cheatsheet 😎.

I mean, do you know how hard it is to create a whole Google doc from
scratch, complete with title and single “TBC” line… and remembering
(aka googling) how to share it so anyone can edit it?! To be fair, I
guess a little credit should go to @Deniz Akşimşek for his very small
contribution (although I did find it a bit cheeky of him tbh 🙄) of
replacing my “TBC” line with 2 pages of actual cheatsheet content.

Still, much like Edison is remembered for inventing the lightbulb, I
shall go down in the annals of history as The Creator of The Hyperscript Cheatsheet ™

Discord · December 13, 2021, 3:02 AM UTC

- https://discordapp.com/channels/725789699527933952/796428329531605032/919786407290097725


https://thisweek.htmx.org/assets/2021-12-19/hyperscript-cheatsheet.pdf


Avoid listening to htmx:load when you have big responses
===========================================================

Fubarrr Btw… remember that thing I said before about htmx swapping faster
than intercooler? For some reason, if I’m swapping a bunch of <option>
elements into about 15 selects with htmx… the browser freezes for several seconds

December 16, 2021, 6:21 PM UTC

Fubarrr Ok folks, so after @1cg and @Deniz Akşimşek had a look at the
profiling… turns out that it’s our codebase that’s to blame for that
crazy 7.2 s scripting time on page load ☝️ , NOT htmx 😁 🤦‍♂️

Deniz Akşimşek new htmx best practice just dropped: avoid htmx:load for
responses containing large numbers of elements

Discord · December 16, 2021, 4:52 PM UTC
- https://discordapp.com/channels/725789699527933952/796428329531605032/921082469103583285


How to submit an htmx form using JavaScript ?
==================================================

TL;DR: Use form.requestSubmit() instead of form.submit().

Deniz Akşimşek anyone who had trouble submitting an htmx form with
JS: https://developer.mozilla.org/en-US/docs/Web/API/HTMLFormElement/requestSubmit

Discord · December 16, 2021, 8:46 AM UTC
- https://discordapp.com/channels/725789699527933952/725789747212976259/920960095662276618

