
.. _chained_2021_12_17:

=================================================================================================================
2021-12-17 Django and HTMX #10 - Chained Dropdowns with Django and HTMX by https://x.com/bugbytesio
=================================================================================================================

- https://www.bugbytes.io/posts/django-and-htmx-chained-dropdown/
- https://www.youtube.com/watch?v=YXzb4tw2IrI
- https://htmx.org/examples/value-select/

Description
============

In this video, we build a simple chained dropdown using Django and HTMX.

We will look at a simple scenario where the first dropdown contains a
set of university courses, and the second dropdown contains a set of
modules for that course.

The contents of the second dropdown depend on the selection of the first
dropdown, and we will use HTMX to perform actions when an option in the
first dropdown is selected.

HTMX's documentation refers to this as **cascading selects**.

▶️ Full Playlist: https://www.youtube.com/playlist?list=PL-2EBeDYMIbRByZ8GXhcnQSuv2dog4JxY


📌 𝗖𝗵𝗮𝗽𝘁𝗲𝗿𝘀:
00:00 Intro
01:14 Creating database models
02:58 Creating management command to populate database
04:23 Adding URL, view and templates
06:01 Adding Select Elements (Dropdowns)
07:39 Adding HTMX Attributes

☕️ 𝗕𝘂𝘆 𝗺𝗲 𝗮 𝗰𝗼𝗳𝗳𝗲𝗲:
To support the channel and encourage new videos, please consider buying me a coffee here:
https://ko-fi.com/bugbytes

𝗦𝗼𝗰𝗶𝗮𝗹 𝗠𝗲𝗱𝗶𝗮:
📖 Blog: https://www.bugbytes.io/posts/django-and-htmx-chained-dropdown/

👾 Github: https://github.com/bugbytes-io/django...
🐦 Twitter: https://x.com/bugbytesio

📚 𝗙𝘂𝗿𝘁𝗵𝗲𝗿 𝗿𝗲𝗮𝗱𝗶𝗻𝗴 𝗮𝗻𝗱 𝗶𝗻𝗳𝗼𝗿𝗺𝗮𝘁𝗶𝗼𝗻:
HTMX Value Select: https://htmx.org/examples/value-select/
