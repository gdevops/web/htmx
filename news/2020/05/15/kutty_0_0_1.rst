.. index::
   pair: kutty; intercoooler.js
   pair: kutty; htmx

.. _kutty_0_0_1:

=====================
Kutty 0.0.1 Release
=====================

- https://htmx.org/posts/2020-5-15-kutty-0.0.1-is-released/
- https://x.com/__gotcha/status/1469260774669496324?s=20
