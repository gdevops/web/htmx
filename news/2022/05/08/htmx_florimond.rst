

.. _florimond_2022_05_08:

==============================================================================
2022-05-08 **@htmx_org tient ses promesses, ça dépote vraiment** + pylenium
==============================================================================


Florimond Manca
=================

.. figure:: images/florimond.png
   :align: center

- https://x.com/florimondmanca/status/1523341664923705344?s=20&t=xdxeB8PZCvlXIGJeUqpdlg

@htmx_org tient ses promesses, ça dépote vraiment

Pouah, @htmx_org tient ses promesses, ça dépote vraiment. Je pressens
qu'il pourrait suffire en lieu et place d'un framework frontend dans
la majorité des cas.

L'intérêt d'un point de vue éco-conception numérique m'interroge aussi
beaucoup. Envie de passer en mode R&D ouverte…


.. figure:: images/florimond_htmx.png
   :align: center

   https://discord.com/channels/725789699527933952/725789747212976259/973209403937402910



- https://x.com/florimondmanca/status/1523383771172593664?s=20&t=xdxeB8PZCvlXIGJeUqpdlg

Django et son système de templating.

Je publierai mon repo d'exploration à l'occasion. Il comprend notamment
les vues (dont endpoints pour htmx qui renvoient des fragments de HTML)
+ les templates + des tests e2e avec Playwright.

Friday love, pylenium from CarlosKidman
==========================================

- https://x.com/IamFridayLove/status/1523384157509804032?s=20&t=xdxeB8PZCvlXIGJeUqpdlg
- https://x.com/CarlosKidman
- https://github.com/elsnoman/pyleniumio

C’est ce qu’on fait en production :) django/htmx/hyperscript
In browser tests avec pytest/pylenium
