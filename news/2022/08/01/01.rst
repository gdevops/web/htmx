

.. _htmx_2022_08_01:

=====================
2022-08-01
=====================

.. toctree::
   :maxdepth: 4

   return_to_hypermedia_solving_javascript_fatigue_using_fundamental_web_architecture
