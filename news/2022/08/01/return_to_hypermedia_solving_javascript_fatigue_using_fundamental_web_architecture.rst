

.. _carson_gross_2022_08_01:

=================================================================================================================
2022-08-01 Return To Hypermedia: Solving Javascript Fatigue Using Fundamental Web Architecture
=================================================================================================================

- https://www.youtube.com/watch?v=LRrrxQXWdhI


In the last decade, the Single Page Application (SPA) architecture and
the JSON API has begun to supplant the original Multi-Page Application
architecture of web.

This has allowed much more sophisticated web applications to be built
and the approach has been formalized by libraries such as React and Vue.

In the last few years, however, we have seen the rise of an associated
phenomena: Javascript Fatigue in which developers and development teams
admit to being overwhelmed by the complexity of the SPA architecture.
