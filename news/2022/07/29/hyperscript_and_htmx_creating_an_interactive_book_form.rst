

.. _htmx_django_2022_07_29:

=======================================================================================
2022-07-29 Hyperscript and HTMX - Creating an Interactive Book Form by @bugbytesio
=======================================================================================

- https://www.youtube.com/watch?v=lWdzCUxsMNc
- https://github.com/bugbytes-io/django-hyperscript-bookform
- https://x.com/bugbytesio


Description
============

In this video, we create a small, interactive user interface containing
a form and some buttons, using Hyperscript to control when certain
elements are shown, and using Hyperscript to clear input elements in our
form upon certain actions.

We use HTMX to handle form submissions, and to load new content, without
a page refresh being required.

Elements are styled with TailwindCSS.


Starter code is available here: https://github.com/bugbytes-io/django-hyperscript-bookform


This video was inspired by ideas from David - thank you, David!

📌 𝗖𝗵𝗮𝗽𝘁𝗲𝗿𝘀
===========


- 00:00 Intro
- 01:48 Creating a Book model
- 02:42 Creating a Book Form
- 04:49 Adding Form to Context
- 05:24 Installing & Using django-widget-tweaks
- 07:30 Styling form with TailwindCSS
- 10:17 Using Hyperscript to Toggle Classes
- 11:51 Submitting Form with HTMX
- 19:33 Using Hyperscript and Custom HTMX Events
- 21:36 Using Hyperscript to Clear Inputs
- 23:22 Showing Books Below Form with HTMX


☕️ 𝗕𝘂𝘆 𝗺𝗲 𝗮 𝗰𝗼𝗳𝗳𝗲𝗲
===================

To support the channel and encourage new videos, please consider buying me a coffee here:
https://ko-fi.com/bugbytes

𝗦𝗼𝗰𝗶𝗮𝗹 𝗠𝗲𝗱𝗶𝗮
==============

- 📖 Blog: https://www.bugbytes.io/posts/
- 👾 Github: https://github.com/bugbytes-io/django...
- 🐦 Twitter: https://x.com/bugbytesio

📚 𝗙𝘂𝗿𝘁𝗵𝗲𝗿 𝗿𝗲𝗮𝗱𝗶𝗻𝗴 𝗮𝗻𝗱 𝗶𝗻𝗳𝗼𝗿𝗺𝗮𝘁𝗶𝗼𝗻
====================================

- Hyperscript: https://hyperscript.org/
- HTMX: https://htmx.org/
- HTMX Events: https://htmx.org/events/
- django-widget-tweaks: https://pypi.org/project/django-widge...
- TailwindCSS Buttons: https://v1.tailwindcss.com/components...
- TailwindCSS Forms: https://v1.tailwindcss.com/components...

