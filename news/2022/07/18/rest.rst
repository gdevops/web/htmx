.. index::
   ! New REST
   ! REST classic
   ! REST Zero

.. _rest_2022_07_21:

=================================================================================================
2022-07-21 maybe we should call HTTP JSON APIs "New REST" & hypermedia APIs "REST Classic"
=================================================================================================

- https://htmx.org/essays/how-did-rest-come-to-mean-the-opposite-of-rest/

Naming
========

- https://x.com/htmx_org/status/1549401419266015238?s=20&t=QJbYMxG6jRVE8IwpZLENbQ

::

    maybe we should call HTTP JSON APIs "New REST" & hypermedia APIs "REST Classic"

    GraphQL APIs can be "REST Zero"



- https://x.com/tpierrain/status/1549267078212902912?s=20&t=QJbYMxG6jRVE8IwpZLENbQ


::

    REST must be the most broadly misused technical term in computer programming history.”
    https://htmx.org/essays/how-did-rest-come-to-mean-the-opposite-of-rest/
