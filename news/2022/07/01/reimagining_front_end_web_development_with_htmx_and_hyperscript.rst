.. index::
   pair: Reimagining front-end web development with htmx and hyperscript ; Owen Jones (2022-07-01)

.. _htmx_hyperscript_2022_07_22:

========================================================================================================
2022-07-01 **Reimagining front-end web development with htmx and hyperscript** by Owen Jones
========================================================================================================

- https://nomadiq.hashnode.dev/reimagining-front-end-web-development-with-htmx-and-hyperscript
- https://nomadiq.hashnode.dev/
- https://hashnode.com/@tok3rat0r
- https://github.com/tok3rat0r
- https://github.com/nomadiq-sw/member-zone


Introduction
==============

- https://github.com/nomadiq-sw/member-zone

We all know that to create an interactive front end for your website you
need JavaScript. Not just vanilla JS, mind: we're in 2022, and to create
an acceptable UI you need to be using a framework like React or Vue.js.

Right ?

Wrong.

In recent years, a few mavericks and renegades have started to turn away
from the world of JS frameworks and the inevitable bloated node_modules
folders.
But what if you want a smooth single-page app experience, rather than
waiting for the whole page to render every time you click a button?

Of course, nobody wants to write a load of boilerplate JS for every
little interaction.
This is where hypermedia in the form of htmx and hyperscript come in.

These two open-source toolkits, both developed by Big Sky Software and
collaborators, provide a host of HTML attributes to deal with AJAX requests,
partial DOM updates, CSS transitions, event handling, Server-Sent Events
and WebSockets in a clear, user-friendly syntax.

There are a number of excellent tutorials online demonstrating the
capabilities of these tools; I particularly like `BugBytes' tutorials on
YouTube <https://www.youtube.com/channel/UCTwxaBjziKfy6y_uWu30orA>`_.

In this article, I'm going to show you how I used them in my `current
project <https://github.com/nomadiq-sw/member-zone>`_, which is a simple membership/subscription tracking site made with Django.

