

.. _htmx_2022_07_18:

=====================
2022-07-18
=====================

.. toctree::
   :maxdepth: 4

   reimagining_front_end_web_development_with_htmx_and_hyperscript
