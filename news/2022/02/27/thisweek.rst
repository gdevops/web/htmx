

.. _thisweek_11_2022_02_27:

==============================================================================
2022-02-27 Issue N°11: **Bump Version** by https://denizaksimsek.com/
==============================================================================

- https://thisweek.htmx.org/issue/11/
- :ref:`thisweek_10_2022_02_18`
- :ref:`thisweek_12_2022_03_13`


htmx 1.7.0 released
=====================

- https://x.com/htmx_org/status/1496123303290888192

htmx.org @htmx_org happy to announce the release of htmx 1.7.0: https://htmx.org/posts/2022-02-22-htmx-1.7.0-is-released/

- a new hx-sync attribute for synchronizing requests between elements
- many new extensions including alternative sse & ws support
- hx-disinherit to disable inheritance
- much more!

Enjoy!


The release also includes:

- Server-Send Events and WebSocket support was factored into extensions,
- the new Alpine-Morph plugin, which preserves Alpine state during htmx swaps
- the Restored plugin, which lets elements know when they’ve been restored from the history cache
- the Loading States plugin, which helps you show loading to the user

That’s still not all! Read the full release notes:

- https://htmx.org/posts/2022-02-22-htmx-1.7.0-is-released/


\_hyperscript 0.9.5 released
==============================

Highlights:

- The add command now supports a when clause to allow conditionally adding/removing a class, etc. in a single command
- The beep expression is now available to assist in debugging hyperscript code

add .highlighted to the <p /> in beep! <div.highlight />
	-- Logs the value of `<div.highlight />`.
add .highlighted to beep! the <p /> in <div.highlight />
	-- Logs the value of `the <p /> in <div.highlight />`.

- https://hyperscript.org/posts/2022-02-22-hyperscript-0.9.5-is-released/



Intercooler.js to htmx migration guide
========================================

- https://htmx.org/migration-guide/
- https://x.com/htmx_org/status/1497227566406537218


happy to announce that corporate sponsor & long time supporter @commspaceapp
has smoothly upgraded their SAAS app from intercooler.js/jquery to htmx/hyperscript

they have contributed an upgrade guide here: https://htmx.org/migration-guide/ 👏 👏 👏

Twitter · February 25, 2022, 3:10 PM UTC

Intercooler is still maintained, but htmx provides new features and will
continue to gain more. As such, you might want to port your intercooler
apps to htmx + _hyperscript, which you can do with this guide.

The guide features such gems as:

htmx follows the approach of trying to keep the core small, with a smaller
set of available attributes that are mostly focused on content loading and swapping.

Using the htmx events system with vanilla javascript, alpine.js or hyperscript.

Hyperscript is a small, open scripting language designed to be embedded
in HTML, inspired by HyperTalk and is a companion project of htmx.

None. No direct equivalent functionality exists (TBC)

https://htmx.org/migration-guide/



missing.css is ready for contributions
=========================================

- https://github.com/bigskysoftware/missing/blob/2aa556d/Contributing.md

The skeleton of the project is complete, and eagerly waiting to be filled
in with attractive styles.

Check out the contributing guidelines, which feature the project’s standards
for users, development instructions and a map of the project directory structure:

https://github.com/bigskysoftware/missing/blob/2aa556d/Contributing.md

missing.css is developed on a “free to use, pay to demand” basis.


Functional Web Apps ?
=======================

- https://discordapp.com/channels/725789699527933952/725789747212976259/945741691736948746
- https://arc.codes/docs/en/get-started/quickstart

MSM I’ve been building a thing for work using the `https://arc.codes/ <https://arc.codes/docs/en/get-started/quickstart>`_
framework, and noticed that they have started to advise that lambdas/functions
should return HTML rather than JSON.

there was this interesting line in `https://fwa.dev/before-and-after <https://fwa.dev/before-and-after>`_
that seems HOWLy:

Discord · February 22, 2022, 5:59 PM UTC

A different way to solve all this complexity is to move up the cloud vendor
stack. A Functional Web App (FWA) is authored completely as single-responsibility
cloud functions that can render HTML-first dynamically, incorporating
the full-stack such as a managed database.

Interesting…


