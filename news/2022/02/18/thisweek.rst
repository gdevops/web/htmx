
.. _thisweek_10_2022_02_18:

===========================================================================
2022-02-18 Issue N°10: **The missing link** by https://denizaksimsek.com/
===========================================================================

- https://thisweek.htmx.org/issue/10/
- :ref:`thisweek_11_2022_02_27`


missing.css: A CSS framework from the htmx community
========================================================

missing.css is the missing CSS library that we all wished existed:

It starts with decent default styling for vanilla HTML, akin to classless
CSS libraries

It offers a small set of components based on purely standard, semantic
HTML, utilizing ARIA where appropriate, such as tabs.

Finally, it offers a small & curated set of class-based utilities for
tweaking the styling of a site

The objective of missing.css is to minimize the neccessary intervention
in the HTML by allowing developers to start with a good out of the box
experience, then build common components using plain, semantic HTML and
then, only if necessary, apply a minimal number of classes to achieve.

- https://missing.style/

missing.css will be developed by the htmx community under the leadership
of yours truly.

To contribute:

- `Repository <https://github.com/bigskysoftware/missing/`>_
- `GitHub Discussions <https://github.com/bigskysoftware/missing/discussions>`_
- `Discord <https://htmx.org/discord>`_, find the #missing_dot_css channel-

We’re also seeking funding for missing.css, and our other projects too.


Become a sponsor:

htmx.org @htmx_org

The Missing Stylesheet project has kicked off w/ @DenizAksimsek as lead dev!
We have two corporate sponsors:
https://bigsky.software & @commspaceapp
If you are interested in sponsoring (a 1 time affair) please contact us
You will get a logo spot on the main site + karma!

- https://x.com/htmx_org/status/1494681462624432129


demo.htmx.org: a demo environment for htmx snippets
=====================================================

Simply drop <script src="https://demo.htmx.org"></script> into an HTML
file and you’ll be set up with htmx, _hyperscript and a mock server.

Here’s an example—a server- and client-side counter:

::

    <script src="https://demo.htmx.org"></script>

    <h2>Server Counter</h2>
    <output id="server-n">0</output>
    <button type="button" hx-post="/n?inc=1"  hx-target="#server-n">+</button>
    <button type="button" hx-post="/n?inc=-1" hx-target="#server-n">-</button>
    <!-- Server -->
    <script>let serverInt = 0;</script>
    <template url="/n">${serverInt += Number(inc)}</template>

    <h2>Client Counter</h2>
    <output id="client-n">0</output>
    <button type="button"
        _="on click increment #client-n's textContent">+</button>
    <button type="button"
        _="on click decrement #client-n's textContent">-</button>


- https://demo.htmx.org/


htmx now sponsored by Craft CMS
=====================================

htmx.org @htmx_org
and for corporate sponsor #3…
@CraftCMS has entered the chat
http://htmx.org + http://craftcms.com = 💪
🙏

- https://x.com/htmx_org/status/1491633693936418818

vscode-hyperscript has experimental Sublime support
======================================================

gnat There any syntax highlighting available for sublime?
Fubarrr ↪ Re. gnat There any syntax… I only know of VS Code.
@Deniz Akşimşek will know
Deniz Akşimşek Nope
Deniz Akşimşek but I’m pretty sure it uses the same format as vscode,
so it shouldn’t be too hard to convert

Discord · February 17, 2022, 11:27 AM UTC

- https://discordapp.com/channels/725789699527933952/796428329531605032/943831075229147177

Indeed, it wasn’t. There were some cryptic errors, but @gnat helped me
debug over discord for a day, and wrote an injection syntax that makes
it work inside HTML too — thanks! 💙


gnat So the basic laymens instructions:

- Open Sublime
- Preferences -> Browse Packages…
- Copy both files into User directory.
- Restart Sublime
- View -> Syntax -> User -> HTML (_hyperscript)

Discord · Invalid DateTime
-

The “both files” being talked about are syntaxes/_hyperscript.tm Language
and HTML (_hyperscript).sublime-syntax in the vscode-hyperscript repo.
(I’ll be looking into packaging it).

vscode-hyperscript is also seeking maintainers.

Deniz Akşimşek speaking of: anyone lmk if you can lend a hand for the
vscode-hyperscript extension (i don’t run vs code as often anymore).

dw if you have no experience, i didn’t either & i can help you get familiar
with the code

Discord · February 10, 2022, 4:01 PM UTC
- https://discordapp.com/channels/725789699527933952/725789747212976259/941363986006409256

- https://github.com/dz4k/vscode-hyperscript


Dark Star: a Python web framework with filesystem routing
============================================================

lllama Hello all - just a little curio that I’ve put together: https://lllama.github.io/dark-star/
lllama It’s (yet another) “framework” using Starlette. I got thinking
about Locality of Behaviour (thanks to @1cg) and wondered if you could
put the templates right next to the code for a view. The idea is that a
fair chunk of the boilerplate is removed and replaced by putting files
in the filesystem. (kinda like a webserver - old things are new again.)
lllama And that’s basically it. But it does get you up and running fairly
quickly. And having little pieces of HTML living at URLs is nice and easy.
Seems to fit in with what we’re doing here.
lllama I’m dropping it here in case anyone fancies a play and giving
some feedback. If I get the chance then I’ll try and put together a
Real World (https://realworld-docs.netlify.app/) example - that should
help to shake out any issues. Or show that the idea doesn’t really work 😄

Discord · February 8, 2022, 10:53 PM UTC
- https://discordapp.com/channels/725789699527933952/864934037381971988/940742050847031316


