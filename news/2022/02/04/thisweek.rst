

.. _thisweek_9_2022_02_04:

===========================================================================
2022-02-04 Issue № 9: Making The Rounds by https://denizaksimsek.com/
===========================================================================

- https://thisweek.htmx.org/issue/9/


Dan Jacob, Django, HTMX and Alpine
======================================

::

    A sufficiently skilled team can of course mitigate all of these problems;
    but most teams do not have sufficient skill (or time and resources)
    to build a such a high quality SPA.

    The problem however is not with the SPA architecture itself, but
    rather the current dominant mindset of SPA as the default paradigm
    for all web projects, rather than one possible approach among many
    others for careful consideration based on the requirements of the
    project and the skills of the development team.

    That’s why libraries such as HTMX and Alpine are a great addition to your toolkit.

https://danjacob.net/posts/djangohtmxalpine/

The New Stack, Htmx: HTML Approach to Interactivity in a JavaScript World
===========================================================================

An interview with our own Carson on htmx that introduces the library,
and the SPA/MPA drama surrounding Rich Harris’s talk.

I didn’t want to talk about Twitter drama, as it’s the worst kind of
flamewar that can exist, which is partly why this issue is so short
despite being a month late.

https://thenewstack.io/htmx-html-approach-to-interactivity-in-a-javascript-world/

Hyperwordle
===============

After typehand, a typing speed test, community member Arham Jain wrote
a Wordle clone in hyperscript, replicating most of the functionality
in a ~60 LOC HTML file that is fully view-sourceable.
(Take a look — there’s some clever stuff in there!)

The game made it to The Orange Site, where it made it to the frontpage.

One DEV.to member wrote a counterpart in Elm and used it to compare the
two languages. The resulting article, Elm vs HyperScript -
A Wordle implementation, is a joyous exhibit of how hilarious the _hyperscript project is.

Play a round yourself!

https://arhamjain.com/hyperwordle/


Request synchronization in htmx
==================================

::

    A common problem in AJAX driven applications is synchronization: how
    to two elements on a page synchronize the requests that they need to
    make with one another.
    Currently htmx offers no mechanisms for synchronizing multiple element
    requests. This document will explore some ideas for doing so.

    The goal is to provide both declarative synchronization as well as
    scripting access to htmx-driven requests.

Carson wrote up this proposal detailing the design of the future
hx-sync attribute. What do you think ?

https://gist.github.com/1cg/785a98742b10cfd67d421d9d8f42f918


HDB improvements
=====================

The _hyperscript debugger now has the ability to jump back and forth between commands. See the demo below:


Dan Jacob implemented the Real World demo app with htmx
==========================================================

For those that didn’t know (like myself), Real World is a standardized
demo app that aims to be a more sophisticated alternative to TodoMVC,
showcasing features that real-world apps often need.

Thanks to Dan Jacob <https://danjacob.net/>, now htmx has an implementation
of it too! The source code is here:

https://github.com/danjac/realworld

And a discussion of the project:

https://danjacob.net/posts/anatomyofdjangohtmxproject/


Ben’s _hyperscript widgets collection
========================================

With encouragement from the community, Ben has built a file uploader,
a modal window and tabs in _hyperscript.

In keeping with general htmx principles, we will endeavor to create
code that is:

- Usable
- Accessible
- Un-Scalable

https://github.com/benpate/hyperscript-widgets


