

.. _react_to_htmx_ref:

===================================================================================================================
2022-06-01 Announce **Django From React to htmx on a real-world SaaS product: we did it, and it's awesome!**
===================================================================================================================

- https://pretalx.evolutio.pt/djangocon-europe-2022/talk/review/99HRMWXMMTYN3T9RMYZFFWHTZ9TBFPKP


TL;DR
======

We took the plunge and replaced the 2-year-of-work React UI of our SaaS
product with simple Django templates and htmx in a couple of months.

We’d like to share our experience with you, with concrete indicators on
various aspects, and convince your CTO !


🔍 You’ve probably heard of htmx, maybe seen talks about it (maybe even
   right here last year). Demos are great, potential seems enormous.
   Maybe you've heard it's great for quick prototyping and tried it.

❓ But what about switching for your real-life project, against everything
   you’ve heard since 2016 (”modern web interfaces need Javascript-driven webapps”,
   "the best SaaS products are made of an SPA", etc.) ?

   Does htmx keep its promises? What impact on your product, your team,
   your business?

✅ That's what we just did at Contexte, by getting rid of the React SPA
   of a SaaS product. It wasn't a easy decision to make (because the UI
   holds some fairly complex interactions and we were told such interactions
   require client-side state management), but it's now such a relief.

   Maybe if we share our experience the decision would be easier to make for others.

💬 Draft outline of the talk:

- What is Contexte? mainly about our Product/Tech team size and organization
- What is our first SaaS product, Contexte Scan (click here for a
  Google-translated product presentation)? a quick demo, with an emphasis
  on the most complex UI elements (I'm not here to sell it to you 😉 )
- Our React UI: bulky, buggy, constantly refactored, dependency hell
  (quick tour, some figures)
- Our htmx UI: a bit of code here, with examples of how you replace
  client-side state management with just some htmx-loaded HTML fragments
- Impact on end-users: same UX, better performances
- Impact on the team: faster, fullstack, more agile
- Impact on the planet: smaller footprint on end-users computers
- Conclusion: go for it!

