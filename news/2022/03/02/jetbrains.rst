

.. _htmx_jetbrains_2022_03_02:

==============================================================================================
2022-03-02 absolutely floored to say that htmx has a new corporate sponsor... @jetbrains!
==============================================================================================

- https://x.com/htmx_org/status/1499071190916239361?s=20&t=jA7R6TNr0G5qYFmFuRkt8w
- https://x.com/JetBrains
- https://x.com/pycharm


absolutely floored to say that htmx has a new corporate sponsor... @jetbrains!

i have been a long time (20+ years) user of all of JetBrain's tools and
it is positively surreal to have them as a sponsor

thank you @jetbrains!

🙏🙏
