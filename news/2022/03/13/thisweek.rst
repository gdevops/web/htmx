

.. _thisweek_12_2022_03_13:

==================================================================================
2022-03-13 Issue N°12: **Copying Down The Dates** by https://denizaksimsek.com/
==================================================================================

- https://thisweek.htmx.org/issue/12/
- :ref:`thisweek_11_2022_02_27`



Our newest corporate sponsor: JetBrains
==========================================

Yes, that JetBrains.


::

    htmx.org @htmx_org
    absolutely floored to say that htmx has a new corporate sponsor… @jetbrains!
    i have been a long time (20+ years) user of all of JetBrain's tools
    and it is positively surreal to have them as a sponsor
    thank you @jetbrains!
    🙏🙏🙏


Looking for a new home for the htmx community
=============================================

::

    Discord is a black hole for information.


::

    alfonsrv Top on HN – Discord is a black hole for information: https://knockout.chat/thread/33251/1
    2022-02-18 15:18Z
    gagepeterson As far as which forum software I think GitHub discussions
    is probably the easiest to implement.
    I think I like Discourse’s notification system, since last visit line,
    and obviously has a lot more plugins and features than GitHub discussions.
    However hosting ourselves is a bit more difficult to manage I admit.
    There’s also nodeBB which has pretty slick UX. Pretty sure I can even
    be embedded into the static website if we wanted. Looks like they’ll
    even host it for free for open source projects: https://nodebb.org/pricing/

    2022-02-24

    gagepeterson Also looks like Twist is free for communities:
    https://x.com/amix3k/status/1496225056300711937?t=P2RFxsdv7qYFj3JTe28SRA&s=19
    gagepeterson It’s got really great web, desktop, and mobile app.
    Not publicly searchable but best in class UX

    2022-03-01

    xpufx If leaving Discord ever becomes an option at all please consider
    Zulip. https://zulip.com/ It’s open source. Can be self hosted.
    But they also do provide free cloud hosting for open source projecst
    if I am not mistaken.
    xpufx Its killer feature is it’s topic based threads.
    (every channel conversation requires a topic)
    xpufx Yes it is in fact free for open source projects.
    https://zulip.com/for/open-source/ Refer to the "Lasting knowledge repository "
    section on this page for info relating to the topic of this thread.

    Discord · February 14, 2022, 11:22 AM UTC

    Craig R Wonder if this might be somewhat suitable? Seems like a
    hybrid forum and chat: https://www.talkyard.io/
    lllama https://www.discourse.org/ ?


Do you have any ideas ?


missing.css progress
=========================

The missing.css website has a new font switcher. İn addition: we are
looking into navbar-free navigation, because navbars are a pain.

Meanwhile, we are progressing on the front of covering all the elements.

One thing still undecided is the utility class part — if you have used
utility-based CSS and have opinions about it, https://htmx.org/discord
is the soapbox you’ve been looking for! Ignore that buzzıng noise. 🐝

    coreload @coreload
    Concepts for missing.css This could turn out to be good. https://github.com/bigskysoftware

    Twitter · February 28, 2022, 12:25 AM UTC
    - https://x.com/coreload/status/1498091863122661377


django-htmx on Read the Docs
===============================

::


    I’ve just released django-htmx 1.9.0. As part of this release, it
    now has a documentation site built with Sphinx, hosted on
    Read the Docs at https://django-htmx.readthedocs.io.
    — Adam Johnson, django-htmx Now on Read the Docs

Check it out!

- https://django-htmx.readthedocs.io

The maintainability of _hyperscript code
========================================

Fubarrr Just wanted to say how much I love the maintainability of hyperscript code!
Today I discovered a small issue with some code I wrote months ago.
Took all of 30 seconds to find the problematic line of code ('cause LoB),
understand what it was doing ('cause hyperscript syntax is poetry) and roll in a fix.

Discord · February 28, 2022, 10:41 PM UTC
- https://discordapp.com/channels/725789699527933952/796428329531605032/947986864093134908






