

.. _htmx_django_alpine_2022_01_27:

==========================================================================
2022-01-27 **Django, HTMX and Alpine** by Dan Jacob  🐍 + </> + 🌲=
==========================================================================

- https://danjacob.net/posts/djangohtmxalpine/
- https://discord.com/channels/725789699527933952/909436816388669530/936251017115361320
- https://github.com/danjac
- https://x.com/htmx_org/status/1486725268270030850?s=20&t=aWZPKaHL9_jT9xuHzKDUkw


Introduction
=============

Over the past year or so I've been closely following an interesting trend
in web development in which a wave of new libraries and frameworks allow
developers to build modern web applications more simply, cheaply and quickly.

The current dominant paradigm in web development is the Single Page
Application or SPA architecture.

Typically this consists of a backend API or APIs connected to a frontend
built in a Javascript framework such as React, Vue or Svelte.

The API communicates with the frontend through JSON payloads, with the
frontend having sole responsiblity for rendering the data in the DOM.

The SPA model downsides
=========================

The SPA model however also comes with a lot of downsides.
Logic such as form validation has to be duplicated between client and
server. You may have to host two separate applications in different
domains, adding complexity to otherwise "solved problems" such as
authentication.

A bug in your Javascript code may render not merely a semi-functional site,
but a blank page with no clue for the developer or user on how to fix.

SEO is more problematic as Javascript-rendered sites are opaque (or at
least suboptimal) to search engine crawlers and social media sharing
services.
A web page might initially load more quickly, but the user is left looking
at numerous spinning gifs and blinking wireframes while half a dozen API
calls load individual parts of the page.

There are solutions to these issues, from SSR to GraphQL to CORS, but they
entail further complexity and more dependencies.

