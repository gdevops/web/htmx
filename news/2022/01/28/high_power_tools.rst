

.. _django_htmx_2022_01_28:

=================================================================================
2022-01-28 **High power tools for HTML (and for Django): HTMX** by Helit Celik
=================================================================================


- https://x.com/halit_elguia
- https://x.com/django_ch
- https://www.meetup.com/fr-FR/Django-Suisse-Romande/events/
- https://www.meetup.com/Django-Suisse-Romande/events/282916299/
- https://slides.com/halitcelik/deck-fbb288#/0/1/1
- https://slides.com/halitcelik


Details
===========

Why do we have to render the entire page for every single request we do
to the backend ?
And why would the only alternative be to delegate everything to a
JavaScript-heavy codebase that interacts with a complex API ?

What if we could still benefit from Django templates & forms while having
dynamic pages?

For the first Django meetup of 2022, Halit is going to show you how the
hybrid approach of the lately famous HTMX* allows you to get the best
of both worlds, helping you to build modern user interfaces with the
simplicity and power of hypertext (without having to write javascript :)).

Note: because of the current Covid situation the event will take place
online (a link to attend the event will be posted soon).

The talk will be in english, but you’ll be able to ask your questions
in french.

