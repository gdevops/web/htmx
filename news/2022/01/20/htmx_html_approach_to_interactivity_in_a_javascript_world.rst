

.. _htmx_2022_01_20_interactivity:

===========================================================================
2022-01-20 Htmx: HTML Approach to Interactivity in a JavaScript World
===========================================================================

- https://thenewstack.io/htmx-html-approach-to-interactivity-in-a-javascript-world/
- https://thenewstack.io/author/mike-melanson/


What Is htmx and Why Is It Useful ?
=====================================

Complexity in frontend web development is something that Gross has been
attempting to address for nearly a decade now, having first created the
intercooler.js alternative frontend library back in 2013, which came with
the tagline “AJAX With Attributes: There is no need to be complex”.

Recently, intercooler.js hit version 2.0 and became htmx, which the GithHub
description says “allows you to access AJAX, CSS Transitions, WebSockets
and Server Sent Events directly in HTML, using attributes, so you can
build modern user interfaces with the simplicity and power of hypertext”.


Make HTML More Powerful
===========================

“I think a lot of the JavaScript frameworks now, you’re building almost
a thick client in the browser,” said Gross in an interview with The New Stack.

“You’re just using the browser as the virtual machine to do that. When
you’re in that world, you can do a lot more, obviously, but there’s a
lot more complexity.”

Gross sees htmx as an answer to this complexity for many websites out
there — even sites like Facebook or Twitter, which are “mostly text and
pictures”.

“The concept is, let’s use the original model of the web for building
web apps, but let’s make HTML more powerful,” continued Gross.

“If we make HTML more powerful, maybe we don’t have to put up with all
the complexity that comes along with these JavaScript frontend frameworks,
because when you’re writing your user interface in JavaScript, you’re
writing a lot of code.

There’s a lot of complexity to manage that.

Htmx is really kind of a move back and an attempt to say, ‘
Okay, instead of going down this JavaScript route, building big applications
in the browser in JavaScript, let’s make HTML more powerful and see how
much interactivity we can get by just trying to make HTML more powerful
as a hypertext, as a hypermedia, as something that exchanges HTML.'”

