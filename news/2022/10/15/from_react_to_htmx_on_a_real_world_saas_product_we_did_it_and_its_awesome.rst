.. index::
   pair: htmx; react

.. _react_to_htmx:

====================================================================================================================================
2022-10-15 DjangoCon 2022 **From React to htmx on a real-world SaaS product: we did it, and it's awesome !** by David Guillot
====================================================================================================================================

- :ref:`react_to_htmx_ref`
- https://pretalx.evolutio.pt/djangocon-europe-2022/talk/review/99HRMWXMMTYN3T9RMYZFFWHTZ9TBFPKP
- https://www.youtube.com/watch?v=3GObi93tjZI
- :ref:`david_guillot`

Description
=============


From React to htmx on a real-world SaaS product: we did it, and it's
awesome! by David Guillot

We took the plunge and replaced the 2-year-of-work React UI of our SaaS
product with simple Django templates and htmx in a couple of months.

We’d like to share our experience with you, with concrete indicators on
various aspects, and convince your CTO!


Slides
--------

- https://docs.google.com/presentation/d/1jW7vTiHFzA71m2EoCywjNXch-RPQJuAkTiLpleYFQjI/edit#slide=id.g35f391192_00


Articles
===========

- https://htmx.org/essays/a-real-world-react-to-htmx-port/


Django components (Create simple reusable template components in Django)
==========================================================================

- https://github.com/EmilStenstrom/django-components/
- https://github.com/EmilStenstrom/django-components/graphs/contributors
- https://x.com/EmilStenstrom

.. figure:: images/django_components.png
   :align: center

   https://x.com/EmilStenstrom/status/1581399633393487872?s=20&t=7oa1u2p4YFDUO__wLNaNCQ



Twitter reactions
====================

.. figure:: images/you_dit_it_friends.png
   :align: center

   https://x.com/htmx_org/status/1581401541042655233?s=20&t=RPCk5ZkwiJXDJcc8f1SDzQ



Hynek Schlawack
------------------

- https://x.com/hynek/status/1581551126012252161?s=20&t=OhLR5LksARyx7yJD0M83gg

@hynek 26 min En réponse à  @kvalekseev
That said & shitposting aside: nobody is saying React should literally
go away. What htmx does tho is a re-democratization of web dev & the
allergic reactions by SPA incumbents (NOT meaning you) are VERY telling.

Allowing to build rich web UIs in langs you like is a net positive.

htmx
--------

.. figure:: images/python_full_stack.png
   :align: center

   https://x.com/htmx_org/status/1581426779662602241?s=20&t=RPCk5ZkwiJXDJcc8f1SDzQ


