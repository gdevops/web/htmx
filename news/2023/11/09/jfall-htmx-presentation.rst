.. index::
   pair: HTMX: Web 1.0 with the benefits of Web 2.0 without the grift of Web 3.0-at JFall 2023; Martijn Dashorst

.. _dashorst_2024_11_09:

=============================================================================================================================
2023-11-09 **HTMX: Web 1.0 with the benefits of Web 2.0 without the grift of Web 3.0-at JFall 2023** by Martijn Dashorst
=============================================================================================================================

- https://martijndashorst.com/blog/2023/11/09/jfall-htmx-presentation

Introduction
===============

JFall 2023 is a Dutch Java User Group 3-day conference packed into 1 day,
and this year celebrated its 20th anniversary!

I was honored to be able to present at the conference and my presentation
“HTMX: Web 1.0 with the benefits of Web 2.0 without the grift of Web 3.0”
was selected.

In this post I’m giving you the cliff notes of the presentation and the slides,
as we eagerly await the upload of the recorded video.


The slides
============

- https://fr.slideshare.net/dashorst/htmx-web-10-with-the-benefits-of-web-20-without-the-grift-of-web-30

- HTMX is a JavaScript library that allows any HTML element to interact as a
  hypermedia component by adding attributes that instruct HTMX on what requests
  to make and how to update the DOM.
- Attributes like hx-get, hx-post, hx-target, and hx-swap allow elements
  to make requests and update other elements without JavaScript.
  Inherited attributes remove repetition.
- HTMX requests can be detected on the server via request headers, and response
  headers can modify requests by changing targets or swapping mechanisms.
  Classes provide feedback during requests.


Conclusions
===================

With HTMX **a backend developer can now make HTML pages more dynamic without
having to switch to a full client-side application build**.

You just render out HTML and progressively enhance the application with HTMX
tags and such to make a rich application.

I heartily recommend checking out HTMX and give it a 9/10.
