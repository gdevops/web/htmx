

.. _htmx_is_the_future_2023_05_05:

=======================================
2023-05-05 **HTMX is the Future**
=======================================

- https://quii.dev/HTMX_is_the_Future


The current state of web application development
------------------------------------------------------

User expectations of the web are now that you have this super-smooth
no-reload experience.

Unfortunately, it's an expectation that is usually delivered with single-page
applications (SPAs) that rely on libraries and frameworks like React and
Angular, which are very specialised tools that can be complicated to work with.

A new approach is to put the ability to deliver this UX back into the
hands of engineers that built websites before the SPA-craze, leveraging
their existing toolsets and knowledge, and HTMX is the best example I've
used so far.

