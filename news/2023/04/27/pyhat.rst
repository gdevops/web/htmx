.. index::
   pair: Awsome;  PyHAT (Awesome Python htmx)
   ! PyHAT

.. _pyhat_2023_04_27:

=============================================
2023-04-27 **PyHAT: Awesome Python htmx**
=============================================

- https://github.com/PyHAT-stack/awesome-python-htmx
- https://htmx.org/essays/a-real-world-react-to-htmx-port/

What is PyHAT
================

PyHAT is more than just a snake.

It stands for Python htmx ASGI Tailwind—a web stack that allows you to
build powerful web applications using nothing more than... drumroll...
Python, htmx, and Tailwind.

