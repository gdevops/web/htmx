.. index::
   pair: Building a streaming ChatGPT clone, with Django, Channels, and HTMX ; Corry Zue (2024-04-05)

.. _zue_2024_04_05:

=====================================================================================================
2024-04-05 **Building a streaming ChatGPT clone, with Django, Channels, and HTMX** by Corry Zue
=====================================================================================================

.. youtube:: 8JSiiPW4S0A


Description
================

In this video I walkthrough the process of building a ChatGPT clone step-by-step
using Django, websockets, and HTMX.

I break down each piece of functionality into a single commit, and then explain
and demo that commit.

At the end we have a fully functional, streaming chatbot app!

Links:
=========

- Channels Tutorial: https://channels.readthedocs.io/en/stable/tutorial/index.html
- HTMX Websockets: https://htmx.org/extensions/web-sockets/
- SaaS Pegasus: https://www.saaspegasus.com/
- Patch file with changes: https://gist.github.com/czue/fc37f732f5c70cd16f38819d399b129f

Timestamps
==================

- 0:00 - Demo
- 1:02 - UI Scaffolding
- 2:13 - Websocket setup with backend consumer and HTMX extension
- 4:15 - Showing the user's message with Websockets and HTMX
- 5:32 - Clearing the input bar with the htmx:wsAfterSend events
- 6:13 - Showing the system message template and loading indicator
- 8:00 - Adding the ChatGPT API and streaming the response to the front end
- 9:37 - Removing the loading indicator
- 10:40 - Adding message history support
- 12:20 - Adding automatic scrolling with htmx:wsAfterMessage event
