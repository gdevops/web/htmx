.. index::
   ! HTMX in production


.. _ramport_2024_04_08:

============================================================================================
2024-04-08 **HTMX in production** by Hamilton Greene
============================================================================================

- https://hamy.xyz/labs/2024-04_htmx-in-production

.. youtube:: Ec_ovkHHuZ8


Introduction
===============

HTMX is changing how modern web apps get built.

It provides most of the capabilities of a Single-Page Application with the
simplicity of a Multi-Page Application.
This means you don't really need reach for React, Vue, Angular, Svelte, etc
to get a modern app - and in most cases it's simpler not to.

But all technologies make big promises and big splashes early in their hype
cycle while few survive the test of running in production.

In this post we're going to survey the experiences of real developers running
HTMX in production to see if it works well or is just hype.
