.. index::
   pair: htmx ; htmx 2.0.0 has been released ! (2024-06-17)

.. _htmx_2_2024_06_17:

=================================================
2024-06-17 **htmx 2.0.0 has been released !**
=================================================

- https://htmx.org/posts/2024-06-17-htmx-2-0-0-is-released/


htmx 2.0.0 Release
====================

I’m very happy to announce the release of htmx 2.0. 

This release ends support for Internet Explorer and tightens up some defaults, 
but does not change most of the core functionality or the core API of the library.

Note that we are not marking 2.0 as latest in NPM because we do not want 
to force-upgrade users who are relying on non-versioned CDN URLs for htmx. 

Instead, 1.x will remain latest and the 2.0 line will remain next until Jan 1, 2025. 

The website, however, will reference 2.0.
