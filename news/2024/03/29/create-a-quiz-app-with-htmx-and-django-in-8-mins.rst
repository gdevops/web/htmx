.. index::
   ! Create a quiz app with HTMX and Django in 6 mins


.. _designer_2024_03_29:

=======================================================================================
2024-03-29 **Create a quiz app with HTMX and Django in 6 mins** by Photon designer
=======================================================================================

- https://www.photondesigner.com/articles/quiz-htmx

.. youtube:: 6xLt1D5boJo

We'll build a simple quiz application using Django and HTMX in 6 minutes.

This includes:

- a multi-stage form using HTMX and Django.
- adding data into your Django database from YAML.
- generating data for your quiz app using an LLM

