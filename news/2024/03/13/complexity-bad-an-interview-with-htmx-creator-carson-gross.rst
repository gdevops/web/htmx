.. index::
   pair: Carson Gross; Complexity bad: An interview with HTMX creator Carson Gross (2024-03-13)

.. _gross_2024_03_13:

==============================================================================
2024-03-13 **Complexity bad: An interview with HTMX creator Carson Gross**
==============================================================================

- https://www.infoworld.com/article/3713164/complexity-bad-an-interview-with-carson-gross.html#tk.rss_all
- https://grugbrain.dev/


How one computer science professor in Montana aims to supplant reactivity,
compete with JavaScript, restore true REST, and make the web more developer-friendly.

Carson Gross is the creator of HTMX and Hyperscript, the mind behind The
Grug Brained Developer, a professor of software engineering at Montana State
University, and co-author of Hypermedia Systems.

It was a pleasure to pick Carson's brain about the impetus behind projects like
HTMX and Hyperscript, the failures of REST, why JavaScript is here to stay,
and much more.

Tyson: It’s hard to pick where to start here so I’m going to go with Grug
Brained Developer. I kind of feel like I could just refer everyone there for
all programming-related questions.  [ Keep up with the latest developments in
software development. Subscribe to the InfoWorld First Look newsletter ]

Gross: Ha, I appreciate that. I think it’s worth reading for most
developers. I kind of geared it towards younger devs, though. It certainly
isn’t perfect—hard to be when you are using a caveman voice and trying to
be funny—but I think most of the advice is solid.

Tyson: It’s true that most of the coding trouble I’ve ever gotten myself
into was mainly a result of thinking I was smart.

Gross: Yeah, I think everyone who has programmed for a while has experienced
that point at which you realize you’ve gotten in too deep and can’t keep the
system in your head anymore. I don’t know if it’s always thinking you are too
smart, or just not recognizing when things start to get too complicated. Also,
social dynamics in tech jobs punish people who say things like, “Uh, this
is too complicated to me.”

Tyson: That is so true about the atmosphere around admitting it's too
complicated. One time I chatted to a friend “JavaScript is so flexible,
you can code your way out of anything,” and he chatted back, “Haha, that's
dangerous talk.”  He was right.

Do you have any thoughts about JavaScript as a language?

Gross: I think JavaScript is an okay scripting language. It isn’t a great
general-purpose language in my opinion. In a somewhat bizarre way, it feels
like C++ to me: it has too many features and ways to do things now. On the
other hand, JavaScript has one unbelievable feature: it’s there, in the
browser. And that will make it the premier scripting language for the web for
the foreseeable future, in my opinion.

Tyson: I’m pretty impressed by Hyperscript, but I get the feeling it’s kind
of a fun sideline for you. Would you tell us about the technical experience
of implementing the parser and so on?

Gross: Sure. Hyperscript is a scripting language that “competes” with
JavaScript as an option for front-end scripting. It’s based on HyperTalk,
the scripting language for HyperCard, and has features that I think make for
a better authoring experience when you are simply trying to script on a web
page. For example, the Hyperscript runtime automatically resolves Promises it
encounters, so script writers don’t need to deal with them. It has a natural,
English-like syntax, which some people love and a lot of people hate. It is
definitely a passion project of mine, but I’m hoping to get it to 1.0 this
year, after HTMX 2 ships.

As far as its implementation, yep, it’s implemented in JavaScript as a lexer,
parser, and eval-based runtime. I was surprised that the JavaScript runtime
is fast enough to let me get away with that, but it is. I wouldn’t try to
implement a Bitcoin miner in Hyperscript, but it’s fast enough for light
DOM scripting. The Hyperscript parser and runtime is right at the edge of the
complexity I’d be willing to take on with JavaScript.

Tyson: HTMX is another impressive bit of coding. My introduction to HTMX was
one of the most-read articles on InfoWorld last year, which shows there's a
lot of interest in it. This project seems more serious, too, in that it looks
to restore at least the possibility of a true REST architecture.

Gross: Yeah, it’s been a wild year for HTMX. For readers that haven’t heard
of it, HTMX is a JavaScript library I wrote that lets you add attributes,
very similar in spirit to the href and action attributes of links and forms
in standard HTML. Using those attributes, you can trigger AJAX requests and
then replace parts of the DOM with the HTML that the server responds with.

Tyson: I'd say HTMX extends the vocabulary of good ol’ HTML to cover some
modern use cases, in particular, AJAX. Is that a fair description?

Gross: Yep, that’s a good way to explain it. I sometimes say that HTMX
“completes HTML as a hypermedia” in that it allows any element on the page
to issue an HTTP request in response to an event, and then place that response
anywhere in the DOM.

That probably sounds pretty simple, and it is, but with just that little
generalization of HTML you unlock a lot of UX patterns that have traditionally
been reserved to JavaScript. Examples include infinite scrolling, lazy loading
sections of a page, and as-you-type server-side validation.

Tyson: I have to say, I thought I pretty much knew what REST meant but I was
wrong for a long time. You address how that came to be in your article, How
did REST come to mean the opposite of REST?

Would you mind distilling for readers the difference between REST-as-intended
versus REST as it is commonly implemented?

Gross: *laughs* Well, we aren’t changing how the industry uses the term
REST. Today that term means “JSON APIs over HTTP” and that’s what it’s
going to mean likely for the rest of our careers.

However, that isn’t what REST originally meant. Instead, REST was a description
of the way the World Wide Web worked (it’s network architecture) before JSON
APIs existed; just links and forms driving interactions via a browser. The
term REST came from a famous dissertation by Roy Fielding, and he defined a
series of constraints that a RESTful system needs to conform to.

Today, most JSON APIs that are called “RESTful” don’t satisfy those
constraints. Even more hilariously, a simple static website with a couple of
HTML pages that link to one another does satisfy all those constraints. So,
someone who creates a simple static website is, at some level, a better
“REST” engineer than people with fancy “RESTful” JSON API engineer
titles on their resumes.

The language isn’t going to change, but I think it’s worth understanding
the original meaning of the term and, in particular, what is called the uniform
interface constraint of REST to really understand why the web is so flexible.

Tyson: I haven’t been in a programming class since the beginning of the
millennium. Do you still use books?  Just kidding. (I think.)

What's it like being a teacher of programming?  Can I take CSCI 468 from you?

Gross: Ha!  Well, if you want to take 468, which is the compilers class I
teach, you can just read Crafting Interpreters by Bob Noystrom and get about
50% of it. We target the JVM with bytecode on the back end and, to be honest,
that isn’t that interesting unless you are a big Java guy.

I like teaching. There are definitely downsides: the pay sucks and there is
bureaucracy to deal with. But there are lots of upsides and I enjoy getting
students over the hump to where they feel like they can program on their own.

Tyson: Yep, I am definitely a Java guy.

So, this is returning a bit to the REST topic, but from a higher perspective. You
are co-author of Hypermedia Systems, which makes the case that the way the
web is designed provides an application architecture that we can build upon,
if we look at it right.

Gross: Definitely, and HTMX tries to build on that network architecture by
enhancing HTML as hypermedia, rather than replacing that original architecture
with a new one, namely fixed-format JSON Data APIs. In the book, I try to
outline the systemic nature of properly functioning hypermedia. For example,
for a long time, I didn’t appreciate how important the hypermedia client
(e.g., browsers) was for the uniform interface to work properly. You really
need to have a whole system—hypermedia like HTML, a network protocol like
HTTP, hypermedia servers, and hypermedia clients—to make the whole thing
work as intended.

Tyson: In closing, I’d like to summarize everything important in a few short,
amusing remarks, but you’ve already done that. I try to find things I disagree
with in grug’s philosophy, but I can’t really. Expression complexity,
closures, saying no ... grug has good advice for all of these topics.

But I'm curious about grug's feelings about the visitor pattern. I’ve used
it and thought it went well. In the end, the project didn’t ship, but the
code was solid. Tell me what’s wrong with the visitor pattern.

Gross: Yeah, I’m not 100% against the visitor pattern, despite what the
grugbrain essay says. (In general, I’m not 100% for or against anything
in software development.)  However, I often think it’s better to encode
operations directly in a tree rather than have another side thingie that
performs operations over the tree. Sometimes this mixes concerns a bit, but
I don’t mind that in this case.

As an example, in Hyperscript, the parse tree elements have the eval methods
defined on them and are defined directly in the spot they are parsed. This
mixes concerns, but I think it adds clarity to the program, because when you
want to understand how, for example, the “wait” command works, you can go to
one place and see how it’s both parsed and evaluated. I find that very useful.

Tyson: Why is it that we know complexity is bad for us but we are drawn to
it anyway?

Gross: Well, again, I think there are industry pressures at play here. Software
is a brutal industry and appearing to be unintelligent can harm your career
badly. This means that we all feel pressure to demonstrate we are clever,
and also feel pressure to not admit that someone else’s code is confusing
or seems too complicated to us. In many circles, “sophisticated” is praise
and “dumb” is criticism.

I understand that dynamic and I’m sympathetic to engineers who feel those
pressures. This is one reason why I suggest senior engineers who have a secure
position in an engineering organization should often loudly proclaim that
things seem too complicated to them if, in fact, they do. This makes it okay
for younger engineers to voice their concerns, as well, and relieves those
pressures organizationally, to an extent at least.
