.. index::
   pair: htmx; 1.6.0 (2021-10-02)

.. _htmx_1_6_0:

==================================================================================
htmx 1.6.0 (2021-10-02)
==================================================================================

- https://htmx.org/posts/2021-10-02-htmx-1.6.0-is-released/
- https://github.com/bigskysoftware/htmx/tree/v1.6.0
- https://unpkg.com/browse/htmx.org@1.6.0/dist/htmx.js

htmx 1.6.0 Release
=======================

I'm happy to announce the 1.6.0 release of htmx.
New Features & Major Changes

- Completely reworked <script> tag support that now supports the <script src="...'/> form
- You can now use the value unset to clear a property that would normally
  be inherited (e.g. hx-confirm)
- The htmx-added class is added to new content before a swap and removed
  after the settle phase, which allows you more flexibility in writing
  CSS transitions for added content (rather than relying on the target,
  as with htmx-settling)
- The htmx:beforeSwap event has been updated to allow you to configure
  swapping behavior

Improvements & Bug fixes
===========================

- Improved <title> extraction support
- You can listen to events on the window object using the from: modifier in hx-trigger
- The root option of the intersect event was fixed
- Boosted forms respect the enctype declaration
- The HX-Boosted header will be sent on requests from boosted elements
- Promises are not returned from the main ajax function unless it is an
  api call (i.e. htmx.ajax)

Enjoy!


Some questions about 1.6.0
===========================

- https://discord.com/channels/725789699527933952/725789747212976259/893890768090046464

1. What does “much better script support” mean?
2. Where is the documentation for “unset”? I can’t seem to find it?
3. Is there perhaps an example on how to use the htmx-added class in a transition?

Responses:

`1 <https://discord.com/channels/725789699527933952/725789747212976259/893892287682183258>`_.
   If you include a <script src="/foo.js"/> tag, it will now load (previously
   we only evaluated inline scripts)

2. **Er, I'll write some**.
   Basically you can use the value unset to "cancel" an attribute that
   would normally be inherited::

        <div hx-confirm="Blah blah" ...>
           <a hx-confirm="unset" ...>No Confirm</a>
        </div>


`3 <https://discord.com/channels/725789699527933952/725789747212976259/893893380843667517>`_.
   Yes, I updated the settling animation to use .htmx-added: https://htmx.org/examples/animations/#settling
   It's nearly identical to .htmx-settling but is added on the newly
   added content, rather than the target::

       <a hx-get="/example' hx-target="#div1">...</a>
       <!-- this div would get the .htmx-settling class -->
       <div id="div1">
            <!-- these ps would get the .htmx-added class -->
            <p> Some Content...</p>
            <p> Some More Content...</p>
       </div>

   NB: in the case of hx-swap='outerHTML' the target and the added content are the same
   I really need an illustrator for the htmx docs
