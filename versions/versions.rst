.. index::
   paie: htmx; Versions

.. _htmx_versions:

==================================================================================
HTMX versions
==================================================================================

- https://github.com/bigskysoftware/htmx/graphs/contributors
- https://github.com/bigskysoftware/htmx/blob/master/CHANGELOG.md
- https://github.com/bigskysoftware/htmx/tags
- https://github.com/bigskysoftware/htmx/commits/master
- https://htmx.org/talk/#announcements


.. toctree::
   :maxdepth: 3

   2.0.0/2.0.0
   1.9.0/1.9.0
   1.7.0/1.7.0
   1.6.0/1.6.0
   1.5.0/1.5.0
   1.4.1/1.4.1
   1.4.0/1.4.0
   0.4.0/0.4.0
