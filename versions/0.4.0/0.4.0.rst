.. index::
   pair: htmx; 0.4.0 (2020-11-16)

.. _htmx_0_4_0:

==================================================================================
htmx 0.4.0 (2020-11-16)
==================================================================================

- https://github.com/bigskysoftware/htmx/tree/v0.4.0


[0.4.0] - 2020-11-16
=======================

* Now support the `HX-Redirect` and `HX-Refresh` response headers for
  redirecting client side and triggering a page refresh, respectively
* `hx-vars` now overrides input values
* `<title>` tags in responses will be used to update page titles
* All uses of `eval()` have been removed in favor of `Function`
* `hx-vals <https://htmx.org/attributes/hx-vals>`_ is available as a
  safe alternative to `hx-vars`.

  It uses `JSON.parse()` rather than evaluation, if you wish to safely
  pass user-provided values through to htmx.

