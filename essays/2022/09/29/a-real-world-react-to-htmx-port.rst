.. index::
   pair: htmx; port
   pair: From React to htmx on a real-world SaaS product: we did it, and it’s awesome!; React

.. _htmx_port_2022_09_29:

=====================================================================================================================================
2022-09-29 **A Real World React -> htmx Port** (From React to htmx on a real-world SaaS product: we did it, and it’s awesome!)
=====================================================================================================================================

- https://htmx.org/essays/a-real-world-react-to-htmx-port/


Introduction
===================

It is all well and good talking about REST & HATEOAS in theory or describing
the Hypermedia-Driven Application architecture, but, at the end of the day,
what matters in software is practical: Does it work? Does it improve things?

We can say for sure that htmx works, since we use it in our own software.

But it is hard to say that it would be an improvement over other approaches,
since we haven’t had an apples-to-apples comparison of how htmx might
compare with, say, react.

Until now.

David Guillot at Contexte has given what we are calling `“The Mother of All htmx Demos”
at DjangoCon 2022 <https://www.youtube.com/watch?v=3GObi93tjZI>`_:

::

    From React to htmx on a real-world SaaS product: we did it, and it’s awesome!
    We took the plunge and replaced the 2-year-of-work React UI of our
    SaaS product with simple Django templates and htmx in a couple of
    months.

    We’d like to share our experience with you, with concrete indicators
    on various aspects, and convince your CTO!

.. youtube:: 3GObi93tjZI


Executive Summary
========================

- The effort took about 2 months (with a 21K LOC code base, mostly JavaScript)
- No reduction in the application’s user experience (UX)
- They reduced the code base size by 67% (21,500 LOC to 7200 LOC)
- They increased python code by 140% (500 LOC to 1200 LOC), a good thing if you prefer python to JS
- They reduced their total JS dependencies by 96% (255 to 9)
- They reduced their web build time by 88% (40 seconds to 5)
- First load time-to-interactive was reduced by 50-60% (from 2 to 6 seconds to 1 to 2 seconds)
- Much larger data sets were possible when using htmx, because react simply couldn’t handle the data
- Web application memory usage was reduced by 46% (75MB to 45MB)


Slides
===============

The slides for the presentation can be found here (be sure to check the excellent speakers notes!)

- https://docs.google.com/presentation/d/1jW7vTiHFzA71m2EoCywjNXch-RPQJuAkTiLpleYFQjI/edit?usp=sharing
