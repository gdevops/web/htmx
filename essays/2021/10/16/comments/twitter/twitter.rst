
.. _htmx_hateoas_x.comments:

==================================================================================
**twitter** comments
==================================================================================


a comment from reddit showing again just how dramatically our industry has failed at passing on historical knowledge
=====================================================================================================================

- https://x.com/htmx_org/status/1475832875442397191?s=20

many, perhaps a majority, of web developers are unaware that REST was a
description of the original web architecture

JSON != REST

a great opportunity for us!


This is a great resource to alleviate the confusion
=======================================================

- https://x.com/rednafi/status/1475852573932355590?s=20

This is a great resource to alleviate the confusion:  https://twobithistory.org/2020/06/28/rest.html
