.. _sphx_tag_hypermedia-apis:

My tags: Hypermedia APIs
########################

.. toctree::
    :maxdepth: 1
    :caption: With this tag

    ../essays/2021/07/17/hypermedia-apis-vs-data-apis.rst
