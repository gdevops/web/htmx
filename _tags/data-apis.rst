.. _sphx_tag_data-apis:

My tags: Data APIs
##################

.. toctree::
    :maxdepth: 1
    :caption: With this tag

    ../essays/2021/07/17/hypermedia-apis-vs-data-apis.rst
