.. _sphx_tag_ssr-server-side-rendering:

My tags: SSR (Server-Side Rendering)
####################################

.. toctree::
    :maxdepth: 1
    :caption: With this tag

    ../essays/2023/06/14/10-tips-for-ssr-hda-apps.rst
