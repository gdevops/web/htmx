.. _sphx_tag_hda-hypermedia-driven-application:

My tags: HDA (Hypermedia-Driven Application)
############################################

.. toctree::
    :maxdepth: 1
    :caption: With this tag

    ../essays/2023/06/14/10-tips-for-ssr-hda-apps.rst
