.. _sphx_tag_affordance:

My tags: #affordance
####################

.. toctree::
    :maxdepth: 1
    :caption: With this tag

    ../essays/2023/09/21/the-viewsource-affordance.rst
