:orphan:

.. _tagoverview:

Tags overview
#############

.. toctree::
    :caption: Tags
    :maxdepth: 1

    #affordance (1) <affordance.rst>
    #enshittification (1) <enshittification.rst>
    Data APIs (1) <data-apis.rst>
    HDA (Hypermedia-Driven Application) (1) <hda-hypermedia-driven-application.rst>
    Hypermedia APIs (1) <hypermedia-apis.rst>
    SSR (Server-Side Rendering) (1) <ssr-server-side-rendering.rst>
    hypertext (1) <hypertext.rst>
    mindset shift (1) <mindset-shift.rst>
    react (1) <react.rst>
