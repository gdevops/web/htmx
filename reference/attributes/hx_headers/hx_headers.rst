.. index::
   pair: HTMX; hx_headers
   ! hx_headers

.. _hx_headers:

=======================================================================================================================================================
HTMX **hx_headers** attribute (allows you to add to the headers that will be submitted with an AJAX request)
=======================================================================================================================================================

- https://htmx.org/attributes/hx-headers/
- https://docs.djangoproject.com/en/dev/ref/csrf/#how-it-works
- https://owasp.org/www-community/attacks/xss/

Description
=============

The **hx-headers** attribute allows you to add to the headers that will
be submitted with an AJAX request.

By default, the value of this attribute is a list of name-expression values
in `JSON (JavaScript Object Notation) <https://developer.mozilla.org/en-US/docs/Glossary/JSON>`_ format.

If you wish for hx-headers to evaluate the values given, you can prefix
the values with javascript: or js::


.. code-block:: django

    <div hx-get="/example" hx-headers='{"myHeader": "My Value"}'>Get Some HTML, Including A Custom Header in the Request</div>

Security Considerations
==========================

- https://docs.djangoproject.com/en/dev/ref/csrf/#how-it-works

By default, the value of **hx-headers** must be valid `JSON <https://developer.mozilla.org/en-US/docs/Glossary/JSON>`_.

It is not dynamically computed. If you use the javascript: prefix, be
aware that you are introducing security considerations, especially when
dealing with user input such as query strings or user-generated content,
which could introduce a `Cross-Site Scripting (XSS) vulnerability <https://owasp.org/www-community/attacks/xss/>`_.

Notes
=======

- **hx-headers** is inherited and can be placed on a parent element.
- A child declaration of a header overrides a parent declaration.
