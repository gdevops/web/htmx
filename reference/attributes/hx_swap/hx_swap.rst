.. index::
   pair: HTMX; hx-swap
   ! hx-swap

.. _hx_swap:

==================================================================================================================================
HTMX **hx-swap** attribute (allows you to specify how the response will be swapped in relative to the target of an AJAX request)
==================================================================================================================================

- https://htmx.org/attributes/hx-swap/
- https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentHTML


Description
=============

The hx-swap attribute allows you to specify how the response will be
swapped in relative to the target of an AJAX request.

The possible values of this attribute are:

- innerHTML - The default, replace the inner html of the target element
- **outerHTML** - Replace the entire target element with the response
- beforebegin - Insert the response before the target element
- afterbegin - Insert the response before the first child of the target element
- beforeend - Insert the response after the last child of the target element
- **afterend** - Insert the response after the target element
- none- Does not append content from response (out of band items will still be processed).


These options are based on standard DOM naming and the `Element.insertAdjacentHTML <https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentHTML>`_
specification.

So in this code

.. code-block:: django

  <div hx-get="/example" hx-swap="afterend">Get Some HTML & Append It</div>

The div will issue a request to /example and append the returned content after the div

Notes
======

- **hx-swap** is inherited and can be placed on a parent element
- The default value of this attribute is innerHTML
- The default swap delay is 0ms
- The default settle delay is 100ms


Examples
========

- :ref:`devops_click_to_edit`
