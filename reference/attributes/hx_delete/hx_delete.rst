.. index::
   pair: HTMX; hx-delete
   ! hx-delete

.. _hx_delete:

====================================================================================================================================================
HTMX **hx-delete** attribute (will cause an element to issue a **DELETE** to the specified URL and swap the HTML into the DOM using a swap strategy)
====================================================================================================================================================

- https://htmx.org/attributes/hx-delete/
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/DELETE

Description
=============


.. code-block:: django

    <button hx-delete="/account" hx-target="body">
      Delete Your Account
    </button>



This example will cause the button to issue a DELETE to /account and
swap the returned HTML into the innerHTML of the body.


Notes
======

- **hx-delete** is not inherited
- You can control the target of the swap using the :ref:`hx-target <hx_target>` attribute
- You can control the swap strategy by using the :ref:`hx-swap <hx_swap>` attribute
- You can control what event triggers the request with the :ref:`hx-trigger <hx_trigger>` attribute
- You can control the data submitted with the request in various ways, documented here: :ref:`Parameters <htmx_parameters>`
