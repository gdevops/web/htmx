.. index::
   pair: HTMX; HX-Redirect
   ! HX-Redirect

.. _hx_redirect:

==================================================================================================================================================================
HTMX **HX-Redirect** can be used to do a client-side redirect to a new location
==================================================================================================================================================================

- https://htmx.org/reference/#attributes

Description
=============

.. versionadded:: 0.4.0
   :ref:`htmx_0_4_0`

Can be used to do a client-side redirect to a new location.


Examples
==========

open the pdf in a new tab?  yes exactly that
----------------------------------------------

- https://discord.com/channels/725789699527933952/864934037381971988/900016929861935105

    open the pdf in a new tab?  yes exactly that. I will try to come up with an example since the whole code is way too big

    1cg — Hier à 15:45
    OK, what you probably want to do is respond with a HX-Redirect
    response header to the location of the generated PDF.
    That should open the PDF in a new tab.


htmx.js source code
=====================

.. code-block:: javascript
   :linenos:
   :emphasize-lines: 15-17

        function handleAjaxResponse(elt, responseInfo) {
            var xhr = responseInfo.xhr;
            var target = responseInfo.target;

            if (!triggerEvent(elt, 'htmx:beforeOnLoad', responseInfo)) return;

            if (hasHeader(xhr, /HX-Trigger:/i)) {
                handleTrigger(xhr, "HX-Trigger", elt);
            }

            if (hasHeader(xhr,/HX-Push:/i)) {
                var pushedUrl = xhr.getResponseHeader("HX-Push");
            }

            if (hasHeader(xhr, /HX-Redirect:/i)) {
                window.location.href = xhr.getResponseHeader("HX-Redirect");
                return;
            }

            if (hasHeader(xhr,/HX-Refresh:/i)) {
                if ("true" === xhr.getResponseHeader("HX-Refresh")) {
                    location.reload();
                    return;
                }
            }
