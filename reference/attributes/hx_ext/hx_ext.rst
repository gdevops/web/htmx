.. index::
   pair: HTMX; hx-ext
   ! hx-ext

.. _hx_ext:

=======================================================================================================================================================
HTMX **hx-ext** attribute (enables an htmx extension for an element and all its children)
=======================================================================================================================================================

- https://htmx.org/attributes/hx-ext/
- :ref:`htmx_extensions`

Description
=============

The **hx-ext** attribute enables an :ref:`htmx extension <htmx_extensions>` for an element and
all its children.

The value can be a single extension name or a comma separated list of
extensions to apply.

The **hx-ext** tag may be placed on parent elements if you want a plugin
to apply to an entire swath of the DOM, and on the body tag for it to
apply to all htmx requests.

Notes
======

- hx-ext is both inherited and merged with parent elements, so you can
  specify extensions on any element in the DOM hierarchy and it will
  apply to all child elements.
- You can ignore an extension that is defined by a parent node using
  hx-ext="ignore:extensionName"


Use examples
===============

Djangonauts don't let djangonauts write Javascript
-----------------------------------------------------

- :ref:`hernantz_htmx_preload`

::

    <div hx-ext="preload">
      <a href="/my-next-page" preload="mouseover" preload-images="true">Next Page</a>
    </div>
