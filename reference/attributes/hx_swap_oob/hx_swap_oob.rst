.. index::
   pair: HTMX; hx-swap-oob
   ! hx-swap-oob

.. _hx_swap_oob:

==================================================================================================================================================================
HTMX **hx-swap-oob** allows you to specify that some content in a response should be swapped into the DOM somewhere other than the target, that is "Out of Band"
==================================================================================================================================================================

- https://htmx.org/attributes/hx-swap-oob/

Description
=============

The **hx-swap-oob** attribute allows you to specify that some content
in a response should be swapped into the DOM somewhere other than the
target, that is "Out of Band".

This allows you to piggy back updates to other element updates on a response.

Consider the following response HTML:

.. code-block:: html

    <div>
     ...
    </div>
    <div id="alerts" hx-swap-oob="true">
        Saved!
    </div>

The first div will be swapped into the target the usual manner.

The second div, however, will be swapped in as a replacement for the
element with the id alerts, and will not end up in the target.

The value of the hx-swap-oob can be:

- true
- any valid hx-swap value
- any valid hx-swap value, followed by a colon, followed by a CSS selector

If the value is true or outerHTML (which are equivalent) the element will
be swapped inline.

If a swap value is given, that swap strategy will be used.

If a selector is given, the first element matching that selector will
be swapped.
**If not**, the element with an ID matching the new content will be swapped.


Example
==========


- https://discord.com/channels/725789699527933952/725789747212976259/895340146361253888

::

    something like this:
    <div hx-swap-oob="innerHTML:body">
       <!-- content to swap into the body -->
       ...
    </div>
    [18:01]
    I think that should place all that content inside the body tag
    [18:02]
    haven't tested though


::

    @lobre
    Effectively, i forgot that hx-swap-oob could take a selector,
    I thought oob needed an id somewhere. So yes, it seems to mimic
     what could be achieved with HX-Retarget. Thanks!

    @1cg
     no prob, I forget too.  you are pushing htmx hard, this is good!
