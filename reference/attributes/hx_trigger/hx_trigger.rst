.. index::
   pair: HTMX; hx-trigger
   ! hx-trigger

.. _hx_trigger:

==================================================================================================================================
HTMX **hx-trigger** attribute (allows you to specify what triggers an AJAX request)
==================================================================================================================================

- https://htmx.org/attributes/hx-trigger/

Description
=============

The **hx-trigger** attribute allows you to specify what triggers an AJAX
request.

A trigger value can be one of the following:

- An **event name** (e.g. "click" or "my-custom-event") followed by an event
  filter and a set of event modifiers
- A polling definition of the form every **<timing declaration>**
- A comma-separated list of such events


Standard Events
================

A standard event, such as click can be specified as the trigger like so:

.. code-block:: django

    <div hx-get="/clicked" hx-trigger="click">Click Me</div>


Notes
======

**hx-trigger** is inherited and can be placed on a parent element.


Examples
========

- :ref:`devops_click_to_edit`


hx-trigger='load' pattern by Carston Gross
=============================================

- https://discord.com/channels/725789699527933952/725789747212976259/897071168413126686

mmm, yes, interesting.  that particular feature was a good example of
something that htmx has offered since the start via the hx-trigger='load' pattern,
and it definitely breaks progressive enhancement.

(As an aside, intercooler.js has had this feature for a decade now: https://intercoolerjs.org/examples/lazyload.html)

Yes exactly. I just wanted to mitigate a little bit when people say that
Hotwire/Turbo is full batteries-included and full progressive enhancement.
That’s just not the case. There a multiple features in Turbo that need
JavaScript to me.

