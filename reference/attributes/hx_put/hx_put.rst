.. index::
   pair: HTMX; hx-put
   ! hx-put

.. _hx_put:

=============================================================================================================================================
HTMX **hx-put** attribute (will cause an element to issue a PUT to the specified URL and swap the HTML into the DOM using a swap strategy)
=============================================================================================================================================

- https://htmx.org/attributes/hx-put/
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT

Description
=============


.. code-block:: django

    <button hx-put="/account" hx-target="body">
      Put Money In Your Account
    </button>

This example will cause the button to issue a PUT to /account and swap
the returned HTML into the innerHTML of the body.

Notes
======

- hx-put is not inherited
- You can control the target of the swap using the :ref:`hx-target attribute <hx_target>`
- You can control the swap strategy by using the :ref:`hx-swap attribute <hx_swap>`
- You can control what event triggers the request with the hx-trigger attribute
- You can control the data submitted with the request in various ways, documented here: Parameters

Examples
========

- :ref:`devops_click_to_edit`
