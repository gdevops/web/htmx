.. index::
   pair: HTMX; hx-sync
   ! hx-sync

.. _hx_sync:

==================================================================================================================================
HTMX **hx-sync** attribute (allows you to specify how the response will be swapped in relative to the target of an AJAX request)
==================================================================================================================================

- https://htmx.org/attributes/hx-sync/


Description
=============
