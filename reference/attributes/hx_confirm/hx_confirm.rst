.. index::
   pair: HTMX;hx-confirm
   !hx-confirm

.. _hx_confirm:

================================================================================
HTMX **hx-confirm** allows you to confirm an action before issuing  a request
================================================================================

- https://htmx.org/attributes/hx-confirm/

Description
=============

The **hx-confirm** attribute allows you to confirm an action before issuing
a request.

This can be useful in cases where the action is destructive and you want
to ensure that the user really wants to do it.

Here is an example::

    <button hx-delete="/account" hx-confirm="Are you sure you wish to delete your account?">
      Delete My Account
    </button>

.. note::

    hx-confirm is inherited and can be placed on a parent element


Example
==========

- :ref:`htmx_confirm`
