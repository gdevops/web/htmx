.. index::
   pair: HTMX; hx-get
   ! hx-get

.. _hx_get:

=============================================================================================================================================
HTMX **hx-get** attribute (will cause an element to issue a GET to the specified URL and swap the HTML into the DOM using a swap strategy)
=============================================================================================================================================

- https://htmx.org/attributes/hx-get/
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/GET

Description
=============


.. code-block:: django

  <div hx-get="/example">Get Some HTML</div>

This example will cause the div to issue a GET to /example and swap the
returned HTML into the **innerHTML of the div**.


Notes
======

- hx-get is not inherited
- By default hx-get does not include any parameters.
  You can use the :ref:`hx-params <hx_params>` attribute to change this
- You can control the target of the swap using the :ref:`hx-target <hx_target>` attribute
- You can control the swap strategy by using the :ref:`hx-swap <hx_swap>` attribute
- You can control what event triggers the request with the :ref:`hx-trigger <hx_trigger>` attribute
- You can control the data submitted with the request in various ways,
  documented here: :ref:`Parameters <htmx_parameters>`


Examples
========

- :ref:`devops_click_to_edit`
