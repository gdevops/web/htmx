.. index::
   pair: HTMX; hx-post
   ! hx-post

.. _hx_post:

=============================================================================================================================================
HTMX **hx-post** attribute (will cause an element to issue a POST to the specified URL and swap the HTML into the DOM using a swap strategy)
=============================================================================================================================================

- https://htmx.org/attributes/hx-post/
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST

Description
=============

The hx-post attribute will cause an element to issue a POST to the
specified URL and swap the HTML into the DOM using a swap strategy:


.. code-block:: django

   <button hx-post="/account/enable" hx-target="body">
      Enable Your Account
   </button>


This example will cause the button to issue a POST to /account/enable
and swap the returned HTML into the innerHTML of the body.


Notes
======

- hx-post is not inherited
- You can control the target of the swap using the :ref:`hx-target <hx_target>` attribute
- You can control the swap strategy by using the :ref:`hx-swap <hx_swap>` attribute
- You can control what event triggers the request with the :ref:`hx-trigger <hx_trigger>` attribute
- You can control the data submitted with the request in various ways, documented here: :ref:`Parameters <htmx_parameters>`
