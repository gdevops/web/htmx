.. index::
   pair: HTMX; attributes
   ! hx attributes

.. _htmx_attributes:

==================================================================================
**HTMX reference attributes**
==================================================================================

.. warning:: you can use the `data-` prefix on all `hx-*` attributes

   - https://x.com/htmx_org/status/1484268141970550786?s=20


.. toctree::
   :maxdepth: 1
   :caption: AJAX

   hx_boost/hx_boost
   hx_delete/hx_delete
   hx_disinherit/hx_disinherit
   hx_get/hx_get
   hx_headers/hx_headers
   hx_include/hx_include
   hx_post/hx_post
   hx_put/hx_put
   hx_patch/hx_patch
   hx_params/hx_params
   hx_request/hx_request
   hx_sync/hx_sync
   hx_trigger/hx_trigger

.. toctree::
   :maxdepth: 1
   :caption: HTML

   hx_confirm/hx_confirm
   hx_swap/hx_swap
   hx_swap_oob/hx_swap_oob
   hx_target/hx_target
   hx_select/hx_select


.. toctree::
   :maxdepth: 1
   :caption: Response Headers Reference

   hx_redirect/hx_redirect


.. toctree::
   :maxdepth: 1
   :caption: CSS

   hx_indicator/hx_indicator


.. toctree::
   :maxdepth: 1
   :caption: Javascript

   hx_ext/hx_ext
