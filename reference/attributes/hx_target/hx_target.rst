.. index::
   pair: HTMX; hx-target
   ! hx-target

.. _hx_target:

==================================================================================================================================
HTMX **hx-target** attribute (allows you to target a different element for swapping than the one issuing the AJAX request)
==================================================================================================================================

- https://htmx.org/attributes/hx-target/
- https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentHTML


Description
=============

The **hx-target** attribute allows you to target a different element for
swapping than the one issuing the AJAX request.

The value of this attribute can be:

- a CSS query selector of the element to target
- **this** which indicates that the element that the hx-target attribute
  is on is the target
- **closest <CSS selector>** which will find the closest parent ancestor
  that matches the given CSS selector. (e.g. closest tr will target the
  closest table row to the element)
- **find <CSS selector>** which will find the first child descendant element
  that matches the given CSS selector. (e.g **find tr** will target the first
  child descendant row to the element)


Here is an example that targets a div


.. code-block:: django

    <div>
        <div id="response-div"></div>
        <button hx-post="/register" hx-target="#response-div" hx-swap="beforeend">
            Register!
        </button>
    </div>

The response from the /register url will be appended to the div with the
id **response-div**.


Notes
======

**hx-target** is inherited and can be placed on a parent element.


Examples
========

- :ref:`devops_click_to_edit`

