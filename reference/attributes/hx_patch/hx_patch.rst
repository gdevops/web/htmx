.. index::
   pair: HTMX; hx-patch
   ! hx-patch

.. _hx_patch:

====================================================================================================================================================
HTMX **hx-patch** attribute (will cause an element to issue a **PATCH** to the specified URL and swap the HTML into the DOM using a swap strategy)
====================================================================================================================================================

- https://htmx.org/attributes/hx-patch/
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH

Description
=============


.. code-block:: django

    <button hx-patch="/account" hx-target="body">
      Patch Your Account
    </button>


This example will cause the button to issue a **PATCH** to /account
and swap the returned HTML into the innerHTML of the body


Notes
======

- **hx-patch** is not inherited
- You can control the target of the swap using the :ref:`hx-target <hx_target>` attribute
- You can control the swap strategy by using the :ref:`hx-swap <hx_swap>` attribute
- You can control what event triggers the request with the :ref:`hx-trigger <hx_trigger>` attribute
- You can control the data submitted with the request in various ways, documented here: :ref:`Parameters <htmx_parameters>`
