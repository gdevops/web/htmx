.. index::
   pair: HTMX; hx-boost
   ! hx-boost

.. _hx_boost:

==================================================================================================================================================================
HTMX **hx-boost** allows you to "boost" normal anchors and form tags to use AJAX instead
==================================================================================================================================================================

- https://htmx.org/attributes/hx-boost/

Description
=============

The **hx-boost** attribute allows you to "boost" normal anchors and form
tags to use AJAX instead.

**This has the nice fallback that, if the user does not have javascript
enabled, the site will continue to work**.

For anchor tags, clicking on the anchor will issue a GET request to the
url specified in the href and will push the url so that a history entry
is created. The target is the <body> tag, and the innerHTML swap strategy
is used by default.

All of these can be modified by using the appropriate attributes, except
the click trigger.

For forms the request will be converted into a GET or POST, based on the
method in the method attribute and will be triggered by a submit.

Again, the target will be the body of the page, and the innerHTML swap
will be used.

Here is an example of some boosted links:

.. code-block::

    <div hx-boost="true">
      <a href="/page1">Go To Page 1</a>
      <a href="/page2">Go To Page 2</a>
    </div>

.. note::
    - hx-boost is inherited and can be placed on a parent element
    - Only links that are to the same domain and that are not local anchors will be boosted
    - All requests are done via AJAX, so keep that in mind when doing things like redirects


Discussion
=============

A general question for the general channel! (Sorry for the long post).

When using htmx, I find myself striving a lot for "no page reloads".
But today, I am wondering if it is really worth the effort, as I realize
it sometimes requires small "hacks" and adaptations in my code that are
specific to that goal, and that drives me away from writing "normal" web applications.

In htmx, :ref:`hx_boost <hx_boost>` is presented as a solution to avoid page
reloads, to help to implement progressive enhancement, in the same mileage as
Turbolinks/Turbo drive. If I understand correctly, "progressive enhancement"
means to improve the experience for users when possible, and give a basic
experience otherwise.
In our context, I guess it means having "no reload" browsing for users
having Javascript and regular browsing for others.

With all the "tricks" that I used to make the "no reload" experience work,
I am now wondering if my application is still usable without Javascript.
For instance, I try to avoid HTTP redirections as they are not really
compliant with htmx, but it has the side effect of not redirecting users
that don’t have javascript. I could try to preserve the basic experience,
but that would mean checking all the time HX-Request, and enhance or not
accordingly, but still deliver the "normal" behavior if not present.

I don’t know if you share my opinion on this, but I would like to receive
feedback on whether you use hx-boost or not. And if so, do you use it
sparingly, or do you try — as me — to have a full "no reload" experience?

Htmx is more of a toolbox that gives you low-level primitives to deal
with swapping, while apps like Turbolinks (or htmx’s hx-boost) are acting
at a higher level, boosting the experience without you (the developer)
thinking much about it.

What do you think? Thanks for your feedback.
[09:39]
And in the end, I tend to think that it might be easier in terms of
maintenance to simply boost all pages at the body level, without doing
any particular low level/manual swapping in the app, and render the full
body at each interaction. The server-side app would stay classic, and
progressive enhancement would work out of the box.

Or forget about the full "no reload" goal, and only improve small parts
of the application using htmx when a reload really degrade the experience.




