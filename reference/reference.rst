.. index::
   pair: HTMX ; reference

.. _htmx_reference:

==================================================================================
HTMX **reference**
==================================================================================


- https://htmx.org/reference/


.. toctree::
   :maxdepth: 1

   attributes/attributes
   api/api
   events/events
   htmx_js/htmx_js
