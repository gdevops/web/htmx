.. index::
   pair: HTMX ; events
   ! Events

.. _htmx_events:

==================================================================================
HTMX **events**
==================================================================================

- https://htmx.org/reference/#events


Htmx provides an extensive events system that can be used to modify and
enhance behavior.

Events are listed below.

.. toctree::
   :maxdepth: 3

   config_request/config_request

