.. index::
   pair: htmx ; configRequest
   ! htmx:configRequest

.. _htmx_configRequest:

==================================================================================
htmx:configRequest
==================================================================================

- https://htmx.org/events/#htmx:configRequest

Description
=============

This event is triggered after htmx has collected parameters for inclusion
in the request.

It can be used to include or update the parameters that htmx will send

::

    document.body.addEventListener('htmx:configRequest', function(evt) {
        evt.detail.parameters['auth_token'] = getAuthToken(); // add a new parameter into the mix
    });

Note that if an input value appears more than once the value in the
parameters object will be an array, rather than a single value.

Details
==========

- detail.parameters - the parameters that will be submitted in the request
- detail.unfilteredParameters - the parameters that were found before filtering by hx-select
- detail.headers - the request headers
- detail.elt - the element that triggered the request
- detail.target - the target of the request
- detail.verb - the HTTP verb in use

