.. index::
   pair: HTMX ; htmx.js
   pair: Source ; htmx.js
   pair: Javascript ; htmx.js
   ! htmx.js

.. _htmx_js:

==================================================================================
src/htmx.js
==================================================================================

.. literalinclude::  ../../github/src/htmx.js
   :language: javascript
   :linenos:
