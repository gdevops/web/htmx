.. index::
   pair: HTMX; addClass()
   ! addClass()

.. _htmx_add_class:

==================================================================================
**htmx.addClass()** (adds a class to the given element)
==================================================================================

- https://htmx.org/api/#addClass

Description
============

This method adds a class to the given element.
Parameters

- elt - the element to add the class to
- class - the class to add

Example::

  // add the class 'myClass' to the element with the id 'demo'
  htmx.addClass(htmx.find('#demo'), 'myClass');
