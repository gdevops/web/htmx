

.. _htmx_tutorials:

=====================
Htmx tutorials
=====================

.. toctree::
   :maxdepth: 3


   bugbytes/bugbytes
   django-htmx-patterns/django-htmx-patterns
   harabat/harabat
   mkennedy/mkennedy
