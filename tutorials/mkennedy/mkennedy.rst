
.. _mkennedy_htmx:

==========================================================================================
**HTMX + Flask: Modern Python Web Apps, Hold the JavaScript Course** by Mickael Kennedy
==========================================================================================

- https://x.com/mkennedy
- https://github.com/talkpython/htmx-python-course
- https://training.talkpython.fm/courses/htmx-flask-modern-python-web-apps-hold-the-javascript#course_outline


Course Summary
==================

**htmx** is one of the hottest properties fire in web development today, and
for good reason.

This framework, along with the libraries and techniques introduced in
this course, will have you writing the best Python web apps you've ever
written: clean, fast, and interactive without all that frontend overhead.

If you are a Python web developer that has wanted to build more dynamic,
interactive apps, but just don't want to (or can't) write a significant
portion of your app in a rich frontend JavaScript framework, you'll
absolutely love htmx.

This library lets you write Python code you love and still add that
client-side interactive aspect.

