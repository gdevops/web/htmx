.. index::
   pair: Tutorial ; Harabat

.. _htmx_harabat:

==================================================================================
**A tutorial for building the RealWorld app in Django and HTMX**
==================================================================================

- https://github.com/harabat/django-htmx
- https://harabat.github.io/django-htmx/
