.. index::
   pair: Tutorial ; Bugbytes

.. _bugbytes_videos:

==================================================================================
**Bugbytes videos**
==================================================================================

- https://github.com/bugbytes-io/django-htmx
- https://www.bugbytes.io/posts/django-and-htmx/
- https://www.youtube.com/watch?v=Ula0c_rZ6gk&list=PL-2EBeDYMIbRByZ8GXhcnQSuv2dog4JxY&ab_channel=BugBytes


- Django and HTMX #1 - Introduction to HTMX and Dynamic AJAX Requests
