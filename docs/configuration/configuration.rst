.. index::
   pair: config ; Htmx
   pair: config ; Htmx

.. _htmx_config:

==================================================================================
Configuring htmx
==================================================================================

- https://htmx.org/docs/#config


Introduction
==============

Htmx has some configuration options that can be accessed either
programatically or declaratively.

They are listed below:


Example
=========


- https://discord.com/channels/725789699527933952/864934037381971988/895275258708377670

Hi, I am facing an issue with my csp rules, when importing my local copy
of "htmx.js" (v.1.6.0). My browser console tells me , that it blocked
some inline style (htmx.js:2673:35). The referenced function inserts
some html in style tags.

Accoding to the security section in the htmx documentation it should work
without an issue with CSP. I would like to go with the general recommendations
of not configuring the CSP style-src to allow unsave-inline. It currently
only allows 'self'.

Am I missing something? I would appreciate any help!


1cg — Hier à 13:44
You can turn off the automatic insertion of styles by setting the config
option htmx.config.includeIndicatorStyles to false, and then you'd need
to manually include the styles for indictators:

  .htmx-indicator {opacity:0;transition: opacity 200ms ease-in;}
  .htmx-indicator.htmx-request {opacity:1}
  .htmx-indicator .htmx-request {opacity:1}
