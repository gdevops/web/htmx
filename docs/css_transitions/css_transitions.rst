.. index::
   pair: CSS ; Transitions
   ! CSS Transitions

.. _htmx_css_transitions:

==================================================================================
HTMX CSS transitions
==================================================================================

- https://htmx.org/docs/#css_transitions

.. toctree::
   :maxdepth: 3

   introduction/introduction
   examples/examples
