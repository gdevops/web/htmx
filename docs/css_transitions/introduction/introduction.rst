
.. _htmx_css_transitions_intro:

==================================================================================
HTMX CSS transitions Introduction
==================================================================================

- https://htmx.org/docs/#css_transitions


htmx makes it easy to use **CSS Transitions without javascript**.

To understand how CSS transitions work in htmx, you must first understand
the swap & settle model that htmx uses.

When new content is received from a server, before the content is swapped in,
the existing content of the page is examined for elements that match by
the id attribute.
If a match is found for an element in the new content, the attributes of
the old content are copied onto the new element **before the swap occurs**.

The new content is then swapped in, but with the old attribute values.

Finally, the new attribute values are swapped in, after a "settle" delay
(100ms by default).

This may seem a bit complicated, but with this mechanic for swapping
content, you can write CSS transitions from old to new attribute values.

An example will help clarify. Consider this original content:


.. code-block:: html

   <div id="div1">Original Content</div>

And this content, which has been received by htmx after an AJAX request,
to replace it:

.. code-block:: html

   <div id="div1" class="red">New Content</div>


The first thing that htmx does, before it swaps in this new content, is
note that these two elements match by id (and tag type).
It therefore swaps the old attribute values onto the new content:

.. code-block:: html

  <div id="div1">New Content</div>

Note that the new content no longer has a class attribute. This modified
new content is then swapped into the DOM. This is the swap step.

Next, after a "settle" delay, the new div will have its attributes
updated to the actual values received from the server:

.. code-block:: html

  <div id="div1" class="red">New Content</div>


.. code-block:: css

    .red {
      color: red;
      transition: all ease-in 1s ;
    }

And the newly swapped content will gently transition to a red text color over one second.

All of that may seem a little crazy, but it can be summarized as this:

    In htmx, all you need to do to use CSS transitions for an element is keep its id stable across requests


Because this div was in the DOM with the original div's attributes, this
is will trigger a CSS transition. So you can write, for example, this CSS:

.. code-block:: css

    .red {
      color: red;
      transition: all ease-in 1s ;
    }

And the newly swapped content will gently transition to a red text color
over one second.

All of that may seem a little crazy, but it can be summarized as this::

    In htmx, all you need to do to use CSS transitions for an element
    is keep its id stable across requests

