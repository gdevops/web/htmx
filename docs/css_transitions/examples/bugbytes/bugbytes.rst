

.. _htmx_css_transitions_bugbytes:

==============================================================================================
Django and HTMX #2 - Trigger Modifiers & CSS Transitions by https://x.com/bugbytesio
==============================================================================================

- https://x.com/bugbytesio
- https://www.bugbytes.io/posts/django-and-htmx-trigger-modifiers-css-transitions/
- https://github.com/bugbytes-io/django-htmx/tree/master/Video%20%232
- https://www.youtube.com/watch?v=yf7_txKvexk (Django and HTMX #2 - Trigger Modifiers and CSS Transitions)

Introduction
=================================================

This post will demonstrate how to use HTMX trigger modifiers, and how to
use CSS transitions in the context of an HTMX request/response.

We will extend the code from the previous tutorial in this `video <https://www.youtube.com/watch?v=yf7_txKvexk>`_.
This code can be found here on `Github <https://github.com/bugbytes-io/django-htmx/tree/master/Video%20%232>`_


Objectives
==============

In this post, we will:

- Learn what HTMX trigger modifiers are, and how to use them.
- Learn how to use CSS transitions in the context of HTMX requests/responses


CSS Transitions
=================

**HTMX enables easy use of CSS transitions, too**.
Let's walk through how to achieve this.

Underneath our form-field, we had a <div id='username-error'> element where
our message from the backend was displayed.

We also had the following view which returned HTML on the keyup trigger.


.. code-block:: python

    def check_username(request):
        username = request.POST.get('username')
        if get_user_model().objects.filter(username=username).exists():
            return HttpResponse("This username already exists")
        else:
            return HttpResponse("This username is available")


We are now going to modify this to return a <div id='username-error'>
element that can be swapped in, and we are going to attach a class of
success if the username is available, and error if the username is not
available. It's vital that we keep the ID the same, so the transition/animation
can occur. Replace the above code with the following.


.. code-block:: python

    def check_username(request):
        username = request.POST.get('username')
        if get_user_model().objects.filter(username=username).exists():
            return HttpResponse(
                "<div id='username-error' class='error'>This username already exists</div>"
            )
        else:
            return HttpResponse(
                "<div id='username-error' class='success'>This username is available</div>"
            )

We are now going to create CSS styles that enable the transition to occur.
In the **static/css/styles.css** file, add the following styles.

.. code-block:: css

    .success {
        color: green;
        transition: all ease-in .5s;
    }

    .error {
        color: red;
        font-size: 50px;
        transform: rotate(45deg);
        transition: all ease-in 5s;
    }

This code results in a transition occurring when the class changes from
error to success, or vice-versa. When the class changes to error, a 5-second
animation occurs where:

- The text-colour transitions from green to red
- The font-size transitions to 50px
- The text rotates 45 degrees

So, we simply have to keep the same element ID, and in our CSS files we
can specify the transition.

For this to work, we need to perform a few extra setup steps. Firstly,
add the following CSS link to the base.html, along with the static template-tag.

.. code-block:: django

    {% load static %}
    <link rel="stylesheet" href="{% static 'css/styles.css' %}" />

We also need to tell Django where to find our static files.
Within settings.py add the following line.

.. code-block:: python

   STATICFILES_DIRS = [BASE_DIR / 'static']

Now, we're good to go. We should see a transition such as the following
on the registration form.

.. figure:: images/demo.png
   :align: center

This is just for demonstration - this particular transition is a terrible
user experience, so do not use in your web apps!

But it shows that we can easily create legitimate, engaging transitions
using HTMX. We simply return HTML from our view that preserves element
IDs, and attach CSS classes that define what styles and transitions we want.

This functionality helps you build reactive, modern web applications,
without resorting to complex JavaScript frameworks and build tools.


