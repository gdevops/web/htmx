.. index::
   pair: Parameters ; HTMX
   ! Parameters

.. _htmx_parameters:

==================================================================================
HTMX **Parameters**
==================================================================================

- https://htmx.org/docs/#parameters

Introduction
=============

By default, an element that causes a request will include its value if
it has one.
If the element is **a form** it will include the values of all
inputs within it.

Additionally, if the element causes a non-GET request, the values of all
the inputs of the nearest enclosing form will be included.

If you wish to include the values of other elements, you can use the
hx-include attribute with a CSS selector of all the elements whose
values you want to include in the request.

If you wish to filter out some parameters you can use the :ref:`hx-params <hx_params>` attribute.

Finally, if you want to programatically modify the parameters, you can
use the :ref:`htmx:configRequest event <htmx_configRequest>`.

