.. index::
   pair: debugging ; monitorEvents()
   pair: debugging ; htmx.logAll()
   ! htmx.logAll()
   ! monitorEvents()

.. _htmx_debugging:

==================================================================================
Htmx debugging => htmx.logAll(), monitorEvents()
==================================================================================

- https://htmx.org/docs/#debugging


Introduction
==============

Declarative and **event driven programming** with htmx (or any other declarative
language) can be a wonderful and highly productive activity, but one
disadvantage when compared with imperative approaches is that it can be
tricker to debug.

Figuring out why something isn't happening, for example, can be difficult
if you don't know the tricks.

Well, here are the tricks:

The first debugging tool you can use is the htmx.logAll() method.
This will log every event that htmx triggers and will allow you to see
exactly what the library is doing::

    htmx.logAll();

Of course, that won't tell you why htmx isn't doing something.

You might also not know what events a DOM element is firing to use as a
trigger.
To address this, you can use the monitorEvents() method available in the
browser console::

    monitorEvents(htmx.find("#theElement"));

This will spit out all events that are occuring on the element with the
id theElement to the console, and allow you to see exactly what is going
on with it.

Note that this only works from the console, you cannot embed it in a
script tag on your page.

Finally, push come shove, you might want to just debug htmx.js by loading
up the unminimized version.
It's about 2500 lines of javascript, so not an insurmountable amount of code.

You would most likely want to set a break point in the issueAjaxRequest()
and handleAjaxResponse() methods to see what's going on.

And always feel free to jump on the Discord if you need help.
