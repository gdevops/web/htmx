.. index::
   pair: AJAX ; HTMX
   ! AJAX

.. _htmx_ajax:

==================================================================================
HTMX **AJAX**
==================================================================================

- https://htmx.org/docs/#ajax

Introduction
=============

The core of htmx is a set of attributes that allow you to issue AJAX
requests directly from HTML.


+---------------------------------+----------------------------------------------------------------------------------------------------------------+
| HTMX Attribute                  | Description                                                                                                    |
+=================================+================================================================================================================+
| :ref:`hx-get <hx_get>`          | Issues a `GET request to the given URL <https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/GET>`_       |
+---------------------------------+----------------------------------------------------------------------------------------------------------------+
| :ref:`hx-post <hx_post>`        | Issues a `POST request to the given URL <https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST>`_     |
+---------------------------------+----------------------------------------------------------------------------------------------------------------+
| :ref:`hx-put <hx_put>`          | Issues a `PUT request to the given URL <https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT>`_       |
+---------------------------------+----------------------------------------------------------------------------------------------------------------+
| :ref:`hx-patch <hx_patch>`      | Issues a `PATCH request to the given URL <https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH>`_   |
+---------------------------------+----------------------------------------------------------------------------------------------------------------+
| :ref:`hx-delete <hx_delete>`    | Issues a `DELETE request to the given URL <https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/DELETE>`_ |
+---------------------------------+----------------------------------------------------------------------------------------------------------------+

Each of these attributes takes a URL to issue an AJAX request to.
The element will issue a request of the specified type to the given URL
when the element is :ref:`triggered <htmx_triggers>`:

::

  <div hx-put="/messages">
    Put To Messages
  </div>


This tells the browser: When a user clicks on this div, issue a PUT request
to the URL **/messages** and load the response into the div.



MDN HTTP methods documentation
==================================

- https://tools.ietf.org/html/rfc7231#section-4
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/GET
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/POST
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PUT
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/PATCH
- https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods/DELETE
