.. index::
   pair: HTMX ; docs
   pair: HTMX ; in a Nutshell

.. _htmx_docs:

==================================================================================
HTMX **docs** (AJAX, CSS transitions)
==================================================================================

- https://htmx.org/docs/

.. toctree::
   :maxdepth: 3

   introduction/introduction
   installing/installing
   ajax/ajax
   triggers/triggers
   configuration/configuration
   parameters/parameters
   inheritance/inheritance
   css_transitions/css_transitions
   debugging/debugging
