.. index::
   pair: HTMX ; installing

.. _installing_htmx:

==================================================================================
Installing HTMX
==================================================================================

- https://htmx.org/docs/#installing


Htmx is a **dependency-free javascript library**.

It can be used via NPM as "htmx.org" or downloaded or included from unpkg
or your other favorite NPM-based CDN::

    <script src="https://unpkg.com/htmx.org@1.6.0"></script>

For added security, you can load the script using Subresource Integrity (SRI).

    <script src="https://unpkg.com/htmx.org@1.6.0" integrity="sha384-G4dtlRlMBrk5fEiRXDsLjriPo8Qk5ZeHVVxS8KhX6D7I9XXJlNqbdvRlp9/glk5D" crossorigin="anonymous"></script>

