
.. _htmx_people:

==================================================================================
**htmx people**
==================================================================================

.. toctree::
   :maxdepth: 3

   carson_gross/carson_gross
   deniz_aksimsek/deniz_aksimsek
   benpate/benpate
   bugbytesio/bugbytesio
   david_guillot/david_guillot
   dev_simplicity/dev_simplicity
   hernantz/hernantz
   jack_linke/jack_linke
   jonathan_adly/jonathan_adly
   fubarr/fubarr
   mickael_kennedy/mickael_kennedy
   thomas_guttler/thomas_guttler

