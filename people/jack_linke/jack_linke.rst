.. index::
   ! Jack Linke

.. _jack_linke:

==================================================
**Jack Linke** (Django + HTMX)
==================================================

- https://x.com/JackDLinke
- https://jacklinke.com/
- https://github.com/jacklinke/htmx-talk-2021

Conferences
============

- :ref:`jack_linke_2021_10_22`
