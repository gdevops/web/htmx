.. index::
   ! Carson Gross

.. _carson_gross:

==================================================
**Carson Gross**
==================================================

- https://x.com/htmx_org
- https://github.com/1cg
- https://disqus.com/by/carsongross/
- https://github.com/bigskysoftware
- https://stackoverflow.com/users/14288140/1cg
- https://www.youtube.com/channel/UCope0NMQh8RDGLgCTBjk6-A/featured


::

    carson @ bigsky.software


.. figure:: carson.png
   :align: center

   https://github.com/bigskysoftware


:ref:`I'm a programming languages person. <programming_lang_person>`


On stackoverflow
==================

- https://stackoverflow.com/users/14288140/1cg


Conferences/articles
=======================

- :ref:`gross_2024_03_13`
- :ref:`carson_gross_2021_10_22`
