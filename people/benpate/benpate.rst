.. index::
   ! benpate

.. _benpate:

==================================================
**benpate**
==================================================

- https://stackoverflow.com/users/4437560/ben-pate
- https://stackoverflow.com/questions/69245098/how-to-navigate-html-pages-without-loading-using-htmx/69563878#69563878
- https://stackoverflow.com/questions/66955550/htmx-format-date-for-browser-locale/69563926#69563926


2021-10-26
===========

- https://discord.com/channels/725789699527933952/725789747212976259/902553678999871538

Thank you @benpate for your work on pulling SSE and Websockets out to
`extensions ! <htmx_extensions`
