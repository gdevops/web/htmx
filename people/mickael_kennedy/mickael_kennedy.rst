.. index::
   ! Mickael Kennedy

.. _mickael_kennedy:

==================================================
**Mickael Kennedy** (Flask + HTMX)
==================================================

- https://x.com/mkennedy
- https://github.com/talkpython/htmx-python-course
- https://training.talkpython.fm/courses/htmx-flask-modern-python-web-apps-hold-the-javascript#course_outline


Course
==========

- :ref:`mkennedy_htmx`
