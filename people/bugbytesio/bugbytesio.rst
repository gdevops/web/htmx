.. index::
   ! bugbytesio

.. _bugbytesio:

==================================================
**bugbytesio**
==================================================

- https://x.com/bugbytesio
- https://www.youtube.com/channel/UCTwxaBjziKfy6y_uWu30orA/videos


2022
=====

- https://www.youtube.com/watch?v=AWhv-R24hu4 (2022-02-15, Django, HTMX and Alpine.js - Building an Accordion component)
- https://www.youtube.com/watch?v=27vq-h4Vv6c (2022-02-07, Django and HTMX #15 - Click to Load pattern)
- https://www.youtube.com/watch?v=Fh2iDdASxjE (2022-01-30, django-crispy-forms and HTMX integration #3 - Field validation and the hx-swap-oob attribute)
- https://www.youtube.com/watch?v=ZxvhAKT0Wwo (2022-01-27, django-crispy-forms and HTMX integration #2 - User Registration and Login/Logout
- https://www.youtube.com/watch?v=MZwKoi0wu2Q (2022-01-18, Building Django forms with django-crispy-forms #1)

2021
=====

- https://www.bugbytes.io/posts/django-and-htmx-forms-with-django-forms-dynamic/ (2021-12-29)
- https://www.bugbytes.io/posts/django-bokeh-and-htmx-data-driven-line-charts/ (2021-12-28)
- https://www.bugbytes.io/posts/django-bokeh-and-htmx-data-driven-bar-charts/ (2021-12-27)
- https://www.bugbytes.io/posts/django-and-htmx-chained-dropdown/ (2021-12-16)
- https://www.bugbytes.io/posts/django-and-htmx-live-scores-project-2/ (2021-12-02)
- https://www.bugbytes.io/posts/django-and-htmx-live-scores-project/ (2021-12-01)
- https://www.bugbytes.io/posts/django-and-htmx-delete-page/ (2021-11-03)
- https://www.bugbytes.io/posts/django-and-htmx-list-and-create-page-with-no-refreshes/ (2021-11-02)
- https://www.bugbytes.io/posts/django-and-htmx-trigger-modifiers-css-transitions/ (2021-10-30)
