.. index::
   ! Deniz Akşimşek

.. _deniz_aksimsek:

==================================================
**Deniz Akşimşek**
==================================================

- https://x.com/DenizAksimsek/
- https://denizaksimsek.com/
- https://github.com/dz4k/
- https://thisweek.htmx.org

.. figure:: deniz_aksimsek.webp
   :align: center

   https://denizaksimsek.com/

