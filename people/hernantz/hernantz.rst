.. index::
   ! hernantz

.. _hernantz:

==================================================
**hernantz**
==================================================

- https://x.com/hernantz
- https://github.com/hernantz
- http://hernantz.github.io/
- https://github.com/hernantz/django-htmx-demo


Conférence
===========

- :ref:`hernantz_2021_10_25`
