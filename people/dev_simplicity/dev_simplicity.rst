.. index::
   ! DevSimplicity

.. _DevSimplicity:

==================================================
**DevSimplicity**
==================================================

- https://x.com/devsimplicity
- https://devsimplicity.com/

DevSimplicity: Simple, Not Easy — 2021-10-16 à 15:47
======================================================

- https://discord.com/channels/725789699527933952/725789747212976259/898930548892925984

::

    Hi guys, I'm new here (but not new to the ideas behind htmx/intercooler/hyperscript).
    [15:48]
    I've been using very similar approach to htmx/intercooler for a very
    long time. I was kind of hoping that this thick-client SPA frameworks
    cargo culting will end, but apparently not.
    [15:49]
    I recently started writing about those issues and I'm trying to
    spread as much awareness as I can about htmx (and the underlying
    issues it's solving).

    I'll also try to familiarize myself with the codebase, so I can help
    with occasional pull requests.


DevSimplicity: Simple, Not Easy — Hier
==========================================

- https://discord.com/channels/725789699527933952/725789747212976259/898946800113819659

::


    Sorry, I probably didn't express myself well. I'm new here (on discord),
    but I've used both intercooler.js and htmx before (and I've had a very
    similar, custom lib long before I saw intercooler.js on HN).

    I'll take a quick look at docs if something important has changed
    in the midtime.

    btw. thank you all for working on this great lib (and making the web
    a better place), especially to the author for creating it.
