.. index::
   ! FrOW (Fragments-Over-the-Wire)

.. _ref_frow_essay_guttli:

============================================================
**FrOW** HTML Fragments over the Wire by Thomas Güttler
============================================================

- :ref:`guettli`
- :ref:`frow_essay_guttli`
- https://github.com/guettli/frow--fragments-over-the-wire
