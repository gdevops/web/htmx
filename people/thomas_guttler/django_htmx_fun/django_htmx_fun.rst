
.. _django_htmx_fun_guttli:

==================================================
**django-htmx-fun**
==================================================

- :ref:`guettli`
- https://github.com/guettli/django-htmx-fun
- https://github.com/guettli/django-htmx-fun/commits/main

Description
============

- :ref:`infinite_scroll_thomas`

A small Django application to advertise the fun htmx can bring you.
It implements a Single-Page-Application for writing a diary.

The entries in the diary database get lazy loaded (endless scrolling)
via hx-trigger="revealed"


Why I love htmx ?
====================

If you look into the past then one thing is clear: stateless has won.
Nobody starts a new project with Corba these days. Stateless http is the winner.

I don't understand why JavaScript based frontend frameworks seem to be
the only way for new projects.

I want the client/browser to be SSS (simple, stupid and stateless).

I need to validate my data on the server anyway. So why should I validate
them on the client?

The Django Forms library has all you need to write database focused applications.

Sending HTML fragements over the wire keeps my application simple.

There is just one thing which is outdated (although it is still perfectly fine).
The need for a full page refresh after submitting a form.

I want html pages with several small forms and I want to load and submit
each of them individually. This does not mean I want to write a
Single-Page-Application. There are more colors than black and white.

For more about htmx see the homepage: `htmx.org <https://htmx.org/>`_
