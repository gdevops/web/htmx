.. index::
   ! frow (fragments-over-the-wire)
   ! guettli
   ! Thomas Güttler

.. _guettli:

==================================================
**Thomas Güttler (guettli)** (Django + HTMX)
==================================================

- https://x.com/guettli
- https://github.com/guettli
- https://github.com/guettli/wol
- https://stackoverflow.com/users/633961/guettli
- https://stackexchange.com/filters/409974/htmx

Thomas Güttler
================

"I am Thomas Güttler. Born 1976 in Germany. I like web development and
try to avoid JS as much as possible".

Inventor of the term :ref:`Frow (Fragments over the wire) <frow_essay_guttli>`.
See :ref:`hotx`.


.. figure:: guettli.jpg
   :align: center

   https://thomas-guettler.de/

Conferences
============

- :ref:`thomas_gunter_2021_10_22`


.. _descript_team:

**descript** team
===================

- https://www.descript.de/en/
- https://www.descript.de/en/team/
- :ref:`ref_descript`

.. figure:: descript_team.png
   :align: center

   https://youtu.be/z0yPTv15Fjk?t=2152



Thomas WOL: Working out Loud
===============================

- https://github.com/guettli/wol


Programming guidelines
========================

- https://github.com/guettli/programming-guidelines

Güttli's opinionated Python Tips
====================================

- https://github.com/guettli/python-tips


Why I like PyCharm
=====================

- https://github.com/guettli/why-i-like-pycharm/


Django tips
=============

- https://github.com/guettli/django-tips


Essays
==========


.. toctree::
   :maxdepth: 3

   fragments_over_the_wire/fragments_over_the_wire


Projects
==========


.. toctree::
   :maxdepth: 3

   django_htmx_fun/django_htmx_fun
