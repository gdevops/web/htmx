.. index::
   pair: Django htmx projects; Jonathan Adly
   ! Jonathan Adly

.. _jonathan_adly:

==================================================
**Jonathan Adly**  Django/HTMX developer !
==================================================

- https://jonathanadly.com/
- https://htmx-django.com/
- https://x.com/DjangoHtmx
- https://x.com/Jonathan_Adly\_
- https://github.com/Jonathan-Adly
- https://github.com/Jonathan-Adly/django-boilerplate


.. figure:: jonathan_adly.png
   :align: center

   https://github.com/Jonathan-Adly


Projects
=========

- :ref:`jonathan_adly_projects`
