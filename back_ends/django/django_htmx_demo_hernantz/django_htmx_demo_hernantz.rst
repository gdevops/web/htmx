.. index::
   ! django-htmx-demo

.. _hernantz_django_htmx_demo:

==========================================================================================================
**Django-htmx Test demo to show how to integrate django with htmx**
==========================================================================================================

- https://github.com/hernantz/django-htmx-demo
- :ref:`hernantz_2021_10_25`
- :ref:`preload_extension`
- https://ricostacruz.com/nprogress/
- https://youtu.be/dEg-K3kMj60?t=1491

This project was extracted from the demo app in the `django-htmx app <https://github.com/adamchainz/django-htmx>`.

It adds the `preload <https://htmx.org/extensions/preload/>`_ extension + hx-boost
and the `Nprogress plugin <https://ricostacruz.com/nprogress/>`_ to show
the loading indicator for all ajax requests.

