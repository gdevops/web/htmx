.. index::
   pair: Django ; owela-club

.. _django_owela_club:

======================================================================================================
**owela-club** Play the Namibian game of Owela against a terrible AI. Built using **Django and htmx**
======================================================================================================

- https://github.com/adamchainz/owela-club
- https://mancala.fandom.com/wiki/Owela


Description
=============

A Django project for playing the Namibian game of Owela against a dumb AI.

Built following the rules described on the `Mancala World wiki page for Owela <https://mancala.fandom.com/wiki/Owela>`_.
