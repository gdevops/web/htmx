.. index::
   ! django-htmx-modal-popup-loveliness

.. _django_htmx_modal_popup_loveliness:

==========================================================================================================
**django-htmx-modal-popup-loveliness** django htmx modal popup loveliness
==========================================================================================================

- https://x.com/AndyTWoods
- https://www.andytwoods.com/django-htmx-modal-popup-loveliness/
- https://github.com/andytwoods/htmxDjangoModalPopup


Introduction
============

I thought to share a pattern I've been using recently, for providing
modal popups, via HTMX.

I'm using Bootstrap5 but this is straightforward to implement in whichever
framework.

As ever, HTMX has a wonderful `example <https://htmx.org/examples/modal-bootstrap/>`_ (https://htmx.org/examples/modal-bootstrap/)
of how to do this in the frontend (as well any many other `examples <https://htmx.org/examples/>`_ (https://htmx.org/examples/) !).

Here though I show how I'm doing things both front and backend, and
**specifically for Django**.

I'm doing things a little differently than the official example, cutting
out some lines of code.

You can find the repo (including sqlite db -- admin username and password is 'me')
here: https://github.com/andytwoods/htmxDjangoModalPopup

