.. index::
   ! Django with htmx

.. _django_with_htmx:

==================================================================================
**Django with HTMX**
==================================================================================

- https://htmx.discourse.group/t/django-and-htmx/17
- https://django-news.com/search?q=htmx
- https://x.com/AdamChainz/status/1383114453881868291?s=20
- https://django-news.com/search?q=htmx
- https://github.com/ajcerejeira/talksapp
- https://github.com/adamchainz/django-htmx
- https://github.com/jacklinke/django-htmx-todo-list
- https://github.com/MattSegal/django-htmx-intro
- https://github.com/justdjango/django_htmx_dynamic_forms


.. figure:: htmx_django.png
   :align: center

.. toctree::
   :maxdepth: 3

   faqs/faqs
   codecademy_mealwise/codecademy_mealwise
   django_boilerplate/django_boilerplate
   django_htmx/django_htmx
   django_htmx_demo_hernantz/django_htmx_demo_hernantz
   htmx_django_modal_popup/django_htmx_modal_popup
   htmx_django/htmx_django
   django_htmx_modal_form/django_htmx_modal_form
   django_htmx_toasts/django_htmx_toasts
   django_htmx_rest/django_htmx_rest
   django_check_html_middleware/django_check_html_middleware
   ajax_enabled_checkbox/ajax_enabled_checkbox
   django_htmx_examples/django_htmx_examples
   django_admin_liste_editable/django_admin_list_editable
   django_htmx_todo_list/django_htmx_todo_list
   django_htmx_fun/django_htmx_fun
   how_to_htmx_django/how_to_htmx_django
   owela_club/owela_club
   saaspegasus/saaspegasus
