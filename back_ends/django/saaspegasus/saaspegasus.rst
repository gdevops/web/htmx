
.. _saaspegasus_django_htmx:

=========================================================================================
**Django, HTMX and Alpine.js: Modern websites, JavaScript optional** by saaspegasus
=========================================================================================

- https://www.saaspegasus.com/guides/modern-javascript-for-django-developers/htmx-alpine/


.. figure:: images/chapter_5.png
   :align: center


Introduction
=================


In Part 5 we’re going to take things in a new direction.

In this installment we'll turn to the low- and no-JavaScript world.
We'll cover some of the common approaches to "sprinkling in" light
amounts of JavaScript into your Django projects in 2021.

We’ll start high-level—approaching the big-picture questions of when you
might choose a "low-JavaScript" architecture, and how to decide when to
bring in a framework.

After that we'll dive into two of the best low-JavaScript tools to use
with Django today: Alpine.js and HTMX.

Now get cozy, put away your Webpack, React, and Vue, and get ready for
some good old-fashioned server-rendered Django goodness!
