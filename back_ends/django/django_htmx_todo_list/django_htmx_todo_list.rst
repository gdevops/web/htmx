.. index::
   pair: Django ; htmx-todo-list

.. _django_htmx_todo_list:

===========================================================================================
**django-htmx-todo-list** Quick example of a todo list application using Django and HTMX
===========================================================================================

- https://github.com/OmenApps/django-htmx-todo-list
- https://spookylukey.github.io/django-views-the-right-way/

Background
==============

This project lets you build todo lists. It demonstrates functionality
with django and HTMX, including use of modal forms, adding multiple
forms to a list (an alternative to traditional django formsets), and
deleting items from a list (or an entire list).

The original project used class-based Django views.

That has been improved to use **function-based views**
(see `Django Views — The Right Way <https://spookylukey.github.io/django-views-the-right-way/>`_
**to read why FBV is often the better approach!**)

There are actually two example projects here which are the same in all
respects except that tasker uses hard-coded html forms, and tasker2 uses
django forms with django-crispy-forms for formatting.

This project is very basic.

It does not make use of authorization or other common important concerns.
The focus is 100% on demonstrating some Django & HTMX concepts.
