.. index::
   ! django-htmx

.. _django_htmx:

==========================================================================================================
**Django-htmx (Build Eight Modern Single Page Applications. No complicated JS frameworks necessary)**
==========================================================================================================

- https://github.com/adamchainz/django-htmx


.. toctree::
   :maxdepth: 3

   installation/installation
   versions/versions
