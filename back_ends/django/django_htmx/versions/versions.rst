
.. _django_htmx_versions:

==================================================================================
**Django-htmx versions**
==================================================================================

- https://github.com/adamchainz/django-htmx
- https://github.com/adamchainz/django-htmx/graphs/contributors


.. toctree::
   :maxdepth: 3

   1.2.1/1.2.1
   1.1.0/1.1.0
