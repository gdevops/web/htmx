
.. _django_htmx_install:

==================================================================================
**Django-htmx installation**
==================================================================================

Installation
==============

Install with pip::

    python -m pip install django-htmx



Install with poetry::

    poetry add django-htmx


Add the middleware::

    MIDDLEWARE = [
        ...,
        "django_htmx.middleware.HtmxMiddleware",
        ...,
    ]


