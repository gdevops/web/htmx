.. index::
   pair: modal-form ; Django-htmx

.. _django_htmx_modal_form:

=========================================================================================
**django-htmx-modal-form** Toasts with Django+HTMX
=========================================================================================

- https://blog.benoitblanchon.fr/django-htmx-modal-form/


This article describes the pattern I use to implement modal forms (i.e.,
forms in a modal dialog box) with Django and HTMX.
