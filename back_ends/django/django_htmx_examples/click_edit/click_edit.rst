
.. _devops_click_to_edit:

=========================================================================================
HTMX with Django **Example 1 Click to Edit**
=========================================================================================

- http://127.0.0.1:8000/click_to_edit/init/
- https://framagit.org/gdevops/django_htmx_examples
- https://htmx.org/examples/click-to-edit/
- :ref:`hx_get`
- :ref:`hx_target`
- :ref:`hx_swap`

:Source:

    - https://chriswedgwood.com/blog/htmx-examples-using-django-click-to-edit/

HTMX click-editing
=====================

- https://htmx.org/examples/click-to-edit/


The click to edit pattern provides a way to offer inline editing of all
or part of a record without a page refresh.

This pattern starts with a UI that shows the details of a contact.

The div has a button that will get the editing UI for the contact from
/contacts/1/edit


.. code-block:: html
   :linenos:

    <div hx-target="this" hx-swap="outerHTML">
        <div><label>First Name</label>: Joe</div>
        <div><label>Last Name</label>: Blow</div>
        <div><label>Email</label>: joe@blow.com</div>
        <button hx-get="/contact/1/edit" class="btn btn-primary">
        Click To Edit
        </button>
    </div>

This returns a form that can be used to edit the contact

.. code-block:: html

    <form hx-put="/contact/1" hx-target="this" hx-swap="outerHTML">
      <div>
        <label>First Name</label>
        <input type="text" name="firstName" value="Joe">
      </div>
      <div class="form-group">
        <label>Last Name</label>
        <input type="text" name="lastName" value="Blow">
      </div>
      <div class="form-group">
        <label>Email Address</label>
        <input type="email" name="email" value="joe@blow.com">
      </div>
      <button class="btn">Submit</button>
      <button class="btn" hx-get="/contact/1">Cancel</button>
    </form>

**The form issues a PUT back to /contacts/1, following the usual REST-ful pattern**.

Django HTMX Click To Edit
==============================

- https://framagit.org/gdevops/django_htmx_examples/-/tree/main/django_htmx_examples/click_to_edit

The click to edit pattern provides a way to offer inline editing of all
or part of a record without a page refresh.

- http://127.0.0.1:8000/click_to_edit/init/

.. figure:: click_edit_init.png
   :align: center


click_to_edit/templates/click_to_edit/initial_state.html
---------------------------------------------------------

- https://framagit.org/gdevops/django_htmx_examples/-/raw/main/django_htmx_examples/click_to_edit/templates/click_to_edit/initial_state.html


.. code-block:: django
   :linenos:


    {% extends "base.html" %}

    {% block content %}
        <div hx-target="this" hx-swap="outerHTML">
            {% if contact %}
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">
                    <div class="px-4 py-5 sm:px-6">
                        <h3 class="text-lg leading-6 font-medium text-gray-900">
                            Click To Update Person
                        </h3>
                        <p class="mt-1 max-w-2xl text-sm text-gray-500">
                            <a class="font-medium text-indigo-600 hover:text-indigo-500" href="https://htmx.org/examples/click-to-edit/">HTMX example here</a>
                        </p>
                    </div>
                    <div class="border-t border-gray-200">
                        <dl>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-medium text-gray-500 ">
                                    First Name
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{contact.first_name}}
                                </dd>
                            </div>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-medium text-gray-500">
                                    Last Name
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{contact.last_name}}
                                </dd>
                            </div>
                            <div class="bg-gray-50 px-4 py-5 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-6">
                                <dt class="text-sm font-medium text-gray-500">
                                    Email address
                                </dt>
                                <dd class="mt-1 text-sm text-gray-900 sm:mt-0 sm:col-span-2">
                                    {{contact.email_address}}
                                </dd>
                            </div>
                        </dl>
                    </div>
                </div>
                <button hx-get="{% url 'click_to_edit:contact_view_update' contact_id=contact.id %}" type="button"
                    class="ml-2 inline-flex mt-10 items-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                    Click To Update
                </button>
            {% else %}
                <div class="bg-white shadow overflow-hidden sm:rounded-lg">

                    <div class="px-4 py-5 sm:px-6">
                        <h3 class="text-lg leading-6 font-medium text-gray-900">
                            You dont have a Contact , please add one in the admin!
                        </h3>
                    </div></div>
            {% endif %}
        </div>

    {% endblock content %}
