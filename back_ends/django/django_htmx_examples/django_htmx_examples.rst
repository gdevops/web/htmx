
.. _django_htmx_examples_noam:

=========================================================================================
**Django HTMX examples**
=========================================================================================


- http://127.0.0.1:8000
- https://framagit.org/gdevops/django_htmx_examples
- An app for demonstrating the HTMX examples on https://htmx.org/examples/.

:Source: https://github.com/chriswedgwood/django-htmx-examples


.. figure:: django_htmx_examples.png
   :align: center

.. toctree::
   :maxdepth: 3

   installation/installation
   click_edit/click_edit
   bulk_update/bulk_update
