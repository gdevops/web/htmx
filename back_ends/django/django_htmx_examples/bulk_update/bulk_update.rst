
.. _devops_bulk_update:

=========================================================================================
HTMX with Django **Example 2 Bulk Update**
=========================================================================================

- http://127.0.0.1:8000/bulk_update/bulk-update/
- https://framagit.org/gdevops/django_htmx_examples/-/tree/main/django_htmx_examples/bulk_update
- https://chriswedgwood.com/blog/htmx-with-django-example-2-bulk-update/
- :ref:`hx_target`
- :ref:`hx_swap`


:Source:

    - https://chriswedgwood.com/blog/htmx-with-django-example-2-bulk-update/


HTMX bulk update
===================

- https://htmx.org/examples/bulk-update/


This demo shows how to implement a common pattern where rows are selected
and then bulk updated.

This is accomplished by putting a form around a table, with checkboxes
in the table, and then including the checked values in POST's to two
different endpoints: activate and deactivate:

.. code-block:: html

    <div hx-include="#checked-contacts" hx-target="#tbody">
      <a class="btn" hx-put="/activate">Activate</a>
      <a class="btn" hx-put="/deactivate">Deactivate</a>
    </div>

    <form id="checked-contacts">
        <table>
          <thead>
          <tr>
            <th></th>
            <th>Name</th>
            <th>Email</th>
            <th>Status</th>
          </tr>
          </thead>
          <tbody id="tbody">
            <tr class="">
              <td><input type='checkbox' name='ids' value='0'></td>
              <td>Joe Smith</td>
              <td>joe@smith.org</td>
              <td>Active</td>
            </tr>
            ...
          </tbody>
        </table>
    </form>

The server will either activate or deactivate the checked users and then rerender
the tbody tag with updated rows.

It will apply the class activate or deactivate to rows that have been mutated.
This allows us to use a bit of CSS to flash a color helping the user
see what happened:

.. code-block:: css

  .htmx-settling tr.deactivate td {
    background: lightcoral;
  }
  .htmx-settling tr.activate td {
    background: darkseagreen;
  }
  tr td {
    transition: all 1.2s;
  }


.. figure:: htmx_bulk_update.png
   :align: center


Django HTMX Bulk Update
=========================

.. figure:: bulk_update.png
   :align: center


