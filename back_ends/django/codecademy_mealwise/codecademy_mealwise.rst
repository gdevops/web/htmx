
.. _codecademy:

================================================================================================================================
A web app to help a restaurant keep track of its inventory throughout the day. Capstone project for Codecademy Django course
================================================================================================================================

- https://github.com/iamjameswalters/codecademy-mealwise


Announce
=========

- https://discord.com/channels/725789699527933952/909436816388669530/943593606332174456


Well, I think I'm finally ready to show this off:

Been learning web development with Django on Codecademy. After completing
the capstone project the standard Django web 1.0 way, I spent some time
htmx-ifying it, as well as sprinkling some hyperscript throughout
(which makes this a #djhype app before the stack was coined  ).

Extremely pleased with the UI patterns I was able to achieve this way!
Christ is opening doors for me at my job as I learn this stuff, so I'm
thankful to Him for the opportunity to practice with these libraries.

You can see the code at https://github.com/iamjameswalters/codecademy-mealwise

You can interact with a live demo of the site at https://iamjameswalters.pythonanywhere.com/


Thanks! This is the stack my coworker and I are about to use to reimplement
an internal web app we have, so this was all valuable practice.

**htmx/hyperscript** are definitely elevating the quality of this project,
because we wouldn't have fussed with JS for it


Mealwise
===========

A web app to help a restaurant keep track of its inventory throughout
the day.

Capstone project for `Codecademy Django <https://www.codecademy.com/learn/paths/build-python-web-apps-with-django>`_ course.

This app uses `Bootstrap 5 <https://getbootstrap.com/>`, `htmx <https://htmx.org/>`_,
`\_hyperscript <https://hyperscript.org/>`_, `Bokeh <https://bokeh.org/>`_
for the revenue chart, and `django-admin-interface <https://github.com/fabiocaccamo/django-admin-interface>`_.

The majority of that was added after completing the requirements for
the assignment (v1.0)

Try it Out
===========

Visit `iamjameswalters.pythonanywhere.com <https://iamjameswalters.pythonanywhere.com/>`_ to
interact with a live demo of the app.

The database should reset nightly. Feel free to create your own account,
or use `user: testuser/pass: mealwise`.

Without signing in, you can place and delete orders.
All the other features require an account.
