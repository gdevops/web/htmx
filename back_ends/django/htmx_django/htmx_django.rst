.. index::
   ! htmx-django

.. _htmx_django_adly:

==================================================================================
**htmx-django** by Jonathan Adly
==================================================================================

- https://htmx-django.com/

.. toctree::
   :maxdepth: 3

   projects/projects
