.. index::
   pair: Django htmx projects; Jonathan Adly

.. _jonathan_adly_projects:

====================================
Jonathan Adly Django HTMX Projects
====================================

- https://htmx-django.com/

.. toctree::
   :maxdepth: 3

   tic_tac_toe/tic_tac_toe
   markdown_preview/markdown_preview
   htmx_stack/htmx_stack
   countdown/countdown

