
.. _django_tic_tac_toe:

=========================================
Project 3: **Tic-Tac-Toe** (2021-11-04)
=========================================

- https://htmx-django.com/
- https://github.com/Jonathan-Adly/htmx-tictactoe.git
- https://htmx-django.com/blog/project-3-tic-tac-toe
- https://x.com/DjangoHtmx/status/1456260946704297984?s=20

.. toctree::
   :maxdepth: 3

   configuration/configuration
   human_player/human_player
