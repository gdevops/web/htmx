
.. _tic_tac_toe_humn_player:

==========================
Human Player
==========================

X or O
---------

Since we represented the symbol on our user model, the first user story
is straightforward.

We need to hit an endpoint, that will change the symbol (player field)
from "X" to "O" and vice versa.

We would use HTMX, so we would return a component that holds the player symbol.

First, let's go ahead and make that component::

    touch templates/components/game_info.html

Now, we will cut the game info div from our home template and paste it in our component.


.. code-block:: django

    <!-- cut this div from the home.html -->
    <!-- paste in the game_info.html component -->
    <div class="game-info mt-20">
        <div class="status">
            You are Player X. <a href= "#"> Change to O </a>
        </div>
    </div>


Now - let's go ahead and change our component to use HTMX::

    <div class="game-info mt-20" id="game_info">
        <div class="status">
        You are Player {{request.user.player}}.

        <button
        hx-get="{% url 'change_player' %}"
        hx-target= "#game_info"
        hx-swap = "outerHTML"
        >
         Change Your symbol </button>
    </div>
    </div>

That's the HTMX part. Now, let's work on the Django part, building our endpoint and our view to handle it.
