from django.urls import path
from . import views

urlpatterns = [
    path("", views.home, name="home"),
    path("change-player", views.change_player, name="change_player"),
    path("play", views.play, name="play"),
    path("reset", views.reset, name="reset"),
    path("game-status", views.game_status, name="game_status"),
    path("computer-play", views.computer_play, name="computer_play"),
]
