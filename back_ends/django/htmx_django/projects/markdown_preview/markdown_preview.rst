.. index::
   pair: Django htmx projects; Markdown preview

.. _django_markdown_preview:

=================================================
Project 2: **Markdown preview** (2021-10-26)
=================================================

- https://github.com/Jonathan-Adly/htmx-blog/tree/boilerplate
- https://htmx-django.com/blog/project-2-markdown-preview
