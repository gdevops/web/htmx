.. index::
   pair: Django htmx; FAQ
   ! Django FAQ

.. _django_htmx_faq:

=====================
Django HTMX FAQ
=====================


Many people new to htmx seem to struggle with redirects
=========================================================

Question
    Many people new to htmx seem to struggle with redirects.
    It was the same for me. If you did old-school web development in the
    past, then you used the post/redirect/get pattern.

    This is common in the django world. If you set the hx-redirect http header
    in a response and the http status is 302, AFAIK htmx does first fetch the
    content from the new URL and ignores the hx-redirect http header.

    Where in the docs would be a good place to make people aware of this ?


Response
    https://discord.com/channels/725789699527933952/725789747212976259/899286907027066900
    1cg — Aujourd’hui à 15:25
    It would probably be good to add a section on this topic to the /www/docs.md file
