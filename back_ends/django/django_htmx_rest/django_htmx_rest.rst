.. index::
   ! django-htmx

.. _django_htmx_rest:

==========================================================================================================
**Django-htmx-rest**
==========================================================================================================

- https://github.com/jdevries3133/django_htmx_rest


Discord announce
===================

- https://discord.com/channels/725789699527933952/864934037381971988/903754798682210324

Hey folks, I have an idea for a tiny library to integrate the django rest
framework and htmx. I call it
django-htmx-rest (https://github.com/jdevries3133/django_htmx_rest) It'll
be a pretty small library, because the two already go together very nicely, but
there are a few key features:

- content negotiator that also is responsive to the Hx-Request header
- implement a BrowsableHTMXRenderer, which extends the DRF browsable API
  with frontend component development tooling, like viewport resizing,
  etc (think [storybook](https://storybook.js.org/))
- integrating the existing django-htmx library (https://github.com/adamchainz/django-htmx)
  with DRF, if there's anything to be done there (I'm not sure)
- other misc. niceties, like maybe testing support

Plus, strong documentation, to illustrate how Django + DRF + htmx can make REST
dreams come true.

I have gotten off to a very small start and hope to keep building it out to the
extent that it supports another project I'm working on primarily, but if you're
interested in the idea, it's MIT licensed and I welcome anyone who'd like to
contribute!

Any feedback on the idea is much appreciated too!

Here is the repo link if you're interested in contributing:
https://github.com/jdevries3133/django_htmx_rest (modifié)

