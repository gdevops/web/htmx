.. index::
   pair: lucid-htmx; Haskell

.. _lucid_htmx:

==================================================================================
Lucid EDSL + HTMX = **lucid-htmx**
==================================================================================

- https://github.com/WaviLabs/lucid-htmx
- https://hackage.haskell.org/package/lucid-htmx
- https://hackage.haskell.org/package/lucid-htmx-0.1.0.0/docs/Lucid-HTMX.html

Announce
==========

- https://x.com/htmx_org/status/1455544365477150729?s=20
- https://thisweek.htmx.org/issue/3/
