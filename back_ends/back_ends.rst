.. index::
   pair: HTMX; backends
   ! Backends applications

.. _backends_http_servers:

==================================================================================
**Backends applications (Django, FastAPI...)**
==================================================================================

- https://htmx.org/server-examples/
- :ref:`tuto_webframeworks:web_javascript_technical_debt`
- https://github.com/guettli/frow--fragments-over-the-wire

Front end devs thinkin' everyone is gonna be using javascript on the back end soon.

.. figure:: htmx_is_safe.png
   :align: center
   :width: 600

   https://x.com/htmx_org/status/1449028339042291718?s=20

.. toctree::
   :maxdepth: 3

   server_examples/server_examples

.. toctree::
   :maxdepth: 5

   django/django

.. toctree::
   :maxdepth: 3

   fastapi/fastapi
   flask/flask
   haskell/haskell
