.. index::
   ! Fast htmx

.. _fast_htmx:

==================================================================================
**FastAPI with HTMX (a demo project of FastAPI an HTMX)**
==================================================================================

- https://x.com/marty331
- https://fastapi.tiangolo.com/
- https://github.com/marty331/fasthtmx

**Fast-HTMX is a demo project of FastAPI an HTMX.**

The purpose of this project is to illustrate how to create a website
with no JavaScript, using only HTML, CSS, and Python. HTMX is a plugin
that allows this to be possible.
