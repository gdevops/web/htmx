.. index::
   ! HTMX


Introduction, essays

   - :ref:`frow_essay_guttli`
   - :ref:`htmx_hateoas`

.. figure:: images/htmx_logo.png
   :align: center

   https://htmx.org/

.. figure:: images/htmx_bandeau.jpeg
   :align: center

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/htmx/rss.xml>`_


.. _htmx_tuto:

==========================================================================================
|htmx| **HTMX** (high power tools for HTML, htmx is the successor to intercooler.js)
==========================================================================================

- https://htmx.org/
- https://github.com/bigskysoftware/htmx
- https://github.com/bigskysoftware/htmx/discussions
- https://unpkg.com/browse/htmx.org/
- https://htmx.org/docs/
- https://htmx.org/discord
- http://htmx.org/feed.xml
- https://discord.com/invite/Z6gPqAd
- https://www.reddit.com/r/htmx/
- https://www.reddit.com/user/_htmx/
- https://stackoverflow.com/questions/tagged/htmx
- https://djangochat.com/episodes/htmx-carson-gross/transcript
- https://github.com/rajasegar/awesome-htmx
- https://github.com/PyHAT-stack/awesome-python-htmx
- https://github.com/spookylukey/django-htmx-patterns
- https://spookylukey.github.io/django-views-the-right-way/
- https://hypermedia.systems/


.. figure:: images/hateoas_schema.webp

.. toctree::
   :maxdepth: 5

   news/news


.. toctree::
   :maxdepth: 3


   definition/definition
   people/people
   essays/essays
   philosophy/philosophy
   docs/docs
   reference/reference
   extensions/extensions
   examples/examples
   tips/tips
   faqs/faqs
   tutorials/tutorials
   versions/versions


.. toctree::
   :maxdepth: 5
   :caption: Backends

   back_ends/back_ends

.. toctree::
   :maxdepth: 3
   :caption: Front end

   frontend/frontend

.. toctree::
   :maxdepth: 1
   :caption: intercooler the htmx ancestor

   intercooler/intercooler

