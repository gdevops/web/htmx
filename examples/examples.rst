.. index::
   pair: HTMX ; Examples
   ! Examples

.. _htmx_examples:

==================================================================================
HTMX examples
==================================================================================

- https://htmx.org/examples/
- https://www.youtube.com/@prettyprinted/videos


.. toctree::
   :maxdepth: 1

   progress_bar/progress_bar
   confirm/confirm
