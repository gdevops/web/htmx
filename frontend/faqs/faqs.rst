.. index::
   pair: HTMX; frontend FAQS
   ! Frontend FAQS

.. _frontend_faqs:

==================================================================================
**Front end FAQS**
==================================================================================

.. toctree::
   :maxdepth: 3

   back_button/back_button
   tom_select/tom_select
