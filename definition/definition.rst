.. index::
   pair: HOWL; Hypertext On Whatever (back end) you’d Like
   ! pair: HAT ; htmx, @Alpine_JS, @tailwindcss
   ! HOWL
   ! HAT

.. _htmx_def:

==================================================================================
HTMX definition
==================================================================================


Definition 1
==============


- https://htmx.org/docs/#introduction

**htmx** allows you to access AJAX, CSS Transitions, WebSockets and Server Sent Events
directly in HTML, using attributes, so you can build modern user interfaces
with the simplicity and power of hypertext.

**htmx** is small (~9k min.gz'd), dependency-free, extendable & IE11 compatible

motivation
==============

- Why should only <a> and <form> be able to make HTTP requests?
- Why should only click & submit events trigger them?
- Why should only GET & POST be available?
- Why should you only be able to replace the entire screen?

By removing these arbitrary constraints htmx completes HTML as a hypertext.


Definition 2 **htmx versus hotwire** by Carston Gross
==========================================================

- https://discuss.hotwired.dev/t/hotwire-vs-htmx-comparison/1614

I think that’s a reasonable take on **htmx v. hotwire**. htmx doesn’t
focus nearly as much on progressive enhancement, although there is a
mechanism for doing so (:ref:`hx-boost <hx_boost>`).

htmx is attempting to **push HTML to its logical conclusion as a hypertext**
and, hence, is lower level than what hotwire looks like at first glance.
(I’m looking at it as an ousider, so take all this with a grain of salt.)

With htmx, the idea is to surface as much as possible in straight HTML
and generalize the concept of a hypertext: event triggers, requests, targets
for replacement, etc.

I would expect hotwire to work more smoothly out of the box, and kick out
to Stimulus when necessary, whereas htmx would likely be more explicit
work, but can probably go farther before it is necessary to resort to javascript.

Both reasonable approaches. Regardless of what direction someone decides
to go, I’m glad to see HTML-oriented libraries making a resurgence.


Definition 3 by Carston Gross
================================

- https://discord.com/channels/725789699527933952/725789747212976259/897068914838757427

Yep, as I have said many, many times: **htmx is lower level than hotwire**.
It is a bottom-up improvement of HTML, rather than a top-down framework.

Each approach has its pros and cons: htmx is going to make creating fine-grain
HTTP interactions easier, but it will require more work out of the box
in some cases.

If you want a batteries-included, drop-in framework, hotwire will be better
for you, and that's a perfectly reasonable approach to web development.

I am not going to say anything bad about hotwire, it's a fine library.

htmx is different technically and philosophically, however, and the
differences, I have to say, are pretty obvious if you just spend a bit
of time reading the documentation for each.

HOWL stack: Hypertext On Whatever (back end) you'd Like
=============================================================

- https://x.com/htmx_org/status/1403389705039736835?s=20
- https://htmx.org/essays/locality-of-behaviour/

**htmx** is a proud component of the **HOWL** stack: **Hypertext On Whatever
(back end) you'd Like**

but we excited about the emerging **HAT** stack: **htmx, @Alpine_JS, @tailwindcss**

which allows you to do all your front end directly in HTML, for
`Locality of Behavior bliss <https://htmx.org/essays/locality-of-behaviour/>`_


How about THUD: Tailwind + htmx under Django ?
=================================================

- https://x.com/AdamChainz/status/1383114453881868291?s=20

::

    How about THUD: Tailwind + htmx under Django ?

    https://x.com/AdamChainz/status/1383114453881868291?s=20

different mindsets
==================

- https://x.com/htmx_org/status/1404841847374024710?s=20

different mindsets
vue is js-driven, reactive and thick-clientish
**htmx is hypermedia driven, HATEOAS-ish and REST-ful**


What's this course about and how is it different?


Htmx definition by Mickael Kennedy
====================================

- https://github.com/talkpython/htmx-python-course

**htmx** is a very compelling technology.

It's one of the first legitimate libraries that delivers on the promise
of dynamic, interactive web applications without complex JavaScript.

**htmx** allows us to write server-side code in any language (Python in our case)
and, through a few simple HTML attributes, bring our pages to life.


